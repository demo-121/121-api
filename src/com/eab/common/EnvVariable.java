package com.eab.common;

import java.util.HashMap;
import java.util.Map;

public class EnvVariable {

	private static Map<String, String> defaultVars = new HashMap<String, String>();

	private static void init() {
		defaultVars = new HashMap<String, String>();
		defaultVars.put("INTERNET_PROXY", "");		//Empty String for disable proxy
		defaultVars.put("INTRANET_PROXY", "");		//Empty String for disable proxy
		defaultVars.put("WEB_API_APWSG_PN", "https://preprodapwsg.axa-tech.com:10443/life-api/application/v1/assign-policy-numbers");
		defaultVars.put("WEB_API_MAAM", "https://maam-dev.axa.com/dev/token");
		defaultVars.put("MAAM_USERNAME", "866279d0-cd4e-4211-a4b7-85c3a707346a");
		defaultVars.put("MAAM_SECRET", "3651852b-3506-4c78-bcf5-e19784c516ee");
		defaultVars.put("WEB_API_WFI_DOC", "https://preprodapwsg.axa-tech.com:10443/document-api/common/v2/documents");
		defaultVars.put("WEB_API_WFI_DOC_ENV", "");
		defaultVars.put("WEB_API_RLS_APP", "https://preprodapwsg.axa-tech.com:10443/life-api/application/v1/applications");
		defaultVars.put("WEB_API_RLS_APP_STATUS", "https://preprodapwsg.axa-tech.com:10443/qa4/life-api/application/v1/applications/status");
		defaultVars.put("WEB_API_RLS_APP_CALLBACK", "https://preprodapwsg.axa-tech.com:10443/sg/ease-sit/ease-api/esub/callback");
		defaultVars.put("WEB_API_RLS_PAYMENT", "https://preprodapwsg.axa-tech.com:10443/life-api/initialPayment/v1/payments/initialPayments");
		defaultVars.put("WEB_API_RLS_PAYMENT_STATUS", "https://preprodapwsg.axa-tech.com:10443/qa4/life-api/initialPayment/v1/payments/initialPayments/status");
		defaultVars.put("WEB_API_RLS_PAYMENT_CALLBACK", "https://preprodapwsg.axa-tech.com:10443/sg/ease-sit/ease-api/payrec/callback");
		defaultVars.put("GATEWAY_URL", "http://192.168.222.159");
		defaultVars.put("GATEWAY_PORT", "4985");
		defaultVars.put("GATEWAY_DBNAME", "axasg_dev");
		defaultVars.put("GATEWAY_USER", "");
		defaultVars.put("GATEWAY_PW", "");
		defaultVars.put("WEB_API_SMTP_HOST", "192.168.222.127");
		defaultVars.put("WEB_API_SMTP_PORT", "25");
		defaultVars.put("INTERNET_PROXY_HOSTS"
				, "{\"hostname\": [\"preprodapwsg.axa-tech.com:10443\", \"maam-dev.axa.com\", \"api.infinite-convergence.com\"]}");
				//Empty String for no config, example: {"hostname": ["preprodapwsg.axa-tech.com:10443", "maam-dev.axa.com"]}
		defaultVars.put("INTRANET_PROXY_HOSTS"
				, "{\"hostname\": [\"ease-sit.intranet\", \"ease-sit2.intranet\"]}");
				//Empty String for no config, example: {"hostname": ["ease-sit.intranet", "ease-sit2.intranet"]}
		defaultVars.put("BATCH_EMAIL_REPORT_FROM", "");
		defaultVars.put("BATCH_EMAIL_REPORT_TO", "");
		defaultVars.put("WEB_API_SMS_USERNAME", ""); // [!] Real SMS Config should not apply to DEV / SIT only when
														// testing required.
		defaultVars.put("WEB_API_SMS_PASSWORD", ""); // [!] Real SMS Config should not apply to DEV / SIT only when
														// testing required.
		defaultVars.put("WEB_API_SMS_HOST", ""); // [!] Real SMS Config should not apply to DEV / SIT only when testing
													// required.
		defaultVars.put("WEB_API_SMS_SOURCE", "AXA"); // [!] Real SMS Config should not apply to DEV / SIT only when
														// testing required.
		defaultVars.put("WEB_API_EASYPAY2_HOST", "https://test.wirecard.com.sg/easypay2/paymentpage.do");
		defaultVars.put("WEB_API_EASYPAY2_MID", "20151111011");
		defaultVars.put("WEB_API_EASYPAY2_ENETS_MID", "20151111011");
		defaultVars.put("WEB_API_EASYPAY2_IPP_MID", "20152111288");
		defaultVars.put("WEB_API_EASYPAY2_RETURN_URL", "http://axasglocal.eab.com:3000/static/paymentResult");
		defaultVars.put("WEB_API_EASYPAY2_STATUS_URL", "https://apiwss2-project1.cloudapps.218.189.183.72.xip.io/ease-api/wirecard/callback");
		defaultVars.put("WEB_API_EASYPAY2_TRX_TIME_LIMIT", "120");
		defaultVars.put("WEB_API_EASYPAY2_SECURITY_KEY", "ABC123456");
		defaultVars.put("INTERNAL_API_DOMAIN", "http://localhost:8080/ease-api");
		defaultVars.put("SSL_TRUSTSTORE_PWD", "EasE2017");
		defaultVars.put("SSL_KEYSTORE_PWD", "EasE2017");
		defaultVars.put("SSL_KEY_PWD", "EasePwd!");
		defaultVars.put("SSL_TRUSTSTORE_FILE", "ease-truststore-sit.jks");
		defaultVars.put("SSL_KEYSTORE_FILE", "ease-keystore-sit.jks");
		
		defaultVars.put("SPE_IP", "192.168.223.111");
		defaultVars.put("SPE_PORT", "1344");
		defaultVars.put("SPE_USERNAME", "hello");
		defaultVars.put("SPE_PASSWORD", "abc123");
		defaultVars.put("SPE_LOCAL_TEMP_PATH", "C:\\Temp");
		
		/***
		 * Environment Variable but prohibited controlling in OpenShift.
		 * Warning! Don't put below variable(s) into OpenShift Environment Variable.
		 */
		defaultVars.put("ERROR_EMAIL_FROM", "");			//Empty String for disable.
		defaultVars.put("ERROR_EMAIL_TO", "");				//Empty String for disable.  Comma as separater if more than one address
		defaultVars.put("SWITCH_GET_FROM_POOL", "Y");
		defaultVars.put("PDFREACTOR_LICENSE", "<license><licenseserialno>4436</licenseserialno><licensee><name>AXA Singapore</name><address><street>8 Shenton Way, #24-01 AXA Tower</street><city>Singapore 068811</city><country>Singapore</country></address></licensee><product>PDFreactor</product><majorversion>9</majorversion><minorversion>0</minorversion><licensetype>CPU</licensetype><amount>8</amount><unit>CPU</unit><maintenanceexpirationdate>2019-01-26</maintenanceexpirationdate><expirationdate>2019-01-26</expirationdate><purchasedate>2018-01-26</purchasedate><options><option>pdf</option></options><advanced><conditions><condition>This license is for use on development systems only. It may not be used on staging or productive systems of any kind.</condition></conditions></advanced><signatureinformation><signdate>2018-01-26 11:18</signdate><signature>302d021473bc6def8c7e0b2631a03a6c194237303638a68302150081f7737eca6f2669e2441526f2bbba9d4912089b</signature><checksum>3334</checksum></signatureinformation></license>");
	}

	public static String get(String name) {
		if (defaultVars == null || defaultVars.size() == 0) {
			init();
		}

		String result = System.getenv(name);
		if (result == null) {
			result = defaultVars.get(name);
			Log.debug("Warning: Env Var. " + name + " uses default.");
		}
		return result;
	}

}
