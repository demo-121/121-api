package com.eab.dao;

import org.json.JSONObject;

import com.eab.database.jdbc.DataType;

public class API_AGT_PROXY_HIST extends BaseDao{

	public boolean create(String audID, String proxy1, String proxy2, String startDate, String endDate, String userId)
			throws Exception {
		
		init();

		String sqlStatement = "insert into API_AGT_PROXY_HIST(AUD_ID, PROXY1, PROXY2, START_DT, END_DT, USER_ID, UPDATE_DT)"
				+ " values(" + dbm.param(audID, DataType.TEXT) + ", " + dbm.param(proxy1, DataType.TEXT) + ", "
				+ dbm.param(proxy2, DataType.TEXT) 
				+ ", to_date(" 
				+ dbm.param(startDate, DataType.TEXT) 
				+ ", 'yyyy-MM-dd'), to_date("
				+ dbm.param(endDate, DataType.TEXT) 
				+ ", 'yyyy-MM-dd'), " + dbm.param(userId, DataType.TEXT) + ", sysdate" + ")";

		return insert(sqlStatement);
	}

	public JSONObject selectLastRecord(String userId) throws Exception {
		init();
		
		String sqlStatement = "SELECT PROXY1, PROXY2, to_char( cast(START_DT as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as START_DT, to_char( cast(END_DT as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as END_DT, USER_ID FROM API_AGT_PROXY_HIST WHERE USER_ID = "
				+ dbm.param(userId, DataType.TEXT)
				+ " AND UPDATE_DT IN (SELECT MAX(UPDATE_DT) FROM API_AGT_PROXY_HIST WHERE USER_ID = "
				+ dbm.param(userId, DataType.TEXT) + ")";

		return selectSingleRecord(sqlStatement);

	}

}
