package com.eab.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class API_EMAIL_TRX {
	public boolean create(String audID, String from, String to, String subject, String cc, String filenames, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "INSERT INTO API_EMAIL_TRX(aud_id, mail_from, mail_to, mail_subj, mail_cc, attach_files) "
					+ "values("
					+ "" + dbm.param(audID, DataType.TEXT)
					+ ", " + dbm.param((from!=null && from.length()>0)?from:"", DataType.TEXT)
					+ ", " + dbm.param((to!=null && to.length()>0)?to:"", DataType.TEXT)
					+ ", " + dbm.param((subject!=null && subject.length()>0)?subject:"", DataType.TEXT)
					+ ", " + dbm.param((cc!=null && cc.length()>0)?cc:"", DataType.TEXT)
					+ ", " + dbm.param((filenames!=null && filenames.length()>0)?filenames:"", DataType.TEXT)
					+ ")";
			result = dbm.insert(sql, conn);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
	
	public boolean update(String audID, String status, String reason, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "UPDATE API_EMAIL_TRX"
					+ " SET"
					+ " status=" + dbm.param(status, DataType.TEXT)
					+ ", fail_reason=" + dbm.param(reason, DataType.TEXT)
					+ " WHERE aud_id=" + dbm.param(audID, DataType.TEXT)
					;
			
			int count = dbm.update(sql, conn);
			if (count == 1)
				result = true;
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
}
