package com.eab.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class API_EMS_SMS {
	public boolean create(String audID, String header, String body, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "insert into API_EMS_SMS (aud_id, req_header, req_body) "
					+ "values ("
					+ " " + dbm.param(audID, DataType.TEXT)
					+ ", " + dbm.param(header, DataType.CLOB)
					+ ", " + dbm.param(body, DataType.CLOB)
					+ ")";
			result = dbm.insert(sql, conn);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
	
	public boolean update(String audID, String header, String body, String statusCD, String status, String failReason, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "update API_EMS_SMS"
					+ " set RES_HEADER=" + dbm.param(header, DataType.CLOB)
					+ ", RES_BODY=" + dbm.param(body, DataType.CLOB)
					+ ", STATUS_CODE=" + dbm.param(statusCD, DataType.TEXT)
					+ ", STATUS=" + dbm.param(status, DataType.TEXT)
					+ ", FAIL_REASON=" + dbm.param(failReason, DataType.TEXT)
					+ " where AUD_ID=" + dbm.param(audID, DataType.TEXT)
					;
			
			int count = dbm.update(sql, conn);
			if (count == 1)
				result = true;
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
}
