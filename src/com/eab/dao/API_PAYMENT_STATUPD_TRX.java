package com.eab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.eab.common.Log;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class API_PAYMENT_STATUPD_TRX extends BaseDao {
	public boolean create(String audID, String trxNo, Connection conn) throws SQLException, Exception {
		return create(audID, trxNo, null, null, conn);
	}
	
	public boolean create(String audID, String trxNo, String updateResult, String remark, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		try {
			String sql = "INSERT INTO API_PAYMENT_STATUPD_TRX "
					+ "(aud_id, trx_no"
					+ ((updateResult != null)?", result":"")
					+ ((remark != null)?", remark":"")
					+ ") "
					+ "values(" + dbm.param(audID, DataType.TEXT) 
					+ ", " + dbm.param(trxNo, DataType.TEXT)
					+ ((updateResult != null)?","+dbm.param(updateResult, DataType.TEXT):"")
					+ ((remark != null)?"," + dbm.param(remark, DataType.TEXT):"")
					+ ")";
			
			result = dbm.insert(sql, conn);
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return result;
	}
	

	public boolean updateStatus(String audId, String updateResult, String remark, Connection conn)
			throws Exception {
		DBManager dbm = new DBManager();
		boolean result = false;

		try {
			String sql = "UPDATE API_PAYMENT_STATUPD_TRX" + " SET" + " RESULT=" + dbm.param(updateResult, DataType.TEXT) + ", remark="
					+ dbm.param(StringUtils.isNotEmpty(remark) ? remark : "", DataType.TEXT)
							+ " WHERE aud_id=" + dbm.param(audId, DataType.TEXT);
			
			int count = dbm.update(sql, conn);
			if (count == 1)
				result = true;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return result;
	}


}
