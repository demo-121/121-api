package com.eab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Log;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class API_PAYMENT_TRX extends BaseDao {
	public boolean create(String audID, String trxNo, String appId, String policyNo, String lob, String method, String currency,
			double amount, Connection conn, String relatedPolNumbers, String status) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		DecimalFormat df =  new DecimalFormat("####0.00");

		try {
			String sql = "INSERT INTO API_PAYMENT_TRX(aud_id, trx_no, app_id, policy_no, pol_type, method, currency, amount, related_policy_numbers, status) "
					+ "values(" + dbm.param(audID, DataType.TEXT) + ", " + dbm.param(trxNo, DataType.TEXT) + ", "
					+ dbm.param(appId, DataType.TEXT) + ", "
					+ dbm.param(policyNo, DataType.TEXT) + ", "
					+ dbm.param(lob, DataType.TEXT) + ", "
					+ dbm.param((method != null && method.length() > 0) ? method : "crCard", DataType.TEXT) + ", "
					+ dbm.param((currency != null && currency.length() > 0) ? currency : "SGD", DataType.TEXT) + ", "
					+ dbm.param(df.format(amount), DataType.TEXT) + ", "
					+ dbm.param(relatedPolNumbers != null ? relatedPolNumbers : null, DataType.CLOB) + ", "
					+ dbm.param(status, DataType.TEXT)
					+ ")";
			result = dbm.insert(sql, conn);
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return result;
	}

	public boolean updateStatus(String trxNo, String status, String remark, String body, String amt, String cur, Connection conn)
			throws Exception {
		DBManager dbm = new DBManager();
		boolean result = false;

		try {
			String sql = "UPDATE API_PAYMENT_TRX" + " SET" + " status=" + dbm.param(status, DataType.TEXT) + ", remark="
					+ dbm.param(StringUtils.isNotEmpty(remark) ? remark : "", DataType.TEXT) + ", details="
					+ dbm.param(StringUtils.isNotEmpty(body) ? body : "", DataType.CLOB) 
					+ " WHERE trx_no=" + dbm.param(trxNo, DataType.TEXT);
			
			if (amt != null) {
				sql+= " AND amount=" + dbm.param(Double.parseDouble(amt), DataType.DOUBLE);
			}
			if (cur != null) {
				sql+= " AND currency=" + dbm.param(cur, DataType.TEXT);
			}

			int count = dbm.update(sql, conn);
			if (count == 1)
				result = true;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return result;
	}

	public JsonObject retrieveByTrxNo(String trxNo, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		JsonObject json = null;

		try {
			String sql = "SELECT trx_no, app_id, policy_no, method, currency, amount, from_tz(\"TRX_TIMESTAMP\", 'UTC') as TRX_TIMESTAMP, status, remark, details from API_PAYMENT_TRX"
					+ " WHERE trx_no=" + dbm.param(trxNo, DataType.TEXT);
			
			ResultSet result = dbm.select(sql, conn);
			if (result.next()) {				
				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				cal.setTimeInMillis(result.getTimestamp("TRX_TIMESTAMP").getTime());
				Date date = cal.getTime();
				

				json = new JsonObject();
				json.addProperty("trxNo", result.getString("trx_no"));
				json.addProperty("appId", result.getString("app_id"));
				json.addProperty("policyNo", result.getString("policy_no"));
				json.addProperty("method", result.getString("method"));
				json.addProperty("currency", result.getString("currency"));
				json.addProperty("amount", result.getString("amount"));
				json.addProperty("timestamp", date.getTime());
				json.addProperty("status", result.getString("status"));
				json.addProperty("remark", result.getString("remark"));

				String details = result.getString("details");

				if (StringUtils.isNotEmpty(details)) {
					Gson gson = new GsonBuilder().setPrettyPrinting().create();

					JsonObject detJson = gson.fromJson(details, JsonObject.class);
					double amt = Double.parseDouble(detJson.get("TM_DebitAmt").getAsString());
					String cur = detJson.get("TM_Currency").getAsString();
					String paytype = detJson.get("TM_PaymentType").getAsString();
					String receiptNo = detJson.has("TM_ApprovalCode")? detJson.get("TM_ApprovalCode").getAsString() : "";

					json.addProperty("trxAmount", amt);
					json.addProperty("trxCcy", cur);
					json.addProperty("trxPayType", paytype);
					json.addProperty("receiptNo", receiptNo);
				}
			}
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return json;
	}

	public JSONObject selectPaymentRecordByTrxNo(String trxNo) throws Exception {
		init();
		
		String sqlStatement = "select APP_ID, POLICY_NO, CURRENCY, AMOUNT, POL_TYPE, RELATED_POLICY_NUMBERS, METHOD from API_PAYMENT_TRX where TRX_NO = " + dbm.param(trxNo, DataType.TEXT);

		return selectSingleRecord(sqlStatement);

	}
	
	public JSONArray selectPaymentRecordsByPolicyNumbers(String[] policyNumbers) throws Exception {
		init();
		
		String conditionString = "";
		
		for(String policyNo : policyNumbers) {
			conditionString+= dbm.param(policyNo, DataType.TEXT);
			conditionString+=",";
		}
		
		if(conditionString.length()>0) { // remove last comma
			conditionString = conditionString.substring(0, conditionString.length() - 1);
		}
		
		String sqlStatement = "SELECT a.TRX_NO, a.APP_ID, a.POLICY_NO, a.CURRENCY, a.AMOUNT, a.POL_TYPE, a.TRX_TIMESTAMP, a.METHOD FROM API_PAYMENT_TRX a, (SELECT MAX(TRX_TIMESTAMP) AS TRX_TIMESTAMP,POLICY_NO FROM API_PAYMENT_TRX WHERE POLICY_NO IN (" + conditionString + ")  GROUP BY POLICY_NO) b WHERE a.POLICY_NO = b.POLICY_NO AND a.TRX_TIMESTAMP = b.TRX_TIMESTAMP";
		
		return selectMultipleRecords(sqlStatement);

	}

	public boolean updateStatus(String trxNo, String trxStatus, String remark, String body) throws Exception {
		init();

		String sqlStatement = "UPDATE API_PAYMENT_TRX SET STATUS = " 
		+ dbm.param(trxStatus, DataType.TEXT) 
		+ ", REMARK =" + dbm.param(remark, DataType.TEXT) 
		+ ", DETAILS =" + dbm.param(body, DataType.CLOB) 
		+ " WHERE TRX_NO = "+ dbm.param(trxNo, DataType.TEXT);

		return update(sqlStatement);
	}
}
