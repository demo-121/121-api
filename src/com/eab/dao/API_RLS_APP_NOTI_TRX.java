package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class API_RLS_APP_NOTI_TRX extends BaseDao {

	public boolean create(String audID, String headerAsString, String bodyAsString) throws Exception {

		init();

		String sqlStatement = "insert into API_RLS_APP_NOTI_TRX(AUD_ID, HEADER, PARAM, CREATE_DT)" + " values("
				+ dbm.param(audID, DataType.TEXT) + ", " + dbm.param(headerAsString, DataType.CLOB) + ", "
				+ dbm.param(bodyAsString, DataType.CLOB) + ", sysdate" + ")";

		return insert(sqlStatement);
	}

	public boolean update(String audID, String responseHeaderAsString, String responseAsString, boolean isCompleted)
			throws Exception {

		init();

		String status = isCompleted ? "C" : "F";

		String sqlStatement = "UPDATE API_RLS_APP_NOTI_TRX SET RESP_HEADER = "
				+ dbm.param(responseHeaderAsString, DataType.CLOB) + " , RESPONSE = "
				+ dbm.param(responseAsString, DataType.TEXT) + " , STATUS = " + dbm.param(status, DataType.TEXT)
				+ " WHERE AUD_ID = " + dbm.param(audID, DataType.TEXT);

		return update(sqlStatement);
	}

}