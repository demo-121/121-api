package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class API_RLS_PYMT_STATUS extends BaseDao{
	
	public boolean create(String audID, String policyNumber, String status, String source) throws Exception {

		init();

		String sqlStatement = "insert into API_RLS_PYMT_STATUS(AUD_ID, POL_NUM, STATUS, SOURCE, CREATE_DT)" + " values("
				+ dbm.param(audID, DataType.TEXT) 
				+ ", " + dbm.param(policyNumber, DataType.TEXT)
				+ ", " + dbm.param(status, DataType.TEXT) 
				+ ", " + dbm.param(source, DataType.TEXT) + ", sysdate" + ")";

		return insert(sqlStatement);

	}
}
