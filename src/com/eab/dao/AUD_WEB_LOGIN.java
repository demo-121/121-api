package com.eab.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.eab.common.Log;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class AUD_WEB_LOGIN extends BaseDao {

	public boolean createLoginLog(long timestamp, String userId, String token, boolean success, String version, String build, 
			String timeDiff, String userIp, String serverIp, String device, String browser) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(timestamp);
		Date date = cal.getTime();
		java.sql.Timestamp accessDT = new java.sql.Timestamp(date.getTime());  
		String sql = null;
		try {
			sql = "INSERT INTO AUD_WEB_LOGIN (user_id, token, login_dt, login_success, app_ver, app_build, time_diff, user_ip, server_ip, device, browser) "
					+ " VALUES (" 
					+ dbm.param(userId, DataType.TEXT) + ", "
					+ dbm.param(token, DataType.TEXT) + ", "
					+ dbm.param(accessDT, DataType.TIMESTAMP) + ", "
					+ dbm.param(success?"S":"F", DataType.TEXT) + ", "
					+ dbm.param(version, DataType.TEXT) + ", "
					+ dbm.param(build, DataType.TEXT) + ", "
					+ dbm.param(timeDiff, DataType.TEXT) + ", "
					+ dbm.param(userIp, DataType.TEXT) + ", "
					+ dbm.param(serverIp, DataType.TEXT) + ", "
					+ dbm.param(device, DataType.TEXT) + ", "
					+ dbm.param(browser, DataType.TEXT) 
					+ ")";
			result = dbm.insert(sql);
			
		} catch (SQLException e) {
			Log.error("createLoginLog exp:" + e.getMessage() + " - " + sql);
			throw e;
		} catch (Exception e) {
			Log.error("createLoginLog exp:" + e.getMessage() + " - " + sql);
			throw e;
		} finally {
			dbm = null;
		}

		return result;
	}

	public boolean updateLoginLog(long timestamp, String userId, String token) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		int result = 0;
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(timestamp);
		Date date = cal.getTime();
		java.sql.Timestamp accessDT = new java.sql.Timestamp(date.getTime());  
		
		try {
			String sql = "UPDATE AUD_WEB_LOGIN SET"
					+ " logout_dt = "+ dbm.param(accessDT, DataType.TIMESTAMP) 
					+ " WHERE"
					+ " user_id = "+ dbm.param(userId, DataType.TEXT) + " AND "
					+ " token = " + dbm.param(token, DataType.TEXT);
			result = dbm.update(sql);
			
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			dbm = null;
		}

		return result > 0;
	}
}
