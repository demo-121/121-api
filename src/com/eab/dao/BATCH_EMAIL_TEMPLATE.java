package com.eab.dao;

import org.json.JSONObject;

import com.eab.database.jdbc.DataType;

public class BATCH_EMAIL_TEMPLATE extends BaseDao {
	
	public JSONObject selectEmailTemplate(String id) throws Exception{
		init();
		
		String sqlStatement = "select MAIL_SUBJ, MAIL_CONTENT from BATCH_EMAIL_TEMPLATE where ID =" +dbm.param(id, DataType.TEXT);
		
		return selectSingleRecord(sqlStatement);
	}

}
