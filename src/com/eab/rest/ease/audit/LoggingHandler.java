package com.eab.rest.ease.audit;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.eab.dao.AUD_WEB_ACCESS;
import com.eab.dao.AUD_WEB_LOGIN;

@Path("/audit")
public class LoggingHandler {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/login")
	public void loginLog(String body) {
		if (StringUtils.isNoneEmpty(body)) {
			JSONObject data = new JSONObject(body);
			
			AUD_WEB_LOGIN audDao = new AUD_WEB_LOGIN();
			try {
				audDao.createLoginLog(
						data.has("loginDate")?data.getLong("loginDate"):0,
						data.has("profileId")?data.getString("profileId"):"", 
						data.has("loginToken")?data.getString("loginToken"):"", 
						data.has("loginSuccess")?data.getBoolean("loginSuccess"):false, 
						data.has("version")?data.getString("version"):"", 
						data.has("build")?data.getString("build"):"", 
						data.has("timeDiff")?data.getInt("timeDiff")+"":"", 
						data.has("userIPAddr")?data.getString("userIPAddr"):"", 
						data.has("servIPAddr")?data.getString("servIPAddr"):"", 
						data.has("device")?data.getString("device"):"", 
						data.has("browser")?data.getString("browser"):"");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/logout")
	public void logoutLog(String body) {
		if (StringUtils.isNoneEmpty(body)) {
			JSONObject data = new JSONObject(body);
			
			AUD_WEB_LOGIN audDao = new AUD_WEB_LOGIN();
			try {
				audDao.updateLoginLog(
						data.has("logoutDate")?data.getLong("logoutDate"):0, 
						data.has("profileId")?data.getString("profileId"):"", 
						data.has("loginToken")?data.getString("loginToken"):"");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/access")
	public void accessLog(@Context HttpHeaders headers, String body) {
		
		if (StringUtils.isNoneEmpty(body)) {
			JSONObject data = new JSONObject(body);
			
			AUD_WEB_ACCESS audDao = new AUD_WEB_ACCESS();
			try {
				audDao.createAccessLog(
						data.has("timestamp")?data.getLong("timestamp"):0, 
						data.has("profileId")?data.getString("profileId"):"", 
						data.has("action")?data.getString("action"):"", 
						data.has("token")?data.getString("token"):"", 
						data.has("extraData")?data.getString("extraData"):"");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
		
	}
}
