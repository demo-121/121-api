package com.eab.rest.ease.payment;

import java.util.Arrays;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_PAYMENT_TRX;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Path("/payment-record/resubmission")
public class PaymentResubmissionController {
	
	@POST
	@Path("")
	@Consumes("application/json")
	@Produces("application/json")
	public Response resubmission(@Context HttpHeaders headers, String body) {
		Log.info("****Payment resubmission");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter
		
		Response output = null;
		
		String errorString = "";
		
		try {

			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate Json format
			
			if(bodyJson!=null) {
				if(bodyJson.has("policyNumbers")) {
					JsonArray policyNumbers = bodyJson.get("policyNumbers").getAsJsonArray();
					if(policyNumbers != null) {
						if(policyNumbers.size()>0) {
							
							String[] policyNoStringArray = new String[policyNumbers.size()];
							
							for(int i= 0; i<policyNumbers.size();i++) {
								JsonElement element = policyNumbers.get(i);
								String policyNo = element.getAsString();
								policyNoStringArray[i] = policyNo;
							}
							
							API_PAYMENT_TRX trxPayment = new API_PAYMENT_TRX();
							JSONArray paymentJsonArray = trxPayment.selectPaymentRecordsByPolicyNumbers(policyNoStringArray);
							
							JsonArray resultJsonArray = new JsonArray();
							
							boolean isFail = false;
							
							if(paymentJsonArray !=null && paymentJsonArray.length()>0) {
								String[] policyNoCheckingArray = new String[paymentJsonArray.length()];
								

								Log.debug("***paymentJsonArray.length()="+paymentJsonArray.length());
								for(int i= 0; i<paymentJsonArray.length();i++) {
									
									JSONObject paymentRecord = (JSONObject) paymentJsonArray.get(i);
									
									String policyNO = paymentRecord.has("POLICY_NO")?paymentRecord.getString("POLICY_NO"):null;
									String amount = paymentRecord.has("AMOUNT")?paymentRecord.get("AMOUNT").toString():null;
									String currency = paymentRecord.has("CURRENCY")?paymentRecord.getString("CURRENCY"):null;
									String lob = paymentRecord.has("POL_TYPE")?paymentRecord.getString("POL_TYPE"):null;
									String method = paymentRecord.has("METHOD")?paymentRecord.getString("METHOD"):null;
									
									policyNoCheckingArray[i]= policyNO;
									
									JsonObject recordPaymentJson = PaymentSubmissionUtil.createRecordPaymentJson(policyNO, amount, currency, lob, PaymentSubmissionUtil.convertPaymentRemarksByPayMehtod(method), true);
									
									HttpPost postRequest = PaymentSubmissionUtil.createRecordPaymentPostRequest(recordPaymentJson);
									
									Response recordPaymentOutput = HttpUtil.send(postRequest);
									String responseAsString = recordPaymentOutput.getEntity().toString();
									
									JsonObject resultJsonNode = new JsonObject();
									resultJsonNode.addProperty("policyNo", policyNO);
									resultJsonNode.addProperty("success", true);
									resultJsonNode.addProperty("message", responseAsString);
									
									if (recordPaymentOutput.getStatus() != 202) { // 202 = success
										isFail = true;
										resultJsonNode.addProperty("success", false);
									}
									resultJsonArray.add(resultJsonNode);
									
								}
								
								if(policyNoCheckingArray.length != policyNoStringArray.length) {// check missing policy numbers in DB
									for(String policyNo : policyNoStringArray) {
										boolean isExistingInDB = Arrays.asList(policyNoCheckingArray).contains(policyNo);
										
										if(!isExistingInDB) {
											isFail = true;
											
											JsonObject resultJsonNode = new JsonObject();
											resultJsonNode.addProperty("policyNo", policyNo);
											resultJsonNode.addProperty("success", false);
											resultJsonNode.addProperty("message", "Does not exist in DB");
											resultJsonArray.add(resultJsonNode);
										}
									}
								}
								
							}else{
								errorString+="could not find any payment records in DB";
							}
							isFail = true;
							
							JsonObject resultJsonObject = new JsonObject();
							resultJsonObject.add("result", resultJsonArray);
							
							Status status = Response.Status.OK;
							
							if(isFail) {
								status = Response.Status.BAD_REQUEST;
							}
							
							output = RestUtil.toResponse(audID, status,
									resultJsonObject.toString());
							
						}else {
							errorString+="policyNumbers is empty";
						}
					}else {
						errorString+="policyNumbers is null";
					}
					
				}else {
					errorString+="Missing policyNumbers;";
				}
			}else {
				errorString+="Json Syntax Incorrect;";
			}
			
			if(errorString.length()>0) {
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
						RestUtil.genErrorJsonStr(false, errorString));
			}
			
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		
		return output;
	}

}
