package com.eab.rest.ease.payment;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class PaymentSubmissionUtil {
	
	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private static final String SERVICE_PATH = "/payment-record";
	
	public static HttpPost createRecordPaymentPostRequest(JsonObject jsonBody) throws Exception {
		Log.info("Goto PaymentSubmissionUtil.createApplicationPostRequest");

		String recordPaymentURL = DOMAIN + SERVICE_PATH;

		HttpPost request = new HttpPost(recordPaymentURL);
		request.addHeader("Content-Type", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

	public static JsonObject createRecordPaymentJson(String policyNO, String amount, String currency, String lob, String paymentRemarks, boolean isForce) throws Exception {
		Log.info("Goto PaymentSubmissionUtil.createRecordPaymentJson");

		JsonObject jsonBody = new JsonObject();

		JsonObject recordPaymentRequest = new JsonObject();
		recordPaymentRequest.addProperty("policyNO", policyNO);
		recordPaymentRequest.addProperty("depositAMT", amount);
		recordPaymentRequest.addProperty("currencyCD", currency);

		JsonArray hasLifePolicyTransactionDetailsIn = new JsonArray();
		JsonObject hasPolicyPaymentTransactionDetailsIn = new JsonObject();
		hasPolicyPaymentTransactionDetailsIn.addProperty("transTypeCD", "1"); // Hardcode to be "1"
		hasPolicyPaymentTransactionDetailsIn.addProperty("transCurrencyCD", currency);

		JsonArray hasLifePolicyTransactionHeaderDetailsIn = new JsonArray();
		JsonObject hasLifePolicyTransactionHeaderDetailsInObject = new JsonObject();

		String bankAccountNO = lob.equals("Health") ? "L-CHKS" : "L-CHK";
		hasLifePolicyTransactionHeaderDetailsInObject.addProperty("bankAccountNO", bankAccountNO);
		hasLifePolicyTransactionHeaderDetailsInObject.addProperty("paymentAMT", amount);
		if(paymentRemarks != null) {
			hasLifePolicyTransactionHeaderDetailsInObject.addProperty("paymentSourceCD", paymentRemarks);
		}

		hasLifePolicyTransactionHeaderDetailsIn.add(hasLifePolicyTransactionHeaderDetailsInObject);

		hasPolicyPaymentTransactionDetailsIn.add("hasLifePolicyTransactionHeaderDetailsIn",
				hasLifePolicyTransactionHeaderDetailsIn);

		hasLifePolicyTransactionDetailsIn.add(hasPolicyPaymentTransactionDetailsIn);
		recordPaymentRequest.add("hasLifePolicyTransactionDetailsIn", hasLifePolicyTransactionDetailsIn);

		jsonBody.add("recordPaymentRequest", recordPaymentRequest);
		jsonBody.addProperty("lob", lob != null ? lob : "Life");
		
		if(isForce) {
			jsonBody.addProperty("isForce", true);
		}

		return jsonBody;
	}
	
	public static String convertPaymentRemarksByPayMehtod(String payMethod) {
		String paymentRemarks = null;
		
		if(payMethod !=null) {
			switch(payMethod) {
			case "crCard":
				paymentRemarks ="ECC";
				break;
			case "eNets":
				paymentRemarks ="ENT";
				break;
			case "dbsCrCardIpp":
				paymentRemarks ="IPP";
				break;
			
			}
		}
		
		return paymentRemarks;
		
	}

}
