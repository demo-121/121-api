package com.eab.rest.ease.payment;

import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class WireCardCallbackService {

	public String convertStatus(String providerStatus, String errorCode) {
		String status = null;

		if (providerStatus != null) {
			switch (providerStatus) {
			case "YES":
				status = "Y";
				break;
			case "NO":
				status = "N";
				if ( "9225".equals(errorCode) ) {
					status = "I";
				} else if ("9203".equals(errorCode) ) {
					status = "C";
				}
				break;
			case "RDR":
			case "NA":
			case "INPROGRESS":
			default:
				status = "I";
				break;
			}
		}

		return status;
	}
	
	public boolean updateApplication(String appId, JSONObject application, JSONObject originalPayment, JSONObject paymentResult, String status) {
		try {
			JSONObject mergedPayment = Function.merge2JSONObject(originalPayment, paymentResult);
			application.put("payment", mergedPayment);
			boolean isInitialPaymentCompleted;
	        if (status.equalsIgnoreCase("YES")) {
	        	isInitialPaymentCompleted = true;
	        	application.put("isInitialPaymentCompleted", true);
	        } else {
	        	isInitialPaymentCompleted = false;
	        	application.put("isInitialPaymentCompleted", false);
	        }
	        
	        boolean isShield = Function.transformEleToString(Function.getAsPath(Function.transformObject(application), "quotation/quotType")).equals("SHIELD");
	        
	        if (isShield) {
	        	return updateMasterandChildApplication(appId, application, paymentResult, isInitialPaymentCompleted);
	        } else {
	        	return Constant.CBUTIL.UpdateDoc(appId, application.toString()) == Constant.CBUTIL.UPDATE_SUCCESS;
	        }
		} catch (Exception e) {
			Log.error("WireCard Update Application error" + e);
			return false;
		}
		
	}
	
	public boolean updateMasterandChildApplication(String appId, JSONObject masterApplication, JSONObject paymentResult, boolean isInitialPaymentCompleted) {
		try {
			boolean updateResult = true;
			// Update Master Application
			if (Constant.CBUTIL.UpdateDoc(appId, masterApplication.toString()) != Constant.CBUTIL.UPDATE_SUCCESS) {
				updateResult = false;
			}
			
			// Update Child Application
			JsonArray childIds = Function.getAsPath(Function.transformObject(masterApplication), "childIds").getAsJsonArray();
			JSONObject childApplication;
			for (JsonElement ele : childIds) {
				String childAppId = ele.getAsString();
				childApplication = Constant.CBUTIL.getDoc(childAppId);
				if (childApplication != null ) {
					JSONObject payment = childApplication.has("payment") ? childApplication.getJSONObject("payment"):null;
					JSONObject mergedPayment = Function.merge2JSONObject(payment, paymentResult);
					childApplication.put("payment", mergedPayment);
					childApplication.put("isInitialPaymentCompleted", isInitialPaymentCompleted);
					if (Constant.CBUTIL.UpdateDoc(childAppId, childApplication.toString()) != Constant.CBUTIL.UPDATE_SUCCESS) {
						updateResult = false;
					}
				}
			}
			
			return updateResult;
		} catch (Exception e) {
			Log.error("WireCard Update Shield Application error" + e);
			return false;
		}
		
	}

}
