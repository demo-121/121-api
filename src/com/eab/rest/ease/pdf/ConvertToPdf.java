package com.eab.rest.ease.pdf;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.time.StopWatch;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.realobjects.pdfreactor.Configuration;
import com.realobjects.pdfreactor.Result;

@Path("/pdf")
public class ConvertToPdf {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/html2pdf")
	public Response sendSMS(@Context HttpHeaders headers, String body) {
		Response output = null;
		boolean hasError = false;
		String msgError = "";
		String html = null;
		JsonElement jelem = null;
		JsonObject jobj = null;
		Result pdfResult = null;
		StopWatch stopwatch = new StopWatch();
		Configuration config = new Configuration();
		Gson gson = new Gson();
		Pdf pdf = new Pdf();
		
		Log.info("Landing /html2pdf");
		String audID = headers.getRequestHeader("aud-id").get(0);	// provided by filter
		
		String license = EnvVariable.get("PDFREACTOR_LICENSE");
		
		try {
			//TODO: Create Transaction Log into Database
			
			
			// Convert JSON Body to Object
			if (!hasError) {
				jelem = gson.fromJson(body, JsonElement.class);
				Log.debug("Is JSON Object? " + jelem.isJsonObject());
				Log.debug("Is JSON Array? " + jelem.isJsonArray());
				if (jelem != null && jelem.isJsonObject()) {
					jobj = jelem.getAsJsonObject();
					if (jobj == null) {
						hasError = true;
						msgError += "INVALID BODY;";
					}
				} else {
					hasError = true;
					msgError += "EMPTY BODY;";
				}
			}
			
			// Get known JSON objects
			if (!hasError) {
				if (jobj.get("html") != null) {
					html = jobj.get("html").getAsString();
				} else {
					hasError = true;
					msgError += "EMPTY HTML Contents;";
				}
			}
			
			// Convert HTML to PDF
			if (!hasError) {
				if (license != null && license.length() > 0) {
					config.setLicenseKey(license);
				} else {
					Log.info("[Html2Pdf] PDFReactor License is missing.");
				}
				
				config.setDocument(html);
				stopwatch.start();
				pdfResult = Constant.pdfReactor.convert(config);
				stopwatch.stop();
				pdf.pdf = Base64.encodeBase64String(pdfResult.getDocument());
				pdf.id = audID;
				Log.info("[Html2Pdf] elapsed: " + stopwatch.getTime() + "ms");
				
				output = RestUtil.toResponse(audID, Response.Status.OK, gson.toJson(pdf));
			}
			
			if (hasError) {
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, msgError));
			}
		} catch(Exception ex) {
			Log.error(ex);
			hasError = true;
			msgError = ex.getMessage();
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, msgError));
		} finally {
			jelem = null;
			jobj = null;
			pdfResult = null;
			stopwatch = null;
			config = null;
			gson = null;
			pdf = null;
		}
		
		// Reply the response to caller
		return output;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/xslttrans")
	public Response transformXslt(@Context HttpHeaders headers, String body) {
		Response output = null;
		boolean hasError = false;
		String msgError = "";
		String xslt = null;
		String xml = null;
		JsonElement jelem = null;
		JsonObject jobj = null;
		StopWatch stopwatch = new StopWatch();
		Gson gson = new Gson();
		StringReader reader = null;
		StringWriter writer = null;
        TransformerFactory factory = null;
        Transformer transformer = null;
        XHtml xhtml = null;
		
		Log.info("Landing /xslttrans");
		String audID = headers.getRequestHeader("aud-id").get(0);	// provided by filter
		try {
			//TODO: Create Transaction Log into Database
			
			
			// Convert JSON Body to Object
			if (!hasError) {
				jelem = gson.fromJson(body, JsonElement.class);
				Log.debug("Is JSON Object? " + jelem.isJsonObject());
				Log.debug("Is JSON Array? " + jelem.isJsonArray());
				if (jelem != null && jelem.isJsonObject()) {
					jobj = jelem.getAsJsonObject();
					if (jobj == null) {
						hasError = true;
						msgError += "INVALID BODY;";
					}
				} else {
					hasError = true;
					msgError += "EMPTY BODY;";
				}
			}
			
			// Get known JSON objects
			if (!hasError) {
				if (jobj.get("xslt") != null) {
					xslt = jobj.get("xslt").getAsString();
				} else {
					hasError = true;
					msgError += "EMPTY XSLT Contents;";
				}
				if (jobj.get("xml") != null) {
					xml = jobj.get("xml").getAsString();
				} else {
					hasError = true;
					msgError += "EMPTY XML Contents;";
				}
			}
			
			// Transform XSLT and XML to XHTML
			if (!hasError) {
				stopwatch.start();
				reader = new StringReader(xml);
	            writer = new StringWriter();
	            factory = TransformerFactory.newInstance();
	            transformer = factory.newTransformer(new StreamSource(new ByteArrayInputStream(xslt.getBytes("UTF-8"))));
	            
	            transformer.transform(new StreamSource(reader), new StreamResult(writer));
	            stopwatch.stop();
				Log.info("[Java|transformXslt] Elpased: " + stopwatch.getTime() + " ms");
				
				xhtml = new XHtml();
				xhtml.xhtml = writer.toString();
				xhtml.id = audID;
	            
				output = RestUtil.toResponse(audID, Response.Status.OK, gson.toJson(xhtml));
			}
			
			if (hasError) {
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, RestUtil.genErrorJsonStr(false, msgError));
			}
		} catch(Exception ex) {
			Log.error(ex);
			hasError = true;
			msgError = ex.getMessage();
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, msgError));
		} finally {
			jelem = null;
			jobj = null;
			stopwatch = null;
			gson = null;
			reader = null;
			writer = null;
	        factory = null;
	        transformer = null;
		}
		
		// Reply the response to caller
		return output;
	}
}
