package com.eab.rest.ease.policynum;

public class PolicyNumBean {
	
	public PolicyParam retrievePolicyNumberRequest;
	
	public PolicyNumBean() {
		retrievePolicyNumberRequest = new PolicyParam();
	}
	
	public class PolicyParam {
		public String companyCD;
		public String requestedPolicyNO;
	}
}
