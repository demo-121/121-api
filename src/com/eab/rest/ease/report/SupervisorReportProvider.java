package com.eab.rest.ease.report;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import com.eab.common.Constant;
import com.eab.common.Log;

public class SupervisorReportProvider {
	
	private final short HEADER_COLOR_INDEX = 57;
	
	private final String SHEET_NAME = "Report";

	private final String[] titles = { "Case Status","Case Number / Policy Number","Case creation date","Agent Case Submission date","Supervisor approval date","Firm approval date","Proposer ICDocument Type","Proposer IC","Proposer Name","Life Assured ICDocument Type","Life Assured IC","Life Assured Name","Proposer Mobile No.","Proposer Email address","Agent Name","Manager Name","Director Name","Case Approved by ","Firm Name","Product Name","Premium amount (Basic plan)","Sum assured (Basic Plan)","Premium Type (RP/SP)","Premium Frequency","Top-up","RSP","RSP frequency","Payment Method","Currency","Rider Name 1","Premium amount (Rider 1)","Sum assured (Rider 1)","Rider Name 2","Premium amount (Rider 2)","Sum assured (Rider2)","Rider Name 3","Premium amount (Rider 3)","Sum assured (Rider 3)","Rider Name 4","Premium amount (Rider 4)","Sum assured (Rider 4)","Rider Name 5","Premium amount (Rider 5)","Sum assured (Rider 5)","Rider Name 6","Premium amount (Rider 6)","Sum assured (Rider 6)","Rider Name 7","Premium amount (Rider 7)","Sum assured (Rider 7)","Rider Name 8","Premium amount (Rider 8)","Sum assured (Rider 8)","Rider Name 9","Premium amount (Rider 9)","Sum assured (Rider 9)","Rider Name 10","Premium amount (Rider 10)","Sum assured (Rider 10)","Rider Name 11","Premium amount (Rider 11)","Sum assured (Rider 11)","Rider Name 12","Premium amount (Rider 12)","Sum assured (Rider 12)" };
	
	public String createEmptyReport(String password) throws Exception {
		
		return createExcelReportInBase64Format(new String[0][], password);
	}
	
	public String createExcelReportInBase64Format(String[][] data, String password) throws Exception {
		Log.info("Goto SingpostSubmissionReportProvider.createExcelReportInBase64Format");

		Biff8EncryptionKey.setCurrentUserPassword(password);
		HSSFWorkbook wb = new HSSFWorkbook();

		Map<String, HSSFCellStyle> styles = createStyles(wb);

		Sheet sheet = wb.createSheet(SHEET_NAME);

		// turn off gridlines
		sheet.setDisplayGridlines(false);
		sheet.setPrintGridlines(false);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);

		// the following three statements are required only for HSSF
		sheet.setAutobreaks(true);
		printSetup.setFitHeight((short) 1);
		printSetup.setFitWidth((short) 1);

		// the header row: centered text in 48pt font
		Row headerRow = sheet.createRow(0);
		headerRow.setHeightInPoints(12.75f);
		for (int i = 0; i < titles.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(titles[i]);
			cell.setCellStyle(styles.get("header"));
		}
		
		// freeze the first 1 rows
		sheet.createFreezePane(0, 1);

		Row row;
		Cell cell;
		int rownum = 1;
		for (int i = 0; i < data.length; i++, rownum++) {
			
			row = sheet.createRow(rownum);
			if (data[i] == null)
				continue;

			for (int j = 0; j < data[i].length; j++) {
				
				cell = row.createCell(j);

				String styleName = "cell_normal";
				if(data[i][j] != null) {
					switch (j) {
//					case 0:
//						cell.setCellValue(Integer.parseInt(data[i][j]));
//						break;
//					case 1:
//						String submittedSqlDateTime = data[i][j];
//						
//						DATE_FORMAT_SQL.setTimeZone(TimeZone.getTimeZone("UTC"));
//						
//						Date parsedDate = DATE_FORMAT_SQL.parse(submittedSqlDateTime);
//						Calendar calendar = Calendar.getInstance();
//						calendar.setTimeZone(TimeZone.getTimeZone(Constant.ZONE_ID));
//						calendar.setTime(parsedDate);
//						
//						cell.setCellValue(calendar.getTime());
//						styleName = "cell_normal_date";
//						
//						break;
//					case 9:
//						cell.setCellValue(Double.parseDouble(data[i][j]));
//						styleName = "cell_number";
//						break;
					default:
						cell.setCellValue(data[i][j]);
						break;
					}
				}
				
				cell.setCellStyle(styles.get(styleName));
			}
		}

		// //group rows for each phase, row numbers are 0-based
		// sheet.groupRow(4, 6);
		// sheet.groupRow(9, 13);
		// sheet.groupRow(16, 18);

		// set column widths, the width is measured in units of 1/256th of a character
		// width
		
		for(int i=0;i<65;i++) {
			sheet.setColumnWidth(i, 256 * 20);
		}
		
		sheet.setZoom(100); // 100% scale

		// Write the output to a file

		// convert outputStream to base64 string
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		wb.write(outputStream);
		byte[] img64 = Base64.encodeBase64(outputStream.toByteArray());
		String b64Str = new String(img64);
		outputStream.close();

		wb.close();

		return b64Str;
	}

	/**
	 * create a library of cell styles
	 */
	private Map<String, HSSFCellStyle> createStyles(HSSFWorkbook wb) {
		Log.info("Goto SingpostSubmissionReportProvider.Map");
		
		Map<String, HSSFCellStyle> styles = new HashMap<>();
		DataFormat df = wb.createDataFormat();

		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HEADER_COLOR_INDEX, (byte) 204, (byte) 236, (byte) 255);// Header color

		HSSFCellStyle style = wb.createCellStyle();
		org.apache.poi.ss.usermodel.Font headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(true);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFillForegroundColor(palette.getColor(HEADER_COLOR_INDEX).getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFont(headerFont);
		style.setWrapText(true);
		styles.put("header", style);

		org.apache.poi.ss.usermodel.Font normalFont = wb.createFont();
		normalFont.setFontHeightInPoints((short) 10);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(normalFont);
		style.setWrapText(true);
		styles.put("cell_normal", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setWrapText(true);
		style.setDataFormat(df.getFormat(Constant.DATETIME_PATTERN_AS_AXA));
		styles.put("cell_normal_date", style);
		
		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.RIGHT);
		style.setWrapText(true);
		style.setDataFormat(df.getFormat("#.00"));
		styles.put("cell_number", style);
		
		style = wb.createCellStyle();
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setWrapText(false);
		style.setFont(normalFont);
		styles.put("cell_report_name", style);

		return styles;
	}

	private HSSFCellStyle createBorderedStyle(HSSFWorkbook wb) {
		Log.info("Goto SingpostSubmissionReportProvider.createBorderedStyle");
		
		BorderStyle thin = BorderStyle.THIN;
		short black = IndexedColors.BLACK.getIndex();

		HSSFCellStyle style = wb.createCellStyle();
		style.setBorderRight(thin);
		style.setRightBorderColor(black);
		style.setBorderBottom(thin);
		style.setBottomBorderColor(black);
		style.setBorderLeft(thin);
		style.setLeftBorderColor(black);
		style.setBorderTop(thin);
		style.setTopBorderColor(black);
		return style;
	}

}
