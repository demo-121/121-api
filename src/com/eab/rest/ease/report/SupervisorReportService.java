package com.eab.rest.ease.report;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.USER_PROFILE;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SupervisorReportService {

	private final String SENDER_ADDRESS = "ease@axa.com.sg";
	private final String RECEIVER_ADDRESS = "darren.auyeung@eabsystems.com";

	private final String FILE_NAME = "Supervisor_Report.xls";

	private final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private final String SERVICE_PATH = "/email/sendEmail";

	private final String APP_BY_POL_VIEW_NAME = "appByPolicyNumber";
	private final String APPROVAL_BY_POL_VIEW_NAME = "approvalByPolicyNumber";
	private final String APP_BY_START_DATE_VIEW_NAME = "appByStartedDate";
	private final String APP_BY_SUB_DATE_VIEW_NAME = "appBySubmittedDate";
	private final String APPROVAL_BY_APPROVAL_DATE_VIEW_NAME = "approvalByApprovalDate";

	public JSONArray getAgencyDownlineUserProfile(String userId) {
		Log.info("Goto SupervisorReportService.getPolicyNumber");

		JSONArray jsonArray = new JSONArray();

		try {
			USER_PROFILE userProfileDao = new USER_PROFILE();
			jsonArray = userProfileDao.selectAgencyDownlineByUserId(userId);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return jsonArray;
	}

	public ArrayList<String> extractAgentCodeArray(JSONArray downlineUserProfileJsonArray) {
		Log.info("Goto SupervisorReportService.downlineUserProfileJsonArray");
		ArrayList<String> arrayList = new ArrayList<String>();

		for (Object item : downlineUserProfileJsonArray) {
			JSONObject jsonObject = (JSONObject) item;
			String agentCode = jsonObject.getString("AGENT_NO");

			arrayList.add(agentCode);
		}

		return arrayList;
	}

	public JsonArray getRecordsOnCouchBase(ArrayList<String> agentCodeArrayList, String dateType, String startTime,
			String endTime) {
		Log.info("Goto SupervisorReportService.getRecordsOnCouchBase");

		LinkedHashMap<String, JsonObject> appJsonMap = new LinkedHashMap<String, JsonObject>();
		LinkedHashMap<String, JsonObject> approvalJsonMap = new LinkedHashMap<String, JsonObject>();

		switch (dateType) {
		case "C":// appByStartedDate
			JSONObject appByStartedDateViewJson = Constant.CBUTIL
					.getDoc("_design/main/_view/" + APP_BY_START_DATE_VIEW_NAME + "?startkey=[\"01\",\"" + startTime
							+ "\"]&endkey=[\"01\",\"" + endTime + "\"]");

			appJsonMap = filterAppJson(convertViewResultToGsonJsonArray(appByStartedDateViewJson), agentCodeArrayList);

			approvalJsonMap = filterApprovalJson(getJsonArrayByJsonMap(APPROVAL_BY_POL_VIEW_NAME, appJsonMap),
					agentCodeArrayList);

			break;
		case "S":// appBySubmittedDate

			JSONObject appBySubmittedDateViewJson = Constant.CBUTIL
					.getDoc("_design/main/_view/" + APP_BY_SUB_DATE_VIEW_NAME + "?startkey=[\"01\",\"" + startTime
							+ "\"]&endkey=[\"01\",\"" + endTime + "\"]");

			appJsonMap = filterAppJson(convertViewResultToGsonJsonArray(appBySubmittedDateViewJson),
					agentCodeArrayList);

			approvalJsonMap = filterApprovalJson(getJsonArrayByJsonMap(APPROVAL_BY_POL_VIEW_NAME, appJsonMap),
					agentCodeArrayList);

			break;
		case "A":// approvalByApprovalDate
			JSONObject approvalByApprovalDateViewJson = Constant.CBUTIL
					.getDoc("_design/main/_view/" + APPROVAL_BY_APPROVAL_DATE_VIEW_NAME + "?startkey=[\"01\",\""
							+ startTime + "\"]&endkey=[\"01\",\"" + endTime + "\"]");

			approvalJsonMap = filterApprovalJson(convertViewResultToGsonJsonArray(approvalByApprovalDateViewJson),
					agentCodeArrayList);

			appJsonMap = filterAppJson(getJsonArrayByJsonMap(APP_BY_POL_VIEW_NAME, approvalJsonMap),
					agentCodeArrayList);

			break;
		default:
			break;
		}

		return mergeJsonArray(dateType, appJsonMap, approvalJsonMap);
	}

	private JsonArray mergeJsonArray(String dateType, LinkedHashMap<String, JsonObject> appJsonMap,
			LinkedHashMap<String, JsonObject> approvalJsonMap) {
		Log.info("Goto SupervisorReportService.mergeJsonArray");

		JsonArray jsonArray = new JsonArray();

		switch (dateType) {
		case "C":// appByStartedDate
		case "S":// appBySubmittedDate

			for (String key : appJsonMap.keySet()) {
				JsonObject mergedJsonObject = new JsonObject();
				
				mergedJsonObject.addProperty("policyNumber", key);

				JsonObject appJsonObject = appJsonMap.get(key);
				JsonObject approvalJsonObject = approvalJsonMap.get(key);

				boolean hasApp = false;

				if (appJsonObject != null) {
					mergedJsonObject.add("app", appJsonObject);
					hasApp = true;
				}

				if (approvalJsonObject != null) {
					mergedJsonObject.add("approval", approvalJsonObject);
				}

				if (hasApp) {
					jsonArray.add(mergedJsonObject);
				}

			}

			break;
		case "A":// approvalByApprovalDate

			for (String key : approvalJsonMap.keySet()) {
				JsonObject mergedJsonObject = new JsonObject();

				JsonObject appJsonObject = appJsonMap.get(key);
				JsonObject approvalJsonObject = approvalJsonMap.get(key);

				boolean hasApp = false;

				if (appJsonObject != null) {
					mergedJsonObject.add("app", appJsonObject);
					hasApp = true;
				}

				if (approvalJsonObject != null) {
					mergedJsonObject.add("approval", approvalJsonObject);
				}

				if (hasApp) {
					jsonArray.add(mergedJsonObject);
				}
			}

			break;
		default:
			break;
		}

		return jsonArray;
	}

	private JsonArray getJsonArrayByJsonMap(String viewName, LinkedHashMap<String, JsonObject> jsonMap) {
		Log.info("Goto SupervisorReportService.getJsonArrayByJsonMap");

		String[][] array = new String[jsonMap.size()][];
		int i = 0;
		for (String key : jsonMap.keySet()) {

			ArrayList<String> keyList = new ArrayList<String>();
			keyList.add("01");
			keyList.add(key);

			array[i] = keyList.toArray(new String[keyList.size()]);
			i++;
		}

		JSONArray tempJsonArray = Constant.CBUTIL.getDocsByView(viewName, array);

		return Function.validateJsonArrayFormatForString(tempJsonArray.toString());
	}

	private LinkedHashMap<String, JsonObject> filterAppJson(JsonArray appJsonArray,
			ArrayList<String> agentCodeArrayList) {
		Log.info("Goto SupervisorReportService.filterAppJson");

		LinkedHashMap<String, JsonObject> appJsonMap = new LinkedHashMap<String, JsonObject>();

		if (appJsonArray != null) {
			// filter by agentCode

			for (JsonElement item : appJsonArray) {
				JsonObject jsonObject = item.getAsJsonObject();

				if (jsonObject.has("value")) {
					JsonObject value = jsonObject.get("value").getAsJsonObject();

					if (value.has("quotation") && value.has("policyNumber")) {
						String policyNumber = value.get("policyNumber").getAsString();
						JsonObject quotation = value.get("quotation").getAsJsonObject();

						if (quotation.has("agent")) {
							JsonObject agent = quotation.get("agent").getAsJsonObject();

							if (agent.has("agentCode")) {
								String agentCode = agent.get("agentCode").getAsString();
								;

								if (agentCodeArrayList.contains(agentCode)) {
									appJsonMap.put(policyNumber, jsonObject);
								}
							}
						}
					}
				}
			}
		}

		return appJsonMap;
	}

	private LinkedHashMap<String, JsonObject> filterApprovalJson(JsonArray approvalJsonArray,
			ArrayList<String> agentCodeArrayList) {
		Log.info("Goto SupervisorReportService.filterApprovalJson");

		LinkedHashMap<String, JsonObject> approvalJsonMap = new LinkedHashMap<String, JsonObject>();

		if (approvalJsonArray != null) {
			// filter by agentCode

			for (JsonElement item : approvalJsonArray) {
				JsonObject jsonObject = item.getAsJsonObject();

				if (jsonObject.has("value")) {
					JsonObject value = jsonObject.get("value").getAsJsonObject();

					if (value.has("agentId") && value.has("policyId")) {
						String policyNumber = value.get("policyId").getAsString();
						String agentCode = value.get("agentId").getAsString();

						if (agentCodeArrayList.contains(agentCode)) {
							approvalJsonMap.put(policyNumber, jsonObject);
						}
					}

				}
			}
		}

		return approvalJsonMap;
	}

	private JsonArray convertViewResultToGsonJsonArray(JSONObject rawJsonObject) {
		Log.info("Goto SupervisorReportService.convertViewResultToGsonJsonArray");

		JsonArray gsonJsonArray = new JsonArray();

		if (rawJsonObject != null) {
			JSONArray tempJsonArray = rawJsonObject.getJSONArray("rows");

			String jsonArrayString = tempJsonArray.toString();
			gsonJsonArray = Function.validateJsonArrayFormatForString(jsonArrayString);
		}

		return gsonJsonArray;
	}

	public String[][] parseToExcelDataArray(JsonArray mergedRecordJsonArray) {
		Log.info("Goto SupervisorReportService.parseToExcelDataArray");

		ArrayList<String[]> rowArrayList = new ArrayList<String[]>();
		for (JsonElement item : mergedRecordJsonArray) {

			JsonObject itemJsonObject = item.getAsJsonObject();
			
			SupervisorReportData rowData = mapSupervisorReportData(itemJsonObject);

			// Create row
			String[] rowArray = createRowArray(rowData);
			rowArrayList.add(rowArray);

			Log.debug("*** rowArray= " + rowArray);

		}

		return convertArrayListTo2DStringArray(rowArrayList);
	}
	
	private SupervisorReportData mapSupervisorReportData(JsonObject itemJsonObject) {
		SupervisorReportData rowData = new SupervisorReportData();
		
		JsonObject appJsonObject = null;
		JsonObject approvalJsonObject = null;
		
		boolean isAgencyChannel = false;
		
		rowData.setCaseNumber_PolicyNumber(itemJsonObject.get("policyNumber").getAsString());

		if (itemJsonObject.has("approval")) {
			approvalJsonObject = itemJsonObject.get("approval").getAsJsonObject();
			approvalJsonObject = approvalJsonObject.get("value").getAsJsonObject();
		}

		if (itemJsonObject.has("app")) {
			appJsonObject = itemJsonObject.get("app").getAsJsonObject();
			appJsonObject = appJsonObject.get("value").getAsJsonObject();
		}
		
		if (appJsonObject != null) {
			// TODO - get application status from bundle.application.appstatus
			
			if(appJsonObject.has("quotation")) {
				JsonObject quotation = appJsonObject.get("quotation").getAsJsonObject();
				
				if(quotation != null) {
					if(quotation.has("agent")) {
						JsonObject agent = quotation.get("agent").getAsJsonObject();
						
						if(agent != null) {
							if(agent.has("dealerGroup")) {
								String dealerGroup = agent.get("dealerGroup").getAsString();
								
								if(dealerGroup.equals("AGENCY")) {
									isAgencyChannel = true;
								}
							}
						}
					}
					
				}
			}
			
			if(appJsonObject.has("applicationForm")) {
				JsonObject applicationForm = appJsonObject.get("applicationForm").getAsJsonObject();
				if(applicationForm.has("values")) {
					JsonObject values = applicationForm.get("values").getAsJsonObject();
					
					if(values.has("insured")) {
						JsonArray insured = values.get("insured").getAsJsonArray();
						
						if(insured.size()>0) {
							JsonObject insuredPerson = insured.get(0).getAsJsonObject();
							
							if(insuredPerson.has("personalInfo")) {
								JsonObject insuredPersonInfo = insuredPerson.get("personalInfo").getAsJsonObject();
								
								if(insuredPersonInfo.has("idDocType")) {
									String idDocType = insuredPersonInfo.get("idDocType").getAsString();
									
									if(idDocType.equals("other")) {
										if(insuredPersonInfo.has("idDocTypeOther")) {
											String idDocTypeOther = insuredPersonInfo.get("idDocTypeOther").getAsString();
											rowData.setLifeAssuredICDocumentType(idDocTypeOther);
										}
									}else {
										idDocType = convertIdType(idDocType);
										rowData.setLifeAssuredICDocumentType(idDocType);
									}
									
								}
								
								if(insuredPersonInfo.has("idCardNo")) {
									String idCardNo = insuredPersonInfo.get("idCardNo").getAsString();
									rowData.setLifeAssuredIC(idCardNo);
								}
								
								if(insuredPersonInfo.has("fullName")) {
									String fullName = insuredPersonInfo.get("fullName").getAsString();
									rowData.setLifeAssuredName(fullName);
								}
							}
						}
					}
					
					if(values.has("proposer")) {
						JsonObject proposer = values.get("proposer").getAsJsonObject();
						
						if(proposer.has("personalInfo")) {
							JsonObject personalInfo = proposer.get("personalInfo").getAsJsonObject();
							
							if(personalInfo.has("idDocType")) {
								String idDocType = personalInfo.get("idDocType").getAsString();
								
								if(idDocType.equals("other")) {
									if(personalInfo.has("idDocTypeOther")) {
										String idDocTypeOther = personalInfo.get("idDocTypeOther").getAsString();
										rowData.setProposerICDocumentType(idDocTypeOther);
									}
								}else {
									idDocType = convertIdType(idDocType);
									rowData.setProposerICDocumentType(idDocType);
								}
								
							}
							
							if(personalInfo.has("idCardNo")) {
								String idCardNo = personalInfo.get("idCardNo").getAsString();
								rowData.setProposerIC(idCardNo);
							}
							
							if(personalInfo.has("fullName")) {
								String fullName = personalInfo.get("fullName").getAsString();
								rowData.setProposerName(fullName);
							}
							
							if(personalInfo.has("mobileNo")) {
								String mobileNo = personalInfo.get("mobileNo").getAsString();
								rowData.setProposerMobileNo(mobileNo);
							}
							
							if(personalInfo.has("email")) {
								String email = personalInfo.get("email").getAsString();
								rowData.setProposerEmailaddress(email);
							}
							
						}
						
					}
					
				}
			}
			
			
			if (appJsonObject.has("applicationStartedDate")) {
				String casecreationdate = appJsonObject.get("applicationStartedDate").getAsString();
				
				if(casecreationdate.equals("0"))
					casecreationdate = null;
				
				rowData.setCasecreationdate(casecreationdate);
			}
			
			if (appJsonObject.has("applicationSubmittedDate")) {
				String agentCaseSubmissiondate = appJsonObject.get("applicationSubmittedDate").getAsString();
				
				if(agentCaseSubmissiondate.equals("0"))
					agentCaseSubmissiondate = null;
				
				rowData.setAgentCaseSubmissiondate(agentCaseSubmissiondate);
			}

		}
		
		if (approvalJsonObject != null) {
			if (approvalJsonObject.has("approvalStatus")) {
				String approvalStatus = approvalJsonObject.get("approvalStatus").getAsString();
				approvalStatus = convertApprovalStatus(approvalStatus);
				rowData.setCaseStatus(approvalStatus);
			}
			
			if (approvalJsonObject.has("approvalStatus")) {
				String approvalStatus = approvalJsonObject.get("approvalStatus").getAsString();
				approvalStatus = convertApprovalStatus(approvalStatus);
				rowData.setCaseStatus(approvalStatus);
			}
			
			String approveRejectDate = null;
			if (approvalJsonObject.has("approveRejectDate")) {
				
				approveRejectDate = approvalJsonObject.get("approveRejectDate").getAsString();
				
				if(approveRejectDate.equals("0"))
					approveRejectDate = null;
			}
			
			if(isAgencyChannel) {
				
				rowData.setSupervisorapprovaldate(approveRejectDate);
			}else {
				if (approvalJsonObject.has("supervisorApproveRejectDate")) {
					String supervisorApproveRejectDate = approvalJsonObject.get("supervisorApproveRejectDate").getAsString();
					rowData.setSupervisorapprovaldate(supervisorApproveRejectDate);
				}
				
				rowData.setFirmapprovaldate(approveRejectDate);
			}

		}
		
		return rowData;
	}
	
	private String convertIdType(String idDocType) {
		switch (idDocType) {
		case "passport":
			idDocType = "Passport";
			break;
		case "nric":
			idDocType = "NRIC";
			break;
		case "fin":
			idDocType = "FIN No";
			break;
		default:
			idDocType = null;
			break;
		}

		return idDocType;
	}

	private String convertApprovalStatus(String approvalStatus) {
		switch (approvalStatus) {
		case "A":
			approvalStatus = "Approved";
			break;
		case "R":
			approvalStatus = "Rejected";
			break;
		case "E":
			approvalStatus = "Expired";
			break;
		case "SUBMITTED":
			approvalStatus = "Pending Supervisor Approval";
			break;
		case "PDoc":
			approvalStatus = "Pending Document";
			break;
		case "PDis":
			approvalStatus = "Pending Discussion";
			break;
		case "PFAFA":
			approvalStatus = "Pending FA firm approval";
			break;
		case "PDocFAF":
			approvalStatus = "Pending Document (FA Firm)";
			break;
		case "PDisFAF":
			approvalStatus = "Pending Discussion (FA Firm)";
			break;
		case "PCdaA":
			approvalStatus = "Pending CDA approval";
			break;
		case "PDocCda":
			approvalStatus = "Pending Document (CDA)";
			break;
		case "PDisCda":
			approvalStatus = "Pending Discussion (CDA)";
			break;
		case "inProgressBI":
			approvalStatus = "In Progress BI";
			break;
		case "inProgressApp":
			approvalStatus = "In Progress Application";
			break;
		default:
			approvalStatus = null;
			break;
		}

		return approvalStatus;
	}

	private String[] createRowArray(SupervisorReportData rowData) {
		Log.info("Goto SupervisorReportService.createRowArray");

		ArrayList<String> rowList = new ArrayList<String>();

		rowList.add(rowData.getCaseStatus());
		rowList.add(rowData.getCaseNumber_PolicyNumber());
		rowList.add(rowData.getCasecreationdate());
		rowList.add(rowData.getAgentCaseSubmissiondate());
		rowList.add(rowData.getSupervisorapprovaldate());
		rowList.add(rowData.getFirmapprovaldate());
		rowList.add(rowData.getProposerICDocumentType());
		rowList.add(rowData.getProposerIC());
		rowList.add(rowData.getProposerName());
		rowList.add(rowData.getLifeAssuredICDocumentType());
		rowList.add(rowData.getLifeAssuredIC());
		rowList.add(rowData.getLifeAssuredName());
		rowList.add(rowData.getProposerMobileNo());
		rowList.add(rowData.getProposerEmailaddress());
		rowList.add(rowData.getAgentName());
		rowList.add(rowData.getManagerName());
		rowList.add(rowData.getDirectorName());
		rowList.add(rowData.getCaseApprovedby());
		rowList.add(rowData.getFirmName());
		rowList.add(rowData.getProductName());
		rowList.add(rowData.getPremiumamount_Basicplan());
		rowList.add(rowData.getSumassured_BasicPlan());
		rowList.add(rowData.getPremiumType_RP_SP());
		rowList.add(rowData.getPremiumFrequency());
		rowList.add(rowData.getTop_up());
		rowList.add(rowData.getRSP());
		rowList.add(rowData.getRSPfrequency());
		rowList.add(rowData.getPaymentMethod());
		rowList.add(rowData.getCurrency());

		return rowList.toArray(new String[rowList.size()]);
	}

	private String[][] convertArrayListTo2DStringArray(ArrayList<String[]> rowArrayList) {
		Log.info("Goto SupervisorReportService.convertArrayListTo2DStringArray");

		String[][] array = new String[rowArrayList.size()][];
		for (int i = 0; i < rowArrayList.size(); i++) {
			String[] stringArray = rowArrayList.get(i);
			array[i] = stringArray;
		}

		return array;
	}

	public JsonObject createJsonBody(String fileDataBase64Format) throws Exception {
		Log.info("Goto SupervisorReportService.createJsonBody");

		JsonObject jsonBody = null;

		try {
			// SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			// String toEmail = eSysParam.selectSysValue("REPORT_RLS_SINGPOST_TO");
			// String fromEmail = eSysParam.selectSysValue("REPORT_RLS_SINGPOST_FROM");
			//
			// Log.debug("*** toEmail="+toEmail+", fromEmail="+fromEmail);
			//
			// BATCH_EMAIL_TEMPLATE emailTemplateJson= new BATCH_EMAIL_TEMPLATE();
			//
			// JSONObject templateJsonObject =
			// emailTemplateJson.selectEmailTemplate("REPORT_RLS_SINGPOST");
			//
			// String title = templateJsonObject.has("MAIL_SUBJ")?
			// templateJsonObject.getString("MAIL_SUBJ"):null;
			// String content = templateJsonObject.has("MAIL_CONTENT")?
			// templateJsonObject.getString("MAIL_CONTENT"):null;

			String toEmail = RECEIVER_ADDRESS;
			String fromEmail = SENDER_ADDRESS;
			String title = "Supervisor Report";
			String content = "Testing";

			if (toEmail != null && fromEmail != null && title != null && content != null) {
				JsonObject fileJson = new JsonObject();
				fileJson.addProperty("fileName", FILE_NAME);
				fileJson.addProperty("data", fileDataBase64Format);

				JsonArray attachments = new JsonArray();
				attachments.add(fileJson);

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content", content);
				jsonBody.add("attachments", attachments);
			} else {
				Log.error("ERROR - toEmail, fromEmail, title, content have null");
			}

		} catch (Exception ex) {
			Log.error(ex);
		}

		return jsonBody;
	}

	public HttpPost createEmailPostRequest(JsonObject jsonBody) throws Exception {
		Log.info("Goto SupervisorReportService.createEmailPostRequest");

		String emailURL = DOMAIN + SERVICE_PATH;

		HttpPost request = new HttpPost(emailURL);
		request.addHeader("Content-Type", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

}
