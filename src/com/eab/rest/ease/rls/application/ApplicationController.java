package com.eab.rest.ease.rls.application;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonObject;

@Path("/application")
public class ApplicationController {

	private ApplicationStatusService applicationStatusService = new ApplicationStatusService();

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response applicationStatus(@PathParam("id") String requestID, @QueryParam("lob") String lob,
			@Context HttpHeaders headers, @Context UriInfo uriInfo) {
		Log.info("RLS RecordApplication / applicationStatus");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers); // for debug

		HttpGet getRequest = null;
		Response output = null;

		boolean isCompleted = false;
		String queryAsString = uriInfo.getRequestUri().getQuery();
		String lobString = lob == null ? "Life" : lob;
		
		Log.debug("***lobString="+lobString);

		try {
			boolean result = applicationStatusService.createTransactionRecord(audID,
					RestUtil.convertHeaderToStr(headers.getRequestHeaders()), queryAsString, requestID);
			Log.debug("*** trxAppStatusDB create result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, "applicationStatus get request=" + ex.getMessage()));
		}

		if (requestID != null) {
			try {
				getRequest = applicationStatusService.createGetRequest(audID, requestID, lobString);

				// Send application to AXA
				output = HttpUtil.send(getRequest);
				
				String asyncStatus = "F";
				if (output.getStatus() == 200) {
					asyncStatus = output.getHeaderString("x-axa-async-status");
				}
				boolean result = applicationStatusService.updateApplicationStatus(audID, requestID, asyncStatus);
				Log.debug("*** applicationStatusService.updateApplicationStatus="+result);
			} catch (Exception ex) {
				Log.error(ex);
				output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
						RestUtil.genErrorJsonStr(false, "applicationStatusService=" + ex.getMessage()));
			}

			// *** Testing data ***//
			// String testingResponseAsString
			// ="{\"RetrieveApplicationStatusResponse\":{\"message\":\"{\\\"RetrieveApplicationStatusResponse\\\":{\\\"message\\\":\\\"{\\\\\\\"asyncAppResponse\\\\\\\":{\\\\\\\"ackTransactionList\\\\\\\":[{\\\\\\\"respondedOn\\\\\\\":\\\\\\\"2017-07-28T07:12:29\\\\\\\",\\\\\\\"entityId\\\\\\\":\\\\\\\"SG\\\\\\\",\\\\\\\"txnStatus\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"policyNumber\\\\\\\":\\\\\\\"101-2331011\\\\\\\"}]}}\\\"}}\"}}";
			// output = Response.ok(testingResponseAsString).build();
			// *** Testing data ***//
		} else {
			output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
					RestUtil.genErrorJsonStr(false, "missing request ID"));
		}

		try {
			String requestHeader = getRequest == null ? "" : HttpUtil.convertHeaderToStr(getRequest.getAllHeaders());
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			String responseAsString = output.getEntity().toString();

			isCompleted = output.getStatus() == 200 ? true : false;

			boolean updateResult = applicationStatusService.updateTransactionRecord(audID, requestHeader,
					responseHeaderAsString, responseAsString, isCompleted);
			Log.debug("*****trxAppStatusDB update result=" + updateResult);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}

	private RecordApplicationValidator recordApplicationValidator = new RecordApplicationValidator();
	private RecordApplicationService recordApplicationService = new RecordApplicationService();

	@POST
	@Path("")
	@Consumes("application/json")
	@Produces("application/json")
	public Response recordApplication(@Context HttpHeaders headers, String body) {
		Log.info("RLS RecordApplication / application");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers, body); // for debug

		boolean isCompleted = false;
		HttpPost postRequest = null;
		Response output = null;

		try {

			boolean result = recordApplicationService.createTransactionRecord(audID,
					RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body);

			Log.debug("*****trxRecordAppDB create result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}

		String policyNumberAsString = "";

		try {
			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate Json format

			if (bodyJson == null) {
				// Request body is not in JSON format
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
						RestUtil.genErrorJsonStr(false, "Json Syntax Incorrect;"));
			} else {
				policyNumberAsString = recordApplicationService.getPolicyNumber(bodyJson);
				String errorString = recordApplicationValidator.validate(bodyJson);

				boolean isFail = errorString.length() > 0 ? true : false;

				if (isFail) {
					// Invalid content
					output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
							RestUtil.genErrorJsonStr(false, errorString));
				} else {

					String lob = "Life";

					if (bodyJson.has("lob")) {
						lob = bodyJson.get("lob").getAsString();
						bodyJson.remove("lob");
					}

					try {
						postRequest = recordApplicationService.createRecordApplicationPostRequest(audID, bodyJson, lob);

						// Send application to AXA
						output = HttpUtil.send(postRequest);
					} catch (Exception ex) {
						output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
								RestUtil.genErrorJsonStr(false, "createApplicationPostRequest=" + ex.getMessage()));
					}
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}

		try {
			// *** Testing response ***//
			// output =
			// Response.accepted("{\"Hello\":\"World\"}").header("x-axa-async-request-id",
			// "testing123").build();
			// *** Testing response ***//

			String requestID = "";
			if (output.getStatus() == 202) { // 202 = success
				isCompleted = true;
				requestID = output.getHeaderString("x-axa-async-request-id");
			}

			String requestHeader = postRequest == null ? "" : HttpUtil.convertHeaderToStr(postRequest.getAllHeaders());

			String requestParam = "";

			if (postRequest != null) {
				requestParam = EntityUtils.toString(postRequest.getEntity());
			}

			String responseAsString = output.getEntity().toString();
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());

			boolean result = recordApplicationService.updateTransactionRecord(audID, policyNumberAsString,
					requestHeader, requestParam, responseHeaderAsString, responseAsString, requestID, isCompleted);

			Log.debug("*****trxRecordAppDB update result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}
		return output;
	}

}