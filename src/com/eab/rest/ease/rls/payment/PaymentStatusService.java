package com.eab.rest.ease.rls.payment;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONObject;

import com.eab.auth.ease.Maam;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.API_RLS_PYMT_STATUS;
import com.eab.dao.API_RLS_PYMT_STATUS_TRX;
import com.eab.dao.API_RLS_REC_PYMT_TRX;
import com.google.gson.JsonObject;

public class PaymentStatusService {
	
	public boolean updatePaymentStatus(String audID, String requestID, String asyncStatus) throws Exception {
		Log.info("Goto PaymentStatusService.updatePaymentStatus");
		// save to DB
		
		API_RLS_REC_PYMT_TRX recordPaymentDao = new API_RLS_REC_PYMT_TRX();
		String policyNumber = null;
		
		JSONObject policyNumberJson = recordPaymentDao.getPolicyNumber(requestID);
		if(policyNumberJson != null) {
			if(policyNumberJson.has("POL_NUM")) {
				policyNumber = policyNumberJson.getString("POL_NUM");
			}
		}

		API_RLS_PYMT_STATUS paymentStatusDao = new API_RLS_PYMT_STATUS();

		if (asyncStatus != null) {
			switch (asyncStatus) {
			case "C":
				asyncStatus = "Y";
				break;
			case "P":
				asyncStatus = "P";
				break;
			default:
				break;
			}
		}

		boolean result = paymentStatusDao.create(audID, policyNumber, asyncStatus, "S");

		Log.debug("*** paymentStatusDao=" + result);

		return false;
	}

	public HttpGet createGetRequest(String msgid, String requestId, String lob) throws Exception {
		Log.info("Goto PaymentStatusService.createGetRequest");

		String appStatusHost = EnvVariable.get("WEB_API_RLS_PAYMENT_STATUS");

		HttpGet request = new HttpGet(appStatusHost);
		request.addHeader("Content-Type", "application/json");

		// Headers
		request.addHeader("Authorization", "Bearer " + Maam.getToken());
		request.addHeader("x-axa-entity", "SG");
		request.addHeader("x-axa-requesting-channel", "EASE");
		request.addHeader("x-axa-contextheader-customdata-sourcesystem", "EASE");
		request.addHeader("x-axa-contextheader-customdata-targetsystem", "FileNet");
		request.addHeader("x-axa-msgid", msgid);
		request.addHeader("x-axa-initialmsgid", msgid);
		request.addHeader("x-axa-lob", ("Health".equals(lob)||"Life".equals(lob))?lob:"");
		request.addHeader("x-axa-contextheader-contextversion", "1.0");

		JsonObject statusCheckJson = new JsonObject();
		statusCheckJson.addProperty("requestID", requestId);
		statusCheckJson.addProperty("returnResponse", "Y");

		byte[] bytesEncoded = Function.StrToBase64(statusCheckJson.toString());
		String base64String = Function.ByteToBase64(bytesEncoded);

		Log.debug("**** base64String=" + base64String);

		request.addHeader("x-axa-async-status-check", base64String);
		request.addHeader("Accept", "application/json");

		return request;
	}

	public boolean createTransactionRecord(String audID, String headerAsString, String queryAsString, String requestID)
			throws Exception {
		Log.info("Goto PaymentStatusService.createTransactionRecord");
		API_RLS_PYMT_STATUS_TRX trxPaymentStatusDao = new API_RLS_PYMT_STATUS_TRX();
		return trxPaymentStatusDao.create(audID, headerAsString, queryAsString, requestID);
	}

	public boolean updateTransactionRecord(String audID, String requestHeader, String responseHeaderAsString,
			String responseAsString, boolean isCompleted) throws Exception {
		Log.info("Goto PaymentStatusService.updateTransactionRecord");
		API_RLS_PYMT_STATUS_TRX trxPaymentStatusDao = new API_RLS_PYMT_STATUS_TRX();
		return trxPaymentStatusDao.update(audID, requestHeader, responseHeaderAsString, responseAsString, isCompleted);
	}

}
