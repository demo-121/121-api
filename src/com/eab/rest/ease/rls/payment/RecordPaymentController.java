package com.eab.rest.ease.rls.payment;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.rest.ease.email.EmailNotificationHelper;
import com.google.gson.JsonObject;

@Path("/payment-record")
public class RecordPaymentController {

	private PaymentStatusService paymentStatusService = new PaymentStatusService();

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response paymentStatus(@PathParam("id") String requestID, @QueryParam("lob") String lob,
			@Context HttpHeaders headers, @Context UriInfo uriInfo) {
		Log.info("RLS RecordPayment / paymentStatus");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers); // for debug

		HttpGet getRequest = null;
		Response output = null;

		boolean isCompleted = false;
		String queryAsString = uriInfo.getRequestUri().getQuery();
		String lobString = lob == null ? "Life" : lob;
		
		Log.debug("***lobString="+lobString);

		try {
			boolean result = paymentStatusService.createTransactionRecord(audID,
					RestUtil.convertHeaderToStr(headers.getRequestHeaders()), queryAsString, requestID);
			Log.debug("*** trxPaymentStatusDB create result=" + result);
		} catch (Exception ex) {
			Log.error("Error - ApplicationStatus - save to DB API_RLS_PYMT_STATUS_TRX failed");
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, "applicationStatus get request=" + ex.getMessage()));
		}

		if (requestID != null) {
			try {
				getRequest = paymentStatusService.createGetRequest(audID, requestID, lobString);

				// Send application to AXA
				output = HttpUtil.send(getRequest);

				// *** Testing data ***//
				// String testingResponseAsString =
				// "{\"RetrievePaymentStatusResponse\":{\"message\":\"{\\\"asyncrecPaymentResponse\\\":{\\\"ackTransactionList\\\":[{\\\"respondedOn\\\":\\\"2017-07-28T03:16:58\\\",\\\"entityId\\\":\\\"SG\\\",\\\"txnStatus\\\":\\\"0\\\",\\\"policyNumber\\\":\\\"101-2330740\\\"}]}}\"}}";
				// output = Response.ok(testingResponseAsString).build();
				// *** Testing data ***//

				String asyncStatus = "F";
				if (output.getStatus() == 200) {
					asyncStatus = output.getHeaderString("x-axa-async-status");
				}
				boolean result = paymentStatusService.updatePaymentStatus(audID, requestID, asyncStatus);
				Log.debug("*** paymentStatusService.updatePaymentStatus=" + result);
			} catch (Exception ex) {
				Log.error(ex);
				output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
						RestUtil.genErrorJsonStr(false, "paymentStatusService=" + ex.getMessage()));
			}

		} else {
			output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
					RestUtil.genErrorJsonStr(false, "missing requestID"));
		}

		try {
			String requestHeader = getRequest == null ? "" : HttpUtil.convertHeaderToStr(getRequest.getAllHeaders());
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			String responseAsString = output.getEntity().toString();

			isCompleted = output.getStatus() == 200 ? true : false;

			boolean updateResult = paymentStatusService.updateTransactionRecord(audID, requestHeader,
					responseHeaderAsString, responseAsString, isCompleted);
			Log.debug("*****trxPaymentStatusDB update result=" + updateResult);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}

	private RecordPaymentService recordPaymentService = new RecordPaymentService();
	private RecordPaymentValidator recordPaymentValidator = new RecordPaymentValidator();

	@POST
	@Path("")
	@Consumes("application/json")
	@Produces("application/json")
	public Response recordPayment(@Context HttpHeaders headers, String body) {
		Log.info("RLS RecordPayment");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter

		RestUtil.showLogRequest(headers, body); // for debug

		boolean isCompleted = false;
		HttpPost postRequest = null;
		Response output = null;

		try {
			boolean result = recordPaymentService.createTransactionRecord(audID,
					RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body);

			Log.debug("*****trxRecordPaymentDB create result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}

		String policyNumberAsString = "";

		boolean isFail = false;
		boolean isDuplicatedRecordOnly = false;
		boolean isForce = false;

		try {

			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate Json format

			if (bodyJson == null) {
				// Request body is not in JSON format
				output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
						RestUtil.genErrorJsonStr(false, "Json Syntax Incorrect;"));
			} else {
				policyNumberAsString = recordPaymentService.getPolicyNumber(bodyJson);

				String errorString = recordPaymentValidator.validate(bodyJson);
				
				if(bodyJson.has("isForce") && bodyJson.get("isForce").getAsBoolean()) { //isForce = Resubmission
					isForce = true;
					bodyJson.remove("isForce");
				}
				
				if(!isForce) { //isForce = Resubmission, skip Duplication checking
					boolean needToSendToRSL = recordPaymentService.needToSendToRLS(policyNumberAsString);
					if(!needToSendToRSL) {
						if(errorString.length() <=0) {
							isDuplicatedRecordOnly = true;
						}
						errorString+="Record is already sent to RLS;";
					}
				}

				isFail = errorString.length() > 0 ? true : false;

				if (isFail) {
					// Invalid content
					output = RestUtil.toResponse(audID, Response.Status.BAD_REQUEST,
							RestUtil.genErrorJsonStr(false, errorString));
					
				} else {

					String lob = "Life";

					if (bodyJson.has("lob")) {
						lob = bodyJson.get("lob").getAsString();
						bodyJson.remove("lob");
					}

					try {
						postRequest = recordPaymentService.createRecordPaymentPostRequest(audID, bodyJson, lob);

						// Send application to AXA
						output = HttpUtil.send(postRequest);
					} catch (Exception ex) {
						output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
								RestUtil.genErrorJsonStr(false, "createRecordPaymentPostRequest=" + ex.getMessage()));
					}
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}

		try {
			// *** Testing response ***//
			// output =
			// Response.accepted("{\"Hello\":\"World\"}").header("x-axa-async-request-id",
			// "testing123").build();
			// *** Testing response ***//

			String requestID = "";
			if (output.getStatus() == 202) { // 202 = success
				isCompleted = true;
				requestID = output.getHeaderString("x-axa-async-request-id");
			}else if(!isFail || (isFail && !isDuplicatedRecordOnly)){
				if(!policyNumberAsString.equals("")) { //send error notification to AXA
					EmailNotificationHelper.sendErrorNotificationPayment(policyNumberAsString);
				}else {
					EmailNotificationHelper.sendErrorNotificationPayment(" - ");
				}
			}

			String requestHeader = postRequest == null ? "" : HttpUtil.convertHeaderToStr(postRequest.getAllHeaders());

			String requestParam = "";

			if (postRequest != null) {
				requestParam = EntityUtils.toString(postRequest.getEntity());
			}

			String responseAsString = output.getEntity().toString();
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());

			boolean result = recordPaymentService.updateTransactionRecord(audID, policyNumberAsString, requestHeader,
					requestParam, responseHeaderAsString, responseAsString, requestID, isCompleted, isForce);

			Log.debug("*****trxRecordPaymentDB update result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}

}
