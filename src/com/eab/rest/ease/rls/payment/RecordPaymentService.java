package com.eab.rest.ease.rls.payment;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import com.eab.auth.ease.Maam;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.API_RLS_REC_PYMT_TRX;
import com.google.gson.JsonObject;

public class RecordPaymentService {

	public HttpPost createRecordPaymentPostRequest(String msgid, JsonObject jsonBody, String lob) throws Exception {
		Log.info("Goto RecordPaymentService.createApplicationPostRequest");

		String recordPayemntHost = EnvVariable.get("WEB_API_RLS_PAYMENT");
		String payemntCallbackHost = EnvVariable.get("WEB_API_RLS_PAYMENT_CALLBACK");

		HttpPost request = new HttpPost(recordPayemntHost);
		request.addHeader("Content-Type", "application/json");

		// Headers
		request.addHeader("Authorization", "Bearer " + Maam.getToken());
		request.addHeader("x-axa-entity", "SG");
		request.addHeader("x-axa-requesting-channel", "EASE");
		request.addHeader("x-axa-contextheader-customdata-sourcesystem", "EASE");
		request.addHeader("x-axa-contextheader-customdata-targetsystem", "FileNet");
		request.addHeader("x-axa-msgid", msgid);
		request.addHeader("x-axa-initialmsgid", msgid);
		request.addHeader("x-axa-lob", ("Health".equals(lob)||"Life".equals(lob))?lob:"");
		request.addHeader("x-axa-processnm", "NB");

		JsonObject addressingJson = new JsonObject();
		addressingJson.addProperty("replyTo", payemntCallbackHost);

		byte[] bytesEncoded = Function.StrToBase64(addressingJson.toString());
		String base64String = Function.ByteToBase64(bytesEncoded);

		Log.debug("**** base64String=" + base64String);
		request.addHeader("x-axa-async-addressing", base64String);

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

	public String getPolicyNumber(JsonObject bodyJson) {
		Log.info("Goto RecordPaymentService.getPolicyNumber");

		String policyNumberAsString = "";
		try {
			if (bodyJson.has("recordPaymentRequest")) {
				JsonObject recordPaymentRequest = bodyJson.get("recordPaymentRequest").getAsJsonObject();
				if (recordPaymentRequest.has("policyNO")) {
					policyNumberAsString = recordPaymentRequest.get("policyNO").getAsString();
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		return policyNumberAsString;
	}
	
	public boolean needToSendToRLS(String policyNumber) {
		boolean isNeeded = false;
		
		API_RLS_REC_PYMT_TRX recordPaymentDao = new API_RLS_REC_PYMT_TRX();
		
		try {
			JSONObject jsonObject = recordPaymentDao.getCompletedRecord(policyNumber);
			isNeeded = jsonObject==null?true:false;
			
		}catch(Exception ex){
			Log.error(ex);
		}
		
		return isNeeded;
	}

	public boolean createTransactionRecord(String audID, String headerAsString, String body) throws Exception {
		Log.info("Goto RecordPaymentService.createTransactionRecord");
		API_RLS_REC_PYMT_TRX trxRecordPaymentDao = new API_RLS_REC_PYMT_TRX();
		return trxRecordPaymentDao.create(audID, headerAsString, body);
	}

	public boolean updateTransactionRecord(String audID, String policyNumberAsString, String requestHeader,
			String requestParam, String responseHeaderAsString, String responseAsString, String requestID,
			boolean isCompleted, boolean isForce) throws Exception {
		Log.info("Goto RecordPaymentService.updateTransactionRecord");
		API_RLS_REC_PYMT_TRX trxRecordPaymentDao = new API_RLS_REC_PYMT_TRX();
		return trxRecordPaymentDao.update(audID, policyNumberAsString, requestHeader, requestParam,
				responseHeaderAsString, responseAsString, requestID, isCompleted, isForce);
	}

}
