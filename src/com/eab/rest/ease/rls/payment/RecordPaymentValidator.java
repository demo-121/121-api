package com.eab.rest.ease.rls.payment;

import java.util.Arrays;

import com.eab.common.Log;
import com.eab.rest.ease.validator.BaseValidator;
import com.google.gson.JsonObject;

public class RecordPaymentValidator extends BaseValidator {

	@Override
	protected String validateMandatoryFields(JsonObject json, String parentKey, String level) {
		Log.info("Goto RecordPaymentValidator.validateFields");

		String[] mandatoryFields = new String[] {};
		String localErrorString = "";

		switch (parentKey) {
		case "":
			mandatoryFields = new String[] { "recordPaymentRequest" };
			break;
		case "recordPaymentRequest":
			mandatoryFields = new String[] { "policyNO", "depositAMT", "currencyCD",
					"hasLifePolicyTransactionDetailsIn" };
			break;

		case "hasLifePolicyTransactionDetailsIn":
			mandatoryFields = new String[] { "transTypeCD", "transCurrencyCD",
					"hasLifePolicyTransactionHeaderDetailsIn" };
			break;

		case "hasLifePolicyTransactionHeaderDetailsIn":
			mandatoryFields = new String[] { "bankAccountNO" };
			break;

		default:
			break;
		}

		// validate mandatory fields
		for (String key : mandatoryFields) {
			if (!json.has(key)) {
				localErrorString += "missing " + key + ";";
			}
		}

		return localErrorString;
	}

	@Override
	protected String validateField(JsonObject json, String currentKey, String parentKey) {
		Log.info("Goto RecordPaymentValidator.validateField");

		String localErrorString = "";

		boolean valid = true;
		String[] optionValues = null;

		switch (currentKey) { // check possible values
		case "lob":
			optionValues = new String[] { "Life", "Health" };
			valid = Arrays.asList(optionValues).contains(json.get("lob").getAsString());

			break;
		default:
			break;
		}
		
		if(optionValues != null) {
			valid = Arrays.asList(optionValues).contains(json.get(currentKey).getAsString());
		}

		if (!valid) {
			localErrorString += "invalid " + currentKey + ";";
		}

		// Check JSON type for array fields
		String[] jsonArrayKeys = new String[] { "hasLifePolicyTransactionDetailsIn", "hasPaymentDetailsIn",
				"hasLifePolicyTransactionHeaderDetailsIn" };

		boolean isJsonArrayKey = Arrays.asList(jsonArrayKeys).contains(currentKey);

		if (isJsonArrayKey) {
			if (!json.get(currentKey).isJsonArray()) {
				localErrorString += currentKey + " should be a JSON array;";
			}
		}

		return localErrorString;
	}
	
	@Override
	protected boolean getLevelDisplay() {
		return true;
	}

}
