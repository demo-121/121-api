package com.eab.rest.ease.rls.payment.callback;

import java.util.Base64;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.google.gson.JsonObject;

@Path("/payrec/callback")
public class PaymentCallbackController {
	
	private PaymentCallbackService paymentCallbackService = new PaymentCallbackService();
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response applicationNotification(@Context HttpHeaders headers, String body) {
		Log.info("RLS PaymentNotification / callback");
		String audID = headers.getRequestHeader("aud-id").get(0); // provided by filter
		String reqID = "";
		
		List<String> headerList = headers.getRequestHeader("x-axa-async-response");
		if (headerList != null && headerList.size() > 0) {
			try {
				reqID = new String(Base64.getDecoder().decode(headerList.get(0)), "utf-8");
			} catch(Exception e) {
				///DO Nothing
			}
		}

		RestUtil.showLogRequest(headers, body); // for debug

		boolean isCompleted = false;
		Response output = null;

		try {
			
			boolean result = paymentCallbackService.createTransactionRecord(audID, RestUtil.convertHeaderToStr(headers.getRequestHeaders()), body, reqID);

			Log.debug("*****trxPaymentNotiDB create result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}

		try {
			JsonObject bodyJson = Function.validateJsonFormatForString(body); // Validate Json format
			if (bodyJson == null) {
				// Request body is not in JSON format
				output = paymentCallbackService.createFailureResponse(audID, "Json Syntax Incorrect;");
			} else {
				
				String errorString = paymentCallbackService.validateCallbackContent(audID, bodyJson);
				
				if (errorString.length() > 0) {
					output = paymentCallbackService.createFailureResponse(audID, errorString);
				} else {
					output = paymentCallbackService.createSuccessResponse(audID);
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR,
					RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}

		try {
			isCompleted = output.getStatus() == 200 ? true : false;
			
			String responseHeaderAsString = RestUtil.convertHeaderToStr(output.getStringHeaders());
			String responseAsString = output.getEntity().toString();
			boolean result = paymentCallbackService.updateTransactionRecord(audID, responseHeaderAsString, responseAsString, isCompleted);

			Log.debug("*****trxPaymentNotiDB update result=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}

		return output;
	}

}