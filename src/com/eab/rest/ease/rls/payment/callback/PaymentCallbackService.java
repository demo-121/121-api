package com.eab.rest.ease.rls.payment.callback;

import javax.ws.rs.core.Response;

import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_RLS_PYMT_NOTI_TRX;
import com.eab.dao.API_RLS_PYMT_STATUS;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class PaymentCallbackService {
	
	public String validateCallbackContent(String audID, JsonObject bodyJson) {
		Log.info("Goto PaymentCallbackService.validateContent");

		String errorString = "";

		try {

			if (!bodyJson.has("asyncrecPaymentResponse")) {
				errorString += "missing asyncrecPaymentResponse;";
			} else {

				JsonObject ackTransactionList = bodyJson.get("asyncrecPaymentResponse").getAsJsonObject();
				if (!ackTransactionList.has("ackTransactionList")) {
					errorString += "missing ackTransactionList;";
				} else {

					JsonArray jsonArray = ackTransactionList.get("ackTransactionList").getAsJsonArray();

					int objIndex = 0;

					for (JsonElement jsonElement : jsonArray) {
						if (jsonElement.isJsonObject()) {

							JsonObject ackTransaction = jsonElement.getAsJsonObject();

							// validate AckTransaction
							String localErrorString = validateAckTransaction(ackTransaction);

							if (localErrorString.length() > 0) {
								errorString += "ackTransactionList[" + String.valueOf(objIndex) + "] "
										+ localErrorString + " | ";
							} else {
								// save to DB
								String policyNumber = getPolicyNumber(ackTransaction);

								API_RLS_PYMT_STATUS paymentStatusDao = new API_RLS_PYMT_STATUS();
								
								
								String status = null;
								try {
									if(ackTransaction.has("txnStatus")) {
										status = ackTransaction.get("txnStatus").getAsString();
									}
								}catch (Exception ex){
									Log.error(ex);
								}
								
								if(status != null) {
									switch (status) {
									case "0":
										status = "Y";
										break;
									case "1":
										status = "N";
										break;
										default:
											break;
									}
								}
								
								boolean result = paymentStatusDao.create(audID, policyNumber, status, "C");

								Log.debug("*** appStatusDao=" + result);
							}
						}

						objIndex++;
					}
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
			errorString += "Error - validateCallbackContent=" + ex.getMessage();
		}

		return errorString;
	}

	public Response createSuccessResponse(String audID) {
		Log.info("Goto PaymentCallbackService.createSuccessResponse");

		JsonObject successResponseNode = new JsonObject();

		JsonObject asyncrecPaymentResponseStatus = new JsonObject();
		asyncrecPaymentResponseStatus.addProperty("code", "200");
		asyncrecPaymentResponseStatus.addProperty("desc", "Success");

		successResponseNode.add("asyncAppResponseStatus", asyncrecPaymentResponseStatus);

		return RestUtil.toResponse(audID, Response.Status.OK, successResponseNode.toString());
	}

	public Response createFailureResponse(String audID, String reason) {
		Log.info("Goto PaymentCallbackService.createFailureResponse");

		JsonObject failureResponseNode = new JsonObject();

		JsonObject exception = new JsonObject();
		exception.addProperty("message", "Failure");
		exception.addProperty("reasonCode", "500");
		exception.addProperty("reason", reason);

		failureResponseNode.add("exception", exception);

		return RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, failureResponseNode.toString());
	}
	
	public boolean createTransactionRecord(String audID, String headerAsString, String body, String reqID) throws Exception {
		Log.info("Goto PaymentCallbackService.createTransactionRecord");
		API_RLS_PYMT_NOTI_TRX trxPaymentNotiDao = new API_RLS_PYMT_NOTI_TRX();
		return trxPaymentNotiDao.create(audID, headerAsString, body, reqID);
	}

	public boolean updateTransactionRecord(String audID, String responseHeaderAsString, String responseAsString,
			boolean isCompleted) throws Exception {
		Log.info("Goto PaymentCallbackService.updateTransactionRecord");
		API_RLS_PYMT_NOTI_TRX trxPaymentNotiDao = new API_RLS_PYMT_NOTI_TRX();
		return trxPaymentNotiDao.update(audID, responseHeaderAsString, responseAsString, isCompleted);
	}
	
	private String validateAckTransaction(JsonObject ackTransactionJson) {
		Log.info("Goto PaymentCallbackService.validateAckTransaction");

		String errorString = "";

		// Do nothing for coming data from AXA.
		// Just leave this function here for future validation use.

		return errorString;
	}

	private String getPolicyNumber(JsonObject ackTransactionJson) {
		Log.info("Goto PaymentCallbackService.getPolicyNumber");

		String policyNumberAsString = "";
		try {
			if (ackTransactionJson.has("policyNumber")) {
				policyNumberAsString = ackTransactionJson.get("policyNumber").getAsString();
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		return policyNumberAsString;
	}

}
