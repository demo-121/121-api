package com.eab.rest.ease;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
public class root {
	@GET
	@Produces("application/json")
	public String rootAPI() {
		return "{\"Message\":\"Unauthorized Access\"}";
	}
}