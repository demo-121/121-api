package com.eab.rest.ease.spe;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.eab.common.RestUtil;
import com.eab.common.EnvVariable;

@Path("/spe")
public class SPE {
	@POST
	@Path("")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	//@Path("/{path}")
	//public Response spe(@Context HttpHeaders headers, @PathParam("path") String path) throws Exception {
	public Response spe(@Context HttpHeaders headers, String body) {
		//EnvVariable spePara = new EnvVariable();
		Response output = null;
		String audID = headers.getRequestHeader("aud-id").get(0);
		String outString = "";
		
		outString += "IP:" + EnvVariable.get("SPE_IP") + "\r\n";
		outString += "Port:" + EnvVariable.get("SPE_PORT") + "\r\n";
		outString += "Username:" + EnvVariable.get("SPE_USERNAME") + "\r\n";
		outString += "Password:" + EnvVariable.get("SPE_PASSWORD") + "\r\n";
		outString += "Path:" + EnvVariable.get("SPE_LOCAL_TEMP_PATH") + "\r\n";
		outString += "Body:" + body + "\r\n";
		
		output = RestUtil.toResponse(audID, Response.Status.OK, outString);
		return output;
	}
}
