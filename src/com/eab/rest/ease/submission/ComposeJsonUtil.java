package com.eab.rest.ease.submission;

import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.rest.ease.submission.applicationBean.ApplicationBean;
import com.eab.rest.ease.submission.model.*;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

class ComposeJsonUtil {

	/**
	 * To compose the Submission Bean Object from the data of appBean
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	SubBean prepareSubBean(ApplicationBean appBean) throws Exception {
		SubBean subBean = new SubBean();
		
		try {
			subBean.recordApplicationRequest.policy = preparePolicy(appBean);
			
			return subBean;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return subBean;
		}
	}

	/**
	 * Retrieve the Policy Object
	 * RecordApplicationRequest > policy
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private Policy preparePolicy(ApplicationBean appBean) throws Exception {
		Policy policy = new Policy();

		try {

			//CAPTURE DATE *
			//if (applicationDoc.has("applicationSubmittedDate")) {
			//	Date applicationStartedDate = new Date(applicationDoc.getLong("applicationSubmittedDate"));
			//	obj.recordApplicationRequest.policy.applicationCaptureDT = changeDateFormat(applicationStartedDate);
			//}
			//else {
			//	throw new IOException("Mandatory field missing - application capture date");
			//}

			//Capture date: Date on which Supervisor Approved/Rejected or case expired or invalidated with payment made
			//Submit date:  Date on which Supervisor Approved/Rejected or case expired or invalidated with payment made

			policy.applicationCaptureDT = "";
			policy.policySubmitDT = "";
			policy.applicationSubmitTM = "";

			if (appBean.isAPPROVE_REJECT) {
				policy.applicationCaptureDT = appBean.approveRejectDate;
				policy.policySubmitDT = appBean.approveRejectDate;
				policy.applicationSubmitTM = appBean.approveRejectTime;                
			}

			if (appBean.isINVALIDATED) {
				policy.applicationCaptureDT = appBean.invalidatedDate;
				policy.policySubmitDT = appBean.invalidatedDate;
				policy.applicationSubmitTM = appBean.invalidatedTime;
			}

			if (appBean.isEXPIRED) {
				policy.applicationCaptureDT = appBean.expiredDate;
				policy.policySubmitDT = appBean.expiredDate;
				policy.applicationSubmitTM = appBean.expiredTime;
			}

			//make a copy to appBean
			appBean.policySubmitDT = policy.policySubmitDT;
			policy.applicationSignatureDT = appBean.signatureDate;

			//COMPANY CODE *
			policy.companyCD = "5";

			//POLICY NO. *
			if (appBean.applicationDoc.has("policyNumber")) {
				policy.policyNO = appBean.applicationDoc.getString("policyNumber");
			}

			//SOURCE CODE
			policy.sourceChannelCD = "";

			//AUTO SET FLAG

			//sample obj.recordApplicationRequest.policy.autoSetFLG = "N";
			policy.autoSetFLG = "Y";
			if (appBean.appForm_basicValues_planDetails.has("isBackDate")) {
				if ("Y".equals(appBean.appForm_basicValues_planDetails.getString("isBackDate"))) {
					policy.autoSetFLG = "N";
				}
			}

			//COMMUNICATION MEANS *
			if (appBean.ismAddrdifferent) {
				policy.communicationMode = "E";
			} else {
				policy.communicationMode = "P";
			}

			if ("".equals(policy.communicationMode)) {
				throw new IOException("Mandatory field missing - communication mode");
			}

			//APP STATUS CODE
			//"11":
			//1. (Reject Case):					SupervisorApproval.status is "R"
			//2. (Paid Case):					Paid && (Application.status is Invalidated
			//3. (approval status is expired):	SupervisorApproval.status is expired)
			policy.policyStatusCD = "";

			if ("A".equals(appBean.approvalStatus))
				policy.policyStatusCD = "00";
			else
				policy.policyStatusCD = "11";

			//            if ("R".equals(appBean.approvalStatus) || (appBean.isPaid && ("INVALIDATED".equals(appBean.applicationStatus))) || "E".equals(appBean.approvalStatus)) {
			//                policy.policyStatusCD = "11";
			//            }

			//DEP AMOUNT *
			policy.depositAMT = "0";

			if (appBean.isCheqPayment) {
				if (appBean.quotation.has("premium")) {
					policy.depositAMT = Double.toString(appBean.quotation.getDouble("premium"));
				}
			}


			//Boolean paid = true;
			//paid = application.paid;
			//if applicaton.paymentMethod in (Credit Card / eNet / IPP)
			//          paid = true;
			//else
			//          paid = false;

			//POLICY DATE

			policy.policyEffectiveDTTM = "";

			//fixing for riskCommenceDate position change
			//if (appform_basicvalue_plandetails.has("riskCommenDate")) {
			if (appBean.quotation.has("riskCommenDate")) {
				String Tmp_riskCommenceDate = appBean.quotation.getString("riskCommenDate");
				policy.policyEffectiveDTTM = appBean.quotation.getString("riskCommenDate");
				appBean.riskCommenceDate = policy.policyEffectiveDTTM;
				Integer date_figure = Integer.valueOf(Tmp_riskCommenceDate.substring(8, 10));

				if (date_figure > 28) {
					policy.policyEffectiveDTTM = Tmp_riskCommenceDate.substring(0, 8) + "28";
					//riskCommDate = obj.recordApplicationRequest.policy.policyEffectiveDTTM ;
				}
			}

			policy.languageCD = "E";



			//PaymentCodes - call 1
			appBean.paymentType = "INIT";
			//appBean.parm_PlanCD = appBean.basicPlanCode;
			//appBean.parm_PaymentMethod = appBean.paymentMethod;
			appBean.paymentMode = "M";
			appBean.useGiroInd = "N";


			//"INIT" : initial payment "M" is paymode for ispireD, "N": GIRO, no GIRO for initial payment
			appBean.initialPayment.setPayKeys(appBean.paymentType, appBean.basicPlanCode, appBean.paymentMethod,
					appBean.paymentMode, appBean.useGiroInd);
			appBean.initialPayment.retrievePaymentRecord();
			appBean.isInspireProduct = appBean.initialPayment.isInspirePROD();

			//PAYMENT MODE            
			policy.paymentModeCD = "";

			if (appBean.appForm_basicValues_planDetails.has("paymentMode")) {
				appBean.paymentMode = appBean.appForm_basicValues_planDetails.getString("paymentMode");
				policy.paymentModeCD = getPaymentModeCD(appBean.paymentMode);
				appBean.paymentModeCodeMapped = policy.paymentModeCD;
			} else {
				throw new IOException("Mandatory field missing - payment mode code");
			}

			if (appBean.isInspireProduct) {
				policy.paymentModeCD = "M";
				appBean.paymentModeCodeMapped = policy.paymentModeCD;
			}

			if (appBean.paymentMode.equals("L")) {
				policy.singlePremiumPolicyFLG = "Y";
				appBean.singlePremiumPolicyFLG = policy.singlePremiumPolicyFLG;
			} else { 
				policy.singlePremiumPolicyFLG = "";
				appBean.singlePremiumPolicyFLG = policy.singlePremiumPolicyFLG;
			}

			//PAYMENT METHOD
			policy.paymentMethodCD = "";

			if (appBean.isInspireProduct) {
				policy.paymentMethodCD = appBean.initialPayment.getPaymentMethod();
			}

			//CURRENCY
			if (appBean.payment != null && appBean.payment.has("policyCcy")) {
				policy.currencyCD = appBean.payment.getString("policyCcy");
			} else {
				policy.currencyCD = "";
			}

			//ANNUAL PREM EXCLUDE TOP-UP
			if (appBean.appForm_basicValues_planDetails.has("totYearPrem")) {
				policy.minPremiumAMT = Double.toString(appBean.appForm_basicValues_planDetails.getDouble("totYearPrem"));
			} else {
				throw new IOException("Mandatory field missing - minPremiumAMT.");
			}

			//MODAL PREM EXCLUDE TOP-UP R
			switch (appBean.paymentMode) {
			case "L":
				if (appBean.quotation.has("totYearPrem")) {
					policy.premiumAMT = Double.toString(appBean.quotation.getDouble("totYearPrem"));
				}
				break;
			case "A":
				if (appBean.quotation.has("totYearPrem")) {
					policy.premiumAMT = Double.toString(appBean.quotation.getDouble("totYearPrem"));
				}
				break;
			case "S":
				if (appBean.quotation.has("totHalfyearPrem")) {
					policy.premiumAMT = Double.toString(appBean.quotation.getDouble("totHalfyearPrem"));
				}
				break;
			case "Q":
				if (appBean.quotation.has("totQuarterPrem")) {
					policy.premiumAMT = Double.toString(appBean.quotation.getDouble("totQuarterPrem"));
				}
				break;
			case "M":
				if (appBean.quotation.has("totMonthPrem")) {
					policy.premiumAMT = Double.toString(appBean.quotation.getDouble("totMonthPrem"));
				}
				break;
			default:
				Log.error("invalid Pay_Frequency");
				break;
			}

			//TOP-UP REGULAR MODAL PREMIUM
			//sample: obj.recordApplicationRequest.policy.topupRegularModalPremium = "0";
			if (appBean.payment != null && appBean.payment.has("initRspPrem")) {
				policy.topupRegularModalPremium = Double.toString(appBean.payment.getDouble("initRspPrem"));
			}

			//INITIAL LUMP SUM PREMIUM
			//hard code 5000 firstly from feedback on 10/12/2017 11:50 AM
			//obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = "5000";
			//obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = "0";

			policy.initialLumpsumPremiumAMT = Double.toString(appBean.quotation.getDouble("premium"));
			//if (payment.has("initBasicPrem")) {
			//	obj.recordApplicationRequest.policy.initialLumpsumPremiumAMT = Double.toString(payment.getDouble("initBasicPrem"));
			//}

			//LUMP SUM PREMIUM
			//sample: obj.recordApplicationRequest.policy.lumpsumPremiumAMT = "0";
			if (appBean.payment != null && appBean.payment.has("topupPrem")) {
				policy.lumpsumPremiumAMT = Double.toString(appBean.payment.getDouble("topupPrem"));
			}



			//Prepare the List Object in Details
			policy.hasDetailsOfPolicyIn = prepareHasDetailsOfPolicyIn(appBean);  //bernard:done
			policy.hasProducerInformationIn = prepareHasProducerInformationIn(appBean); //bernard:done
			policy.hasDetailsOfRisksIn = prepareHasDetailsOfRisksIn(appBean);
			policy.hasCustomerInformationIn = prepareHasCustomerInformationIn(appBean);
			policy.hasBenificialOwnerDetailsIn = prepareHasBenificialOwnerDetailsIn(appBean);
			policy.hasPolicyChargeInfoIn = prepareHasPolicyChargeInfoIn(appBean);
			//policy.hasLifePolicyTransactionDetailsIn = prepareHasLifePolicyTransactionDetailsIn(appBean);
			policy.hasLifePolicyTransactionDetailsIn = null; // To check with Kelvin whether it is still needed.
			policy.forLifeUnitofExposure = prepareForLifeUnitofExposure(appBean);
			policy.isAssociatedWithLifeInsuranceProduct = null; // not required to map
			policy.hasNotificationsDetailsIn = null; // not required to map
			policy.hasSharedProducerInformationIn = null; // not required to map
			policy.hasDetailsRecordIn = prepareHasDetailsRecordIn(appBean);
			policy.hasPolicyRemarkDetailsIn = null; // not required to map
			policy.hasCaseUnderwriterDetailsIn = null; // not required to map
			policy.hasPolicyAssociationDetailsIn = null; // not required to map
			policy.hasAssessmentHeadersIn = prepareHasAssessmentHeadersIn(appBean);
			policy.hasAXAAsiaPolicyPaymentAccount = prepareHasAXAAsiaPolicyPaymentAccount(appBean);
			policy.hasAXAAsiaPolicyHeaderPaymentAccount = null; // not required to map
			policy.hasUnderwritingDecisionsIn = null; // not required to map
			policy.hasDependentInformationIn = null; // not required to map
			policy.hasPreviousPolicyDetailsIn = null; // not required to map

			determineMedicalIND(appBean);
			if (appBean.isPolicyPMED) {
				policy.medicalIND = "Y";
			}

			return policy;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return policy;
		}

	}

	/**
	 * Prepare the Object of Policy Details
	 * RecordApplicationRequest > policy > hasDetailsOfPolicyIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasDetailsOfPolicyIn> prepareHasDetailsOfPolicyIn(ApplicationBean appBean) throws Exception {

		List<HasDetailsOfPolicyIn> hasDetailsOfPolicyInList = new ArrayList<>();
		HasDetailsOfPolicyIn hasDetailsOfPolicyIn = new HasDetailsOfPolicyIn();

		try {

			//U/W REMARKS 1
			hasDetailsOfPolicyIn.uwRemark1 = "";

			if ("R".equals(appBean.approvalStatus)) {
				hasDetailsOfPolicyIn.uwRemark1 = "eApp Rejected by Supervisor";
			}

			if ((appBean.isPaid && ("INVALIDATED".equals(appBean.applicationStatus)))) {
				hasDetailsOfPolicyIn.uwRemark1 = "eApp invalid (paid).";
			}

			if ("E".equals(appBean.approvalStatus)) {
				hasDetailsOfPolicyIn.uwRemark1 = "eApp Expired (paid).";
			}

			//add policy status/approval status to policy summary remark: BD19-008
			if (!("".equals(hasDetailsOfPolicyIn.uwRemark1))) {
				appBean.Pol_remarks.addpolicyRemarks(hasDetailsOfPolicyIn.uwRemark1);
			}

			//OWNER AGE
			//sample:HasDetailsOfPolicyIn.ownerAgeAtIssue = "54";

			//Todo The following 2 logic, which one is correct?
			if (appBean.isThirdParty) {
				hasDetailsOfPolicyIn.ownerAgeAtIssue = Integer.toString(appBean.ow.issueAge);
			}

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("age")) {
					hasDetailsOfPolicyIn.ownerAgeAtIssue = Integer.toString(appBean.Owner_personalInfo.getInt("age"));
				}
			}


			//DIVIDEND OPTION
			//sample HasDetailsOfPolicyIn.dvdoIND = "2";
			if (appBean.quotation.has("divInd")) {
				hasDetailsOfPolicyIn.dvdoIND = appBean.quotation.getString("divInd");
			} else {
				hasDetailsOfPolicyIn.dvdoIND = "";
			}

			//DEATH BENEFIT OPTION
			//not in release 1
			//HasDetailsOfPolicyIn.dboIND = "C";
			if (appBean.quotation.has("deathBenF")) {
				hasDetailsOfPolicyIn.dboIND = appBean.quotation.getString("deathBenF");
			} else {
				hasDetailsOfPolicyIn.dboIND = "";
			}

			String blockFieldName = null;
			//String IsHaveCpfAcct_fieldNM = null;
			String withCPFAccountFlagNM = null;            

			if (appBean.quotation_policyOptions.has("paymentMethod")) {
				if (appBean.isCPFPayment) {
					appBean.payorPaymentMethodCode = appBean.quotation_policyOptions.getString("paymentMethod");
					blockFieldName = appBean.payorPaymentMethodCode + "Block";

					if (appBean.payment.has(blockFieldName)) {
						appBean.CPFpaymentBlockNM = appBean.payment.getJSONObject(blockFieldName);
					} else {
						throw new IOException(appBean.payorPaymentMethodCode + " payment block missing");
					}

					//IsHaveCpfAcct_fieldNM = appBean.payorPaymentMethodCode + "IsHaveCpfAcct";
					appBean.investmentAcctNoFieldName = appBean.payorPaymentMethodCode + "InvmAcctNo";
					appBean.bankNamePickerFieldName = appBean.payorPaymentMethodCode + "BankNamePicker";
					withCPFAccountFlagNM = appBean.payorPaymentMethodCode + "IsHaveCpfAcct";

					if (appBean.CPFpaymentBlockNM.has(withCPFAccountFlagNM) && ("Y".equals(appBean.CPFpaymentBlockNM.getString(withCPFAccountFlagNM)))) {
						appBean.withCPFAccount = true;
					}
				}
			}

			//CPF A/C TYPE
			hasDetailsOfPolicyIn.cpfAcctTypeCD = "";

			if (appBean.isCPFPayment) {
				switch (appBean.payorPaymentMethodCode) {
				case "cpfisoa":
					hasDetailsOfPolicyIn.cpfAcctTypeCD = "O";
					break;
				case "cpfissa":
					hasDetailsOfPolicyIn.cpfAcctTypeCD = "S";
					break;
				case "srs":
					hasDetailsOfPolicyIn.cpfAcctTypeCD = "R";
					break;
				default:
					hasDetailsOfPolicyIn.cpfAcctTypeCD = "";
					break;
				}
			}

			//Do not consider without CPY payment scenario.
			//if (isCPFPayment&&(!withCPFAccount)) {
			//	HasDetailsOfPolicyIn.cpfAcctTypeCD = "";
			//}

			//PAYMENT MODE OF DE-LINK RTU
			hasDetailsOfPolicyIn.rtuPaymentModeCD = "";

			//PAYMENT METHOD OF DE-LINK RTU
			hasDetailsOfPolicyIn.rtuPaymentMethodCD = "";

			if (appBean.isInspireProduct) {
				if (appBean.payment != null && appBean.payment.getDouble("initRspPrem") > 0) {
					appBean.paymentType = "RSP";
					//.parm_PlanCD = appBean.basicPlanCode;
					//appBean.parm_PaymentMethod = appBean.paymentMethod;
					appBean.paymentMode = "";

					if (appBean.appForm_basicValues_planDetails.has("rspPayFreq")) {
						switch (appBean.appForm_basicValues_planDetails.getString("rspPayFreq").toUpperCase()) {
						case "ANNUAL":
							appBean.paymentMode = "A";
							break;
						case "SEMIANNUAL":
							appBean.paymentMode = "S";
							break;
						case "QUARTERLY":
							appBean.paymentMode = "Q";
							break;
						default:
							appBean.paymentMode = "M";
							break;
						}
						hasDetailsOfPolicyIn.rtuPaymentModeCD = appBean.paymentMode;
					}
					appBean.useGiroInd = "N";

					if (appBean.payment.has("subseqPayMethod")) {
						if ("Y".equals(appBean.payment.getString("subseqPayMethod"))) {
							appBean.useGiroInd = "Y";
						}
					}

					PaymentCodes rspPayment = new PaymentCodes();
					//"INIT" : initial payment ; GIRO
					rspPayment.setPayKeys(appBean.paymentType, appBean.basicPlanCode,
							appBean.paymentMethod, appBean.paymentMode, appBean.useGiroInd);
					rspPayment.retrievePaymentRecord();
					hasDetailsOfPolicyIn.rtuPaymentMethodCD = rspPayment.getPaymentMethod();
				}
			}


			hasDetailsOfPolicyInList.add(hasDetailsOfPolicyIn);
			return hasDetailsOfPolicyInList;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasDetailsOfPolicyInList;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > hasProducerInformationIn
	 *
	 * @param appBean
	 * @throws Exception
	 */
	private List<HasProducerInformationIn> prepareHasProducerInformationIn(ApplicationBean appBean) throws Exception {

		HasProducerInformationIn hasProducerInformationIn = new HasProducerInformationIn();
		List<HasProducerInformationIn> hasProducerInformationInList = new ArrayList<>();

		try {

			String tmp_agentcode = "";
			String agentID = "";

			if (appBean.quotation_agent.has("agentCode")) {
				tmp_agentcode = appBean.quotation_agent.getString("agentCode");
			} else {
				throw new IOException("Mandatory field missing - agent code not found");
			}

			//direct get from bundle
			JSONObject agentView = Constant.CBUTIL.getDoc("_design/main/_view/agents?startkey=[\"01\",\"0\"]&endkey=[\"01\",\"ZZ\"]&stale=ok");
			JSONArray agentArray = agentView.getJSONArray("rows");
			JSONObject agentDoc_tmp = null;

			for (int i = 0; i < agentArray.length(); i++) {
				JSONObject agentObject = agentArray.getJSONObject(i).getJSONObject("value");
				if (agentObject.getString("agentCode").equals(tmp_agentcode)) {
					agentID = agentArray.getJSONObject(i).getString("id"); //for easy testing
					//if (agentObject.has("agentCodeDisp")) {  				//comment to get it from agent profile as dev do not has this field
					//	agentID = agentObject.getString("agentCodeDisp");
					//}
					agentDoc_tmp = Constant.CBUTIL.getDoc(agentID);

					if (agentDoc_tmp.has("agentCodeDisp")) {
						agentID = agentDoc_tmp.getString("agentCodeDisp");
						break;
					}
				}
			}

			if (agentDoc_tmp == null) {
				throw new IOException("Agent Profile Missing.");
			} else {
				hasProducerInformationIn.producerID = agentID.replaceAll("-", "");
				//HasProducerInformationIn.producerID = add_leading_zero(agentID).replaceAll("-", "");
				//HasProducerInformationIn.producerID = tmp_producerID.substring(tmp_producerID.length() -16, 16);
			}

			hasProducerInformationIn.hasAssociationWith = prepareHasAssociationWith(appBean);
			hasProducerInformationInList.add(hasProducerInformationIn);
			return hasProducerInformationInList;


		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasProducerInformationInList;
		}
	}

	/**
	 * Prepare
	 * RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith [ArrayList]
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAssociationWith> prepareHasAssociationWith(ApplicationBean appBean) throws Exception {

		List<HasAssociationWith> hasAssociationWithList = new ArrayList<>();
		HasAssociationWith hasAssociationWith = new HasAssociationWith();
		HasBankReferrerInfoIn hasBankReferrerInfoIn = new HasBankReferrerInfoIn();

		try {

			appBean.Owner_personalInfo_branchInfo = null;
			appBean.isSINGPOST = false;

			if (appBean.quotation.has("agent")) {
				if ("SINGPOST".equals(appBean.quotation_agent.getString("dealerGroup").toUpperCase())) {
					appBean.isSINGPOST = true;
					appBean.Owner_personalInfo_branchInfo = appBean.Owner_personalInfo.getJSONObject("branchInfo");
				}
			}

			//Bank Code
			//hasAssociationWith.bankCD = "1";

			//Bank Branch Code
			//hasAssociationWith.bankBranchCD = "";
			if (appBean.isSINGPOST) {
				hasAssociationWith.bankBranchCD = getBankCD(appBean.Owner_personalInfo_branchInfo.getString("branch"));

				//Bank Code
				hasAssociationWith.bankCD = "1";

				//Region Code
				hasAssociationWith.stateRegionCD = "00";

				//Area Code
				hasAssociationWith.areaCD = "00";

				//Lead category
				hasBankReferrerInfoIn.leadCategoryCD = "";   
			} else {
				hasAssociationWith.bankBranchCD = "";

				//Bank Code
				hasAssociationWith.bankCD = "";

				//Region Code
				hasAssociationWith.stateRegionCD = "";

				//Area Code
				hasAssociationWith.areaCD = "";

				//Lead category
				hasBankReferrerInfoIn.leadCategoryCD = "";
			}

			//Region Code
			//hasAssociationWith.stateRegionCD = "00";

			//Area Code
			//hasAssociationWith.areaCD = "00";

			hasAssociationWith.hasStaffInformationIn = prepareHasStaffInformation(appBean);

			hasAssociationWithList.add(hasAssociationWith);
			return hasAssociationWithList;


		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasAssociationWithList;
		}
	}

	/**
	 * Prepare Array Object of Life Insured
	 * RecordApplicationRequest > policy > hasDetailsOfRisksIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasDetailsOfRisksIn> prepareHasDetailsOfRisksIn(ApplicationBean appBean) throws Exception {

		List<HasDetailsOfRisksIn> hasDetailsOfRisksInList = new ArrayList<>();
		HasDetailsOfRisksIn hasDetailsOfRisksIn = new HasDetailsOfRisksIn();

		try {
			//Issue Ages
			hasDetailsOfRisksIn.issueAge = "0";            
			hasDetailsOfRisksIn.issueAge = Integer.toString(appBean.la.issueAge);


			hasDetailsOfRisksIn.hasPersonalDetailsIn = prepareHasPersonalDetailsIn(appBean);
			//hasDetailsOfRisksIn.hasUndewritingResultsIn = null; //This piece of information is not required

			hasDetailsOfRisksInList.add(hasDetailsOfRisksIn);
			return hasDetailsOfRisksInList;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasDetailsOfRisksInList;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > hasCustomerInformationIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasCustomerInformationIn> prepareHasCustomerInformationIn(ApplicationBean appBean) throws Exception {
		List<HasCustomerInformationIn> hasCustomerInformationInList = new ArrayList<>();
		HasCustomerInformationIn hasCustomerInformationIn = new HasCustomerInformationIn();
		try {

			//RECEIVE SMS FLAG
			hasCustomerInformationIn.smsTextableFlg = "N";

			//DEPENDENT NO.
			hasCustomerInformationIn.dependentNO = "1";

			//OWNERSHIP
			//sample: obj.ownerIND = "I";
			hasCustomerInformationIn.ownerIND = "I";


			if ((appBean.proposer_dependant != null) && appBean.isThirdParty) {
				//String dependent_code = "";

				if (appBean.proposer_dependant.has("relationship")) {
					switch (appBean.proposer_dependant.getString("relationship")) {
					case "SPO":
						hasCustomerInformationIn.ownerIND = "S";
						break;
					case "MOT":
						hasCustomerInformationIn.ownerIND = "P";
						break;
					case "FAT":
						hasCustomerInformationIn.ownerIND = "P";
						break;
					default:
						break;
					}
				} else {
					hasCustomerInformationIn.ownerIND = "O";
				}
			}

			if (appBean.proposer_dependant == null) {
				hasCustomerInformationIn.ownerIND = "I";
			}
			appBean.ownerIND = hasCustomerInformationIn.ownerIND;

			hasCustomerInformationIn.canBeIndividual = prepareCanBeIndividual(appBean);

			hasCustomerInformationInList.add(hasCustomerInformationIn);
			return hasCustomerInformationInList;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasCustomerInformationInList;
		}
	}

	/**
	 * RecordApplicationRequest > policy > hasPolicyChargeInfoIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasPolicyChargeInfoIn> prepareHasPolicyChargeInfoIn(ApplicationBean appBean) throws Exception {
		List<HasPolicyChargeInfoIn> hasPolicyChargeInfoInList = new ArrayList<>();
		HasPolicyChargeInfoIn hasPolicyChargeInfoIn = new HasPolicyChargeInfoIn();
		try {


			//CHARGE CODE
			//sample: HasPolicyChargeInfoInItemType.chargeCD = "BOND";
			if (appBean.hasFundCharge) {
				hasPolicyChargeInfoIn.chargeCD = appBean.initialPayment.getULChargeCD();
				//sales fee:
				//CHARGE %
				//sample: obj.chargePCT = "9876123476231.98";
				hasPolicyChargeInfoIn.chargePCT = "0.00";

				if (appBean.appForm_basicValues_planDetails.has("singlePremSalesCharge")) {
					Double tmp_int_change = 0.00;
					tmp_int_change = Math.round(appBean.appForm_basicValues_planDetails.getDouble("singlePremSalesCharge") * 100.0) / 100.0;
					hasPolicyChargeInfoIn.chargePCT = (Double.toString(tmp_int_change).concat("000")).substring(0, 4);
				}

				hasPolicyChargeInfoInList.add(hasPolicyChargeInfoIn);
				//service fee:
				if (appBean.appForm_basicValues_planDetails.has("serviceFee") && (appBean.appForm_basicValues_planDetails.getDouble("serviceFee") > 0)) {
					hasPolicyChargeInfoIn = new HasPolicyChargeInfoIn();
					hasPolicyChargeInfoIn.chargeCD = "IM07";
					hasPolicyChargeInfoIn.chargePCT = "0.00";

					if (appBean.appForm_basicValues_planDetails.has("serviceFee")) {
						Double tmp_int_change = 0.00;
						tmp_int_change = Math.round(appBean.appForm_basicValues_planDetails.getDouble("serviceFee") * 100.0) / 100.0;
						hasPolicyChargeInfoIn.chargePCT = (Double.toString(tmp_int_change).concat("000")).substring(0, 4);
					}
					hasPolicyChargeInfoInList.add(hasPolicyChargeInfoIn);
				}
				//Add the full list of charge fee:

				return hasPolicyChargeInfoInList;

			}else {
				return null;
			}



		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return null;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > hasLifePolicyTransactionDetailsIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasLifePolicyTransactionDetailsIn> prepareHasLifePolicyTransactionDetailsIn(ApplicationBean appBean) throws Exception {
		List<HasLifePolicyTransactionDetailsIn> hasLifePolicyTransactionDetailsInList = new ArrayList<>();
		HasLifePolicyTransactionDetailsIn hasLifePolicyTransactionDetailsIn = new HasLifePolicyTransactionDetailsIn();
		try {
			//CASH AMT - K47CSHAMT

			hasLifePolicyTransactionDetailsIn.premiumAMT = "1000000.11";

			if (!appBean.isCreditCardPayment) {
				hasLifePolicyTransactionDetailsIn.premiumAMT = Double.toString(appBean.quotation.getDouble("premium"));
			}

			//TRX CODE
			hasLifePolicyTransactionDetailsIn.transTypeCD = "1";
			//Payment CCY
			hasLifePolicyTransactionDetailsIn.transCurrencyCD = "SGD";

			hasLifePolicyTransactionDetailsIn.hasLifePolicyTransactionHeaderDetailsIn = prepareHasLifePolicyTransactionHeaderDetailsIn(appBean);
			hasLifePolicyTransactionDetailsIn.hasPaymentDetailsIn = null; // not required to map


			hasLifePolicyTransactionDetailsInList.add(hasLifePolicyTransactionDetailsIn);
			return hasLifePolicyTransactionDetailsInList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasLifePolicyTransactionDetailsInList;
		}
	}

	/**
	 * RecordApplicationRequest > policy > hasLifePolicyTransactionDetailsIn > hasLifePolicyTransactionHeaderDetailsIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasLifePolicyTransactionHeaderDetailsIn> prepareHasLifePolicyTransactionHeaderDetailsIn(ApplicationBean appBean) throws Exception {
		List<HasLifePolicyTransactionHeaderDetailsIn> objList = new ArrayList<>();
		HasLifePolicyTransactionHeaderDetailsIn obj = new HasLifePolicyTransactionHeaderDetailsIn();
		try {

			//CCP AMT
			//sample :HasLifePolicyTransactionHeaderDetailsInItemType.ccLocalAMT = "123456789876.08";


			//CREDITCARD NO
			//sample: HasLifePolicyTransactionHeaderDetailsInItemType.creditCardNO = "1234567898765432";

			//BANK ACC NO
			//mandatory field
			//shield - 'L-CHKS'
			//non-shield - 'L-CHK'
			obj.bankAccountNO = "L-CHK";

			//BANK ACC NO
			//obj.cardExpirationDT = "999912";

			//AUTH CODE
			//sample: obj.authenticationCD = "000000";

			//REMARKS
			//sample: obj.paymentRemarks = "132";
			//"EASE - CC" when the payment is made via CC.
			//"EASE - eNets" when the payment is made via enets
			//"EASE - CC IPP" when the payment is made via Credit card IPP

			//BANK CODE
			//sample: obj.bankCD = "A12";


			//BRANCH CODE
			//obj.branchCD = "786TR4";

			//CHEQUE NUMBER
			//sample: obj.chequeNO = "710098761234";

			//CHEQUE DATE
			//sample: obj.chequeDT = this.getDummyDate();

			if (appBean.isCashPayment) {
				obj.ccLocalAMT = Double.toString(appBean.quotation.getDouble("premium"));
				obj.creditCardNO = "0000000000000000";
				obj.authenticationCD = "000000";

				if ("crCard".equals(appBean.paymentMethod)) {
					obj.paymentRemarks = "EASE - CC";
					obj.bankCD = "DBS";
					obj.branchCD = "0";//TO DO: not in excel
					obj.cardExpirationDT = "999912";

					obj.chequeNO = "EASE CC";
					obj.chequeDT = getCurrentDate();
				}

				if ("eNets".equals(appBean.paymentMethod)) {
					obj.paymentRemarks = "EASE - CC";
					obj.bankCD = "DBS";
					obj.branchCD = "0";//TO DO: not in excel
					obj.cardExpirationDT = "999912";
					obj.bankAccountNO = "L-CHK";
					obj.chequeNO = "EASE Nets";
					obj.chequeDT = getCurrentDate();

				}

				if ("dbsCrCardIpp".equals(appBean.paymentMethod)) {
					obj.paymentRemarks = "EASE - CC IPP";
					obj.bankCD = "";
					obj.branchCD = "0";//TO DO: not in excel
					obj.cardExpirationDT = "999912";
					obj.bankAccountNO = "L-CHK";
					obj.chequeNO = "EASE IPP";
					obj.chequeDT = getCurrentDate();
				}
			}


			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Funds Detail Information
	 * return null if there is no fund.
	 * RecordApplicationRequest > policy > hasDetailsRecordInItemType
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasDetailsRecordIn> prepareHasDetailsRecordIn(ApplicationBean appBean) throws Exception {
		List<HasDetailsRecordIn> objList = new ArrayList<>();
		HasDetailsRecordIn obj = new HasDetailsRecordIn();

		try {

			Integer investment_fund = 0;

			if (appBean.appForm_basicValues_planDetails.has("fundList")) {
				JSONArray appform_basicvalue_plandetails_fundList = appBean.appForm_basicValues_planDetails.optJSONArray("fundList");

				if (appform_basicvalue_plandetails_fundList.length() > 0) {
					List<HasAccountDetailsIn> hasAccountDetailsInList = new ArrayList<HasAccountDetailsIn>();

					JSONObject fund_allocation = null;

					for (int fund_count = 0; fund_count < appform_basicvalue_plandetails_fundList.length(); fund_count++) {
						fund_allocation = appform_basicvalue_plandetails_fundList.getJSONObject(fund_count);
						HasAccountDetailsIn hasAccountDetailsIn = new HasAccountDetailsIn();

						List<HasInvestmentFundDetailsIn> hasInvestmentFundDetailsInList = new ArrayList<HasInvestmentFundDetailsIn>();
						HasInvestmentFundDetailsIn hasInvestmentFundDetailsIn = new HasInvestmentFundDetailsIn();

						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn > hasInvestmentFundDetailsIn(InvestmentFund) - S

						//FUND CODE
						if (fund_allocation.has("fundCode")) {
							hasInvestmentFundDetailsIn.investmentFundCd = fund_allocation.getString("fundCode");
						}

						hasInvestmentFundDetailsInList.add(hasInvestmentFundDetailsIn);

						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn > hasInvestmentFundDetailsIn(InvestmentFund) - E
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn - S

						//ALLOCATION %
						if (fund_allocation.has("alloc")) {
							hasAccountDetailsIn.allocPct = Double.toString(fund_allocation.getDouble("alloc"));

							if ("L".equals(appBean.paymentMode)) {
								hasAccountDetailsIn.lumpsumAllocPCT = Double.toString(fund_allocation.getDouble("alloc"));
							}
						}
						hasAccountDetailsIn.hasInvestmentFundDetailsIn = hasInvestmentFundDetailsInList;

						hasAccountDetailsInList.add(hasAccountDetailsIn);
						//RecordApplicationRequest > policy > hasDetailsRecordIn > hasAccountDetailsIn - E
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						investment_fund = investment_fund + 1;
					}

					obj.hasAccountDetailsIn = hasAccountDetailsInList;
				}
			}

			objList.add(obj);
			if (investment_fund > 0) {
				return objList;
			} else {
				return null;
			}

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return null;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > HasAssessmentHeadersIn
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAssessmentHeadersIn> prepareHasAssessmentHeadersIn (ApplicationBean appBean) throws Exception {
		List<HasAssessmentHeadersIn> objList = new ArrayList<>();
		HasAssessmentHeadersIn obj = new HasAssessmentHeadersIn();

		try {

			obj.hasAssessmentDetailsIn = prepareHasAssessmentDetailsIn(appBean);

			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > HasAssessmentHeadersIn >
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAssessmentDetailsIn> prepareHasAssessmentDetailsIn (ApplicationBean appBean) throws Exception {
		List<HasAssessmentDetailsIn> objList = new ArrayList<>();
		HasAssessmentDetailsIn obj = new HasAssessmentDetailsIn();

		try {

			//LINE NO.
			String[] tmp_remarks = appBean.Pol_remarks.getremarklist();
			for (int rm_count = 0; rm_count < tmp_remarks.length; rm_count++) {
				if (tmp_remarks[rm_count] == null || "".equals(tmp_remarks[rm_count])) {
					break;
				}
				obj.lineNO = Integer.toString(rm_count + 1);
				obj.assessmentDetails = tmp_remarks[rm_count];
				objList.add(obj);
				obj = new HasAssessmentDetailsIn();
			}

			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * RecordApplicationRequest > policy > hasAXAAsiaPolicyPaymentAccount
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAXAAsiaPolicyPaymentAccount> prepareHasAXAAsiaPolicyPaymentAccount (ApplicationBean appBean) throws Exception {
		List<HasAXAAsiaPolicyPaymentAccount> objList = new ArrayList<>();
		HasAXAAsiaPolicyPaymentAccount obj = new HasAXAAsiaPolicyPaymentAccount();
		try {

			//user input status of DA form
			obj.sectionCD = "UW";
			obj.statusCD = "";//TODO - USELESS

			//For Monthly payment mode only
			if (appBean.paymentModeCodeMapped.equals("M")) {
				obj.bankNO = "9999";
				obj.branchNO = "999";
				obj.factoringHouseCD = "O";
			} else {
				obj.bankNO = "";
				obj.branchNO = "";
				obj.factoringHouseCD = "";
			}

			obj.validFromDTTM = this.getCurrentDate();//TODO - USELESS
			obj.authProcessDT = this.getCurrentDate();//TODO - USELESS
			obj.modDT = this.getCurrentDate();//TODO - USELESS
			obj.paymentMethodChangeFLG = "0";//TODO - USELESS
			obj.hasAXAAsiaPartyAccount = prepareHasAXAAsiaPartyAccount(appBean);

			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * RecordApplicationRequest > policy > hasAXAAsiaPolicyPaymentAccount > hasAXAAsiaPartyAccount
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAXAAsiaPartyAccount> prepareHasAXAAsiaPartyAccount (ApplicationBean appBean) throws Exception {
		List<HasAXAAsiaPartyAccount> objList = new ArrayList<>();
		HasAXAAsiaPartyAccount obj = new HasAXAAsiaPartyAccount();
		try {


			//Effective date
			if (appBean.hasFundReqDate) {
				obj.cpfBankEffectiveDT = "00000000";
			}else { 
				obj.cpfBankEffectiveDT = appBean.policySubmitDT;
			}

			String bankCD = "";

			if (appBean.bankName.equals("dbs"))
				bankCD = "7171";
			else if (appBean.bankName.equals("uob"))
				bankCD = "7375";
			else if (appBean.bankName.equals("ocbc"))
				bankCD = "7339";

			obj.accountBankCD = bankCD;
			obj.cpfINVAccountNO = "";
			obj.mandateStatusCD = "0";//TODO-Unless
			obj.creditCardNO = "0";//TODO-Unless
			obj.cardExpirationDT = "0";//TODO-Unless
			//obj.cpfBankEffectiveDT = "0";//TODO-Unless
			obj.processDT = "";
			obj.bankID = appBean.LifeAssured_personalInfo.getString("idCardNo");

			//For Monthly payment mode only
			if (!appBean.paymentMode.equals("L")) {
				obj.accountNO = "999-9999999";
			} else {
				obj.accountNO = "";
			}

			obj.cpfBankAbbr = "";
			obj.status = "B";
			obj.accountTypeCD = "";

			if (appBean.paymentModeCodeMapped.equals("M")) {
				if (appBean.isThirdParty) {
					obj.payorLastNM = appBean.ow.fullName;
					obj.payorFirstNM = "";
				} else {
					obj.payorLastNM = appBean.la.fullName;
					obj.payorFirstNM = "";
				}
			} else {
				obj.payorLastNM = "";
				obj.payorFirstNM = "";
			}

			if (appBean.quotation_policyOptions.has("appBean.paymentmethod")) {
				if (appBean.isCPFPayment) {
					switch (appBean.payorPaymentMethodCode) {
					case "cpfissa":
						//CPF BANK ABBRVIATION
						obj.cpfBankAbbr = "";

						//CPF BANK CODE can skip currently
						//obj.accountBankCD = "00TT";//TODO -useless

						//CPF INV A/C NO
						obj.cpfINVAccountNO = appBean.ow.documentId; //Todo the original logic seems cannot get an ID for 1st Party

						//BANK ID NO - can skip currently
						//obj.bankID = "CITI987";

						//Debtor Name
						//obj.payorLastNM = appBean.Owner_personalInfo.getString("lastName");
						//obj.payorFirstNM = appBean.Owner_personalInfo.getString("firstName");

						//CPF A/C NO.
						obj.accountNO = truncateSpecialChar(appBean.CPFpaymentBlockNM.getString("idCardNo"));

						//STATUS
						obj.status = "B";

						//Process Date - rls, add this for rls population
						obj.processDT = "";

						obj.accountTypeCD = "CPF";
						break;
					case "cpfisoa":
						if (appBean.withCPFAccount) {
							//CPF BANK ABBRVIATION
							if (appBean.CPFpaymentBlockNM.has(appBean.bankNamePickerFieldName)) {
								obj.cpfBankAbbr = (appBean.CPFpaymentBlockNM.getString(appBean.bankNamePickerFieldName)).toUpperCase();
							} else {
								throw new IOException("BANK ABBRVIATION missing");
							}

							//Debtor Name
							//obj.payorLastNM = appBean.Owner_personalInfo.getString("lastName");
							//obj.payorFirstNM = appBean.Owner_personalInfo.getString("firstName");

							//CPF BANK CODE can skip currently
							//obj.accountBankCD = "00A";//TODO - useless
							//CPF INV A/C NO
							if (appBean.CPFpaymentBlockNM.has(appBean.investmentAcctNoFieldName)) {
								obj.cpfINVAccountNO = appBean.CPFpaymentBlockNM.getString(appBean.investmentAcctNoFieldName);
							}
							//BANK ID NO - can skip currently
							//obj.bankID = "CITI987";

							//CPF A/C NO.
							obj.accountNO = truncateSpecialChar(appBean.CPFpaymentBlockNM.getString("idCardNo"));

							//STATUS
							obj.status = "B";

							//Process Date - rls, add this for rls population
							obj.processDT = "";

							obj.accountTypeCD = "CPF";
						}

						break;
					case "srs":
						if (appBean.withCPFAccount) {
							//CPF BANK ABBRVIATION
							if (appBean.CPFpaymentBlockNM.has(appBean.bankNamePickerFieldName)) {
								obj.cpfBankAbbr = (appBean.CPFpaymentBlockNM.getString(appBean.bankNamePickerFieldName)).toUpperCase();
							} else {
								throw new IOException("BANK ABBRVIATION missing");
							}

							//Debtor Name
							//obj.payorLastNM = appBean.Owner_personalInfo.getString("lastName");
							//obj.payorFirstNM = appBean.Owner_personalInfo.getString("firstName");

							//CPF BANK CODE can skip currently
							//obj.accountBankCD = "00A";//TODO - useless

							//CPF INV A/C NO
							if (appBean.CPFpaymentBlockNM.has(appBean.investmentAcctNoFieldName)) {
								obj.cpfINVAccountNO = appBean.CPFpaymentBlockNM.getString(appBean.investmentAcctNoFieldName);
							}

							//BANK ID NO - can skip currently
							//obj.bankID = "CITI987";

							//CPF A/C NO.
							obj.accountNO = truncateSpecialChar(appBean.CPFpaymentBlockNM.getString("idCardNo"));

							//STATUS
							obj.status = "B";

							//Process Date - rls, add this for rls population
							obj.processDT = "";

							obj.accountTypeCD = "CPF";
						}

						break;
					default:
						break;
					}
				}
			}
			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * RecordApplicationRequest > policy > hasBenificialOwnerDetailsIn
	 * If it is first party case, system will create only 1 record which is the LA
	 * If it is third party case, one more record created as Owner
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasBenificialOwnerDetailsIn> prepareHasBenificialOwnerDetailsIn (ApplicationBean appBean) throws
	Exception {
		List<HasBenificialOwnerDetailsIn> hasBenificialOwnerDetailsInList = new ArrayList<>();
		HasBenificialOwnerDetailsIn hasBenificialOwnerDetailsIn = new HasBenificialOwnerDetailsIn();
		try {

			//

			String la_tmp_block = "";
			String la_tmp_unit = "";
			String la_addressline1 = "";
			String la_addressline2 = "";
			String la_addressline3 = "";
			String la_addressline4 = "";

			if (appBean.LifeAssured_personalInfo.has("addrBlock")) {
				la_tmp_block = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("addrBlock"));
			}

			if (appBean.LifeAssured_personalInfo.has("unitNum")) {
				la_tmp_unit = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("unitNum"));
			}

			if ("".equals(la_tmp_block) && "".equals(la_tmp_unit)) {
				la_addressline1 = "";
			} else if ("".equals(la_tmp_block)) {
				la_addressline1 = la_tmp_unit;
			} else if ("".equals(la_tmp_unit)) {
				la_addressline1 = la_tmp_block;
			} else {
				la_addressline1 = la_tmp_block + " " + la_tmp_unit;
			}

			//Address 2
			la_addressline2 = "";

			if (appBean.LifeAssured_personalInfo.has("addrStreet")) {
				la_addressline2 = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("addrStreet"));
			}

			//Address 3
			String la_tmp_building = "";
			String la_tmp_city = "";

			if (appBean.LifeAssured_personalInfo.has("addrEstate")) {
				la_tmp_building = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("addrEstate"));
			}

			if (appBean.LifeAssured_personalInfo.has("addrCity")) {
				la_tmp_city = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("addrCity"));
			}

			if ("".equals(la_tmp_building) && "".equals(la_tmp_city)) {
				la_addressline3 = "";
			} else if ("".equals(la_tmp_building)) {
				la_addressline3 = CityConversion(la_tmp_city);
			} else if ("".equals(la_tmp_city)) {
				la_addressline3 = la_tmp_building;
			} else {
				la_addressline3 = la_tmp_building + " " + CityConversion(la_tmp_city);
			}

			//Address 4
			String la_tmp_country = "";
			la_addressline4 = "";
			if (appBean.LifeAssured_personalInfo.has("addrCountry")) {
				la_tmp_country = CountryConversion(truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("addrCountry")));
				if (la_tmp_country.toLowerCase().contains("China".toLowerCase())) {
					la_addressline4 = "China" + " " + truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("postalCode"));
				} else {
					la_addressline4 = la_tmp_country + " " + truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("postalCode"));
				}
			}


			//Prepare Life Assured
			hasBenificialOwnerDetailsIn.relationshipToInsuredCD = "LA";
			hasBenificialOwnerDetailsIn.ownershipFLG = "C";
			hasBenificialOwnerDetailsIn.canBeIndividual = prepareCanBenIndividualForBenificial(appBean, 1);
			hasBenificialOwnerDetailsInList.add(hasBenificialOwnerDetailsIn);

			//Prepare Owner for 3rd Party case
			if(appBean.isThirdParty) {
				hasBenificialOwnerDetailsIn = new HasBenificialOwnerDetailsIn();
				hasBenificialOwnerDetailsIn.relationshipToInsuredCD = "PO";
				hasBenificialOwnerDetailsIn.ownershipFLG = "C";
				hasBenificialOwnerDetailsIn.canBeIndividual = prepareCanBenIndividualForBenificial(appBean, 2);
				hasBenificialOwnerDetailsInList.add(hasBenificialOwnerDetailsIn);
			}


			return hasBenificialOwnerDetailsInList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasBenificialOwnerDetailsInList;
		}
	}

	/**
	 * Prepare RecordApplicationRequest > policy > hasBenificialOwnerDetailsIn > canBeIndividual
	 * @param appBean
	 * @param laOrOw  1 for Life Assured, 2 for Owner
	 * @return
	 * @throws Exception
	 */
	private List<CanBeIndividualReference> prepareCanBenIndividualForBenificial
	(ApplicationBean appBean, int laOrOw)  throws Exception {
		List<CanBeIndividualReference> objList = new ArrayList<>();
		CanBeIndividualReference obj = new CanBeIndividualReference();

		List<HasAddressInfo> hasAddressesInList = new ArrayList<>();
		HasAddressInfo hasAddressesIn = new HasAddressInfo();

		try {

			//To set Life Assured record
			if (laOrOw == 1) {
				//obj.ownershipFLG = "C";
				obj.firstNM = "";
				obj.lastNM = spaceRemover(truncateSpecialChar(appBean.la.fullName)); // past full name as mapping requirement
				obj.chineseNM = spaceRemover(truncateSpecialChar(appBean.la.chineseName));
				obj.birthDT = appBean.la.birthdate;
				obj.genderCD = appBean.la.gender;
				obj.passportNo = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("idCardNo"));//NRIC/Passport No
				obj.countryOfResidenceCD = appBean.la.nationalityMapped;
				obj.nationality = NationalityConversion(appBean.LifeAssured_personalInfo.getString("nationality"), true);




				//obj ->  hasAddressesIn
				hasAddressesIn.addressLine1 = appBean.la.firstName;//Given Name
				hasAddressesIn.addressLine2 = appBean.la.lastName;//Surname
				hasAddressesIn.addressLine3 = appBean.la.otherName;//English Name
				hasAddressesIn.addressLine4 = appBean.la.chineseName;//Han Yu Pin Yin Name

				hasAddressesInList.add(hasAddressesIn);
				obj.hasAddressesIn = hasAddressesInList;

			} else if (laOrOw == 2) {

				//                    obj.relationshipToInsuredCD = "PO";
				//                    obj.ownershipFLG = "C";
				//Initial
				//obj.ownershipFLG = "C";
				obj.firstNM = "";
				obj.lastNM = spaceRemover(truncateSpecialChar(appBean.ow.fullName));
				obj.chineseNM = spaceRemover(truncateSpecialChar(appBean.ow.chineseName));
				obj.birthDT = appBean.ow.birthdate;
				obj.genderCD = appBean.ow.gender;
				obj.passportNo = truncateSpecialChar(appBean.Owner_personalInfo.getString("idCardNo"));
				obj.countryOfResidenceCD = appBean.ow.nationality;

				obj.countryOfResidenceCD = NationalityConversion(appBean.ow.nationality, true);
				obj.nationality = NationalityConversion(appBean.ow.nationality, true);
				//obj ->  hasAddressesIn

				hasAddressesIn.addressLine1 = appBean.ow.firstName;//Given Name
				hasAddressesIn.addressLine2 = appBean.ow.lastName;//Surname
				hasAddressesIn.addressLine3 = appBean.ow.otherName;//English Name
				hasAddressesIn.addressLine4 = appBean.ow.chineseName;//Han Yu Pin Yin Name

				hasAddressesInList.add(hasAddressesIn);
				obj.hasAddressesIn = hasAddressesInList;

			}

			objList.add(obj);

			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith > hasStaffInformationIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasStaffInformationIn> prepareHasStaffInformation (ApplicationBean appBean) throws Exception {
		List<HasStaffInformationIn> objList = new ArrayList<>();
		HasStaffInformationIn obj = new HasStaffInformationIn();

		try {

			//Bank Staff ID
			obj.channelID = "1";
			obj.staffCD = "";
			obj.staffIND = "S";

			if (appBean.isSINGPOST) {                    
				obj.channelID = "1";
				obj.staffIND = "S";				
				obj.staffCD = appBean.Owner_personalInfo_branchInfo.getString("bankRefId");
			} else {
				obj.channelID = "";
				obj.staffIND = "";				
				obj.staffCD = "";
			}

			obj.hasBankReferrerInfoIn = prepareHasBankReferrerInfoIn(appBean);

			objList.add(obj);
			return objList;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > hasProducerInformationIn > hasAssociationWith > hasStaffInformationIn > hasBankReferrerInfoIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasBankReferrerInfoIn> prepareHasBankReferrerInfoIn (ApplicationBean appBean) throws Exception {
		List<HasBankReferrerInfoIn> objList = new ArrayList<>();
		HasBankReferrerInfoIn obj = new HasBankReferrerInfoIn();

		try {

			//leadID
			//if (1 == 1) {

			if (appBean.isSINGPOST) {
				obj.leadID = "1";
			} else {
				obj.leadID = "";
			}
			//}
			//else {
			//	throw new IOException("Mandatory field missing - lead ID");
			//}

			//Lead category
			//if (1 == 1) {
			obj.leadCategoryCD = "";
			//}
			//else {
			//	throw new IOException("Mandatory field missing - lead category");
			//}


			objList.add(obj);
			return objList;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Array Object of Life Insured Personal details
	 * RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasPersonalDetailsIn> prepareHasPersonalDetailsIn (ApplicationBean appBean) throws Exception {
		List<HasPersonalDetailsIn> hasPersonalDetailsInList = new ArrayList<>();
		HasPersonalDetailsIn hasPersonalDetailsIn = new HasPersonalDetailsIn();

		try {

			//INSURED NAME
			//sample:	HasPersonalDetailsIn.lastNM = quotation.getString("iLastName");

			hasPersonalDetailsIn.lastNM = spaceRemover(truncateSpecialChar(appBean.la.fullName));
			hasPersonalDetailsIn.firstNM = "";

			//SEX
			//sample: obj.genderCD = "M";
			hasPersonalDetailsIn.genderCD = appBean.la.gender;


			//BIRTH DATE                
			hasPersonalDetailsIn.birthDT = appBean.la.birthdate;


			//MARITAL STATUS
			//sample: obj.maritalStatusCD = "M";
			hasPersonalDetailsIn.maritalStatusCD = "";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_personalInfo.has("marital")) {
					hasPersonalDetailsIn.maritalStatusCD = appBean.LifeAssured_personalInfo.getString("marital");
				}
			}

			//HEIGHT IN METRE FOR INS/SPO

			//can not move if 0 : if input, then must not be 0;
			//obj.height = "0";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_insurability.has("HW01") && (appBean.LifeAssured_insurability.getDouble("HW01") > 0)) {
					hasPersonalDetailsIn.height = Double.toString(appBean.LifeAssured_insurability.getDouble("HW01"));
				}
			}
			//for bandAid, height default to be 1.5
			//mail at 10:20; 19 Oct
			if (appBean.isBandAid) {
				hasPersonalDetailsIn.height = "1.5";
			}

			//WEIGHT IN KG FOR INS/SPO/DE
			//sample:obj.weight = "67.1";                

			if (appBean.LifeAssured_insurability.has("HW02") && (appBean.LifeAssured_insurability.getDouble("HW02") > 0)) {
				appBean.la.weight = appBean.LifeAssured_insurability.getDouble("HW02");
			}

			//can not move if 0 : if input, then must not be 0;
			//obj.weight = "0.0";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.la.weight > 0) {
					hasPersonalDetailsIn.weight = Double.toString(appBean.la.weight);
				}
			}
			if (appBean.isBandAid) {
				hasPersonalDetailsIn.weight = "40";
			}

			//OCC. CLASS FOR INS/SPO
			hasPersonalDetailsIn.stdOccupationCD = "";
			hasPersonalDetailsIn.stdOccupationCD = getOccupationClass(appBean.la.occupation, appBean.la.issueAge, appBean.la.gender);

			//MONTHLY INCOME FOR INS/SPO
			hasPersonalDetailsIn.annualIncomeAMT = "0";

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.clientDoc.has("allowance")) {
					hasPersonalDetailsIn.annualIncomeAMT = appBean.clientDoc.get("allowance").toString();
				}
				if ("4".equals(hasPersonalDetailsIn.stdOccupationCD) || "6".equals(hasPersonalDetailsIn.stdOccupationCD)) {
					hasPersonalDetailsIn.annualIncomeAMT = "0";
				}
			}

			//                //CITIZEN TYPE
			//                //set it as "FR"
			//                hasPersonalDetailsIn.countryOfResidenceCD = "FR";
			//                //Below 1st party case, set "LR"
			//                if (appBean.isFirstParty || appBean.isThirdParty) {
			//                    if (appBean.LifeAssured_residency.has("EAPP-P6-F1")) {
			//                        if ("resided".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F1"))
			//                                || "outside".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F1"))
			//                                || "outside5".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F1"))) {
			//                            hasPersonalDetailsIn.countryOfResidenceCD = "LR";
			//                        } else {
			//                        	hasPersonalDetailsIn.countryOfResidenceCD = "FR";
			//                        }
			//                    }
			//                    if (appBean.LifeAssured_residency.has("EAPP-P6-F2")) {
			//                        if ("Y".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F2"))) {
			//                            hasPersonalDetailsIn.countryOfResidenceCD = "LR";
			//                        } else {
			//                        	hasPersonalDetailsIn.countryOfResidenceCD = "FR";
			//                        }
			//                    }
			//                }

			//only when the question answer are same for Singaporean Citizen,Singapore PR / Employment Pass / Work Permit,Dependent / Student / Long Term Visit Pass
			//it can be SIT
			String LA_P6_Q1 = "";
			String LA_P6_Q2 = "";
			String LA_P6_Q3 = "";
			String LA_P6_Q4 = "";
			String OW_P6_Q1 = "";
			String OW_P6_Q2 = "";
			String OW_P6_Q3 = "";
			String OW_P6_Q4 = "";

			//if (appBean.isThirdParty) {
			//Insured
			if (appBean.LifeAssured_residency.has("EAPP-P6-F1")) {
				LA_P6_Q1 = appBean.LifeAssured_residency.getString("EAPP-P6-F1");
			}
			if (appBean.LifeAssured_residency.has("EAPP-P6-F2")) {
				LA_P6_Q2 = appBean.LifeAssured_residency.getString("EAPP-P6-F2");
			}
			if (appBean.LifeAssured_residency.has("EAPP-P6-F3")) {
				LA_P6_Q3 = appBean.LifeAssured_residency.getString("EAPP-P6-F3");
			}
			if (appBean.LifeAssured_residency.has("EAPP-P6-F4")) {
				LA_P6_Q4 = appBean.LifeAssured_residency.getString("EAPP-P6-F4");
			}

			//Proposer
			if (appBean.Owner_residency.has("EAPP-P6-F1")) {
				OW_P6_Q1 = appBean.Owner_residency.getString("EAPP-P6-F1");
			}
			if (appBean.Owner_residency.has("EAPP-P6-F2")) {
				OW_P6_Q2 = appBean.Owner_residency.getString("EAPP-P6-F2");
			}
			if (appBean.Owner_residency.has("EAPP-P6-F3")) {
				OW_P6_Q3 = appBean.Owner_residency.getString("EAPP-P6-F3");
			}
			if (appBean.Owner_residency.has("EAPP-P6-F4")) {
				OW_P6_Q4 = appBean.Owner_residency.getString("EAPP-P6-F4");
			}
			// }
			//                if (LA_P6_Q1.equals(OW_P6_Q1) && LA_P6_Q2.equals(OW_P6_Q2) && LA_P6_Q3.equals(OW_P6_Q3) && LA_P6_Q4.equals(OW_P6_Q4)) {
			//                    hasPersonalDetailsIn.countryOfResidenceCD = "LR";
			//                    appBean.isSameAnswer = true;
			//                } else {
			//                    hasPersonalDetailsIn.countryOfResidenceCD = "FR";
			//                }

			//CITIZEN TYPE
			//sample: obj.documentID = "S78654321";
			hasPersonalDetailsIn.documentID = "S00000000";

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_personalInfo.has("idCardNo")) {
					hasPersonalDetailsIn.documentID = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("idCardNo"));
				}
			}


			hasPersonalDetailsIn.hasAddressesIn = prepareHasAddressesInForInsured(appBean);
			hasPersonalDetailsIn.hasIndividualUWInfoIn = prepareHasIndividualUWInfoInForInsured(appBean, hasPersonalDetailsIn);

			hasPersonalDetailsInList.add(hasPersonalDetailsIn);
			return hasPersonalDetailsInList;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasPersonalDetailsInList;
		}
	}

	/**
	 * Prepare Object of
	 * RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasAddressIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAddressInfo> prepareHasAddressInForOwner (ApplicationBean appBean) throws Exception {
		List<HasAddressInfo> objList = new ArrayList<>();
		HasAddressInfo obj = new HasAddressInfo();
		try {
			//CORR ADDRESS
			//Get mail address for this owner's address if proposer's mail addresss is different with resident address, if same with the resident address, get the resident address.

			if (appBean.isFirstParty || appBean.isThirdParty) {
				//Address 1
				if (appBean.ismAddrdifferent) {
					String ow_mail_block = "";
					String ow_mail_unit = "";

					if (appBean.Owner_personalInfo.has("mAddrBlock")) {
						ow_mail_block = appBean.Owner_personalInfo.getString("mAddrBlock");
					}

					if (appBean.Owner_personalInfo.has("mAddrnitNum")) {
						ow_mail_unit = appBean.Owner_personalInfo.getString("mAddrnitNum");
					}

					if ("".equals(ow_mail_block) && "".equals(ow_mail_unit)) {
						obj.addressLine1 = "";
					} else if ("".equals(ow_mail_block)) {
						obj.addressLine1 = truncateSpecialChar(ow_mail_unit);
					} else if ("".equals(ow_mail_unit)) {
						obj.addressLine1 = truncateSpecialChar(ow_mail_block);
					} else {
						obj.addressLine1 = truncateSpecialChar(ow_mail_block) + " " + truncateSpecialChar(ow_mail_unit);
					}

					//Address 2
					obj.addressLine2 = "";

					if (appBean.Owner_personalInfo.has("mAddrStreet")) {
						obj.addressLine2 = truncateSpecialChar(appBean.Owner_personalInfo.getString("mAddrStreet"));
					}

					//Address 3
					String ow_mail_building = "";
					String ow_mail_city = "";

					if (appBean.Owner_personalInfo.has("mAddrEstate")) {
						ow_mail_building = truncateSpecialChar(appBean.Owner_personalInfo.getString("mAddrEstate"));
					}

					if (appBean.Owner_personalInfo.has("mAddrCity")) {
						ow_mail_city = truncateSpecialChar(appBean.Owner_personalInfo.getString("mAddrCity"));
					}

					if ("".equals(ow_mail_building) && "".equals(ow_mail_city)) {
						obj.addressLine3 = "";
					} else if ("".equals(ow_mail_building)) {
						obj.addressLine3 = CityConversion(ow_mail_city);
					} else if ("".equals(ow_mail_city)) {
						obj.addressLine3 = ow_mail_building;
					} else {
						obj.addressLine3 = ow_mail_building + " " + CityConversion(ow_mail_city);
					}

					//Address 4
					String ow_mail_country = "";
					obj.addressLine4 = "";

					if (appBean.Owner_personalInfo.has("mAddrCountry")) {
						ow_mail_country = CountryConversion(truncateSpecialChar(appBean.Owner_personalInfo.getString("mAddrCountry")));

						if (ow_mail_country.toLowerCase().contains("China".toLowerCase())) {
							obj.addressLine4 = "China" + " " + truncateSpecialChar(appBean.Owner_personalInfo.getString("mPostalCode"));
						} else {
							obj.addressLine4 = ow_mail_country + " " + truncateSpecialChar(appBean.Owner_personalInfo.getString("mPostalCode"));
						}
					}

					//Address 5
					obj.cityNM = "";

					if (appBean.Owner_personalInfo.has("addrCountry")) {
						obj.cityNM = getCountryCD(truncateSpecialChar(ow_mail_country));
					}
				} else {
					String ow_tmp_block = "";
					String ow_tmp_unit = "";

					if (appBean.Owner_personalInfo.has("addrBlock")) {
						ow_tmp_block = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrBlock"));
					}

					if (appBean.Owner_personalInfo.has("unitNum")) {
						ow_tmp_unit = truncateSpecialChar(appBean.Owner_personalInfo.getString("unitNum"));
					}

					if ("".equals(ow_tmp_block) && "".equals(ow_tmp_unit)) {
						obj.addressLine1 = "";
					} else if ("".equals(ow_tmp_block)) {
						obj.addressLine1 = ow_tmp_unit;
					} else if ("".equals(ow_tmp_unit)) {
						obj.addressLine1 = ow_tmp_block;
					} else {
						obj.addressLine1 = ow_tmp_block + " " + ow_tmp_unit;
					}

					//Address 2
					obj.addressLine2 = "";

					if (appBean.Owner_personalInfo.has("addrStreet")) {
						obj.addressLine2 = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrStreet"));
					}

					//Address 3
					String ow_tmp_building = "";
					String ow_tmp_city = "";

					if (appBean.Owner_personalInfo.has("addrEstate")) {
						ow_tmp_building = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrEstate"));
					}

					if (appBean.Owner_personalInfo.has("addrCity")) {
						ow_tmp_city = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrCity"));
					}

					if ("".equals(ow_tmp_building) && "".equals(ow_tmp_city)) {
						obj.addressLine3 = "";
					} else if ("".equals(ow_tmp_building)) {
						obj.addressLine3 = CityConversion(ow_tmp_city);
					} else if ("".equals(ow_tmp_city)) {
						obj.addressLine3 = ow_tmp_building;
					} else {
						obj.addressLine3 = ow_tmp_building + " " + CityConversion(ow_tmp_city);
					}

					//Address 4
					String ow_tmp_country = "";
					obj.addressLine4 = "";
					if (appBean.Owner_personalInfo.has("addrCountry")) {
						ow_tmp_country = CountryConversion(appBean.Owner_personalInfo.getString("addrCountry"));
						if (ow_tmp_country.toLowerCase().contains("China".toLowerCase())) {
							obj.addressLine4 = "China" + " " + truncateSpecialChar(appBean.Owner_personalInfo.getString("postalCode"));
						} else {
							obj.addressLine4 = truncateSpecialChar(ow_tmp_country) + " " + truncateSpecialChar(appBean.Owner_personalInfo.getString("postalCode"));
						}
					}

					//Address 5
					obj.cityNM = "";

					if (appBean.Owner_personalInfo.has("addrCountry")) {
						obj.cityNM = getCountryCD(truncateSpecialChar(ow_tmp_country));
					}
				}
			}

			//MOBILE'NUMBER OF OWNER
			//sample obj.mobilePhoneNO = "9876512345";
			if (appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("mobileNo")) {
					try {
						obj.mobilePhoneNO = Long.toString(appBean.Owner_personalInfo.getLong("mobileNo"));
					} catch (Exception ex001) {
						obj.mobilePhoneNO = appBean.Owner_personalInfo.getString("mobileNo");
					}
				}
			}

			//EMAIL ADDRESS OF OWNER
			if (appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("email")) {
					obj.emailAddress = appBean.Owner_personalInfo.getString("email");
				}
			}

			//TEL-RES
			//sample:obj.telephoneNO = "9876543210";
			//Remark: need to enhanced to fix the issue while toString(002378474) becomes 2378474.
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_personalInfo.has("mobileNo")) {
					try {
						obj.telephoneNO = Long.toString(appBean.LifeAssured_personalInfo.getLong("mobileNo"));
					} catch (Exception ex003) {
						obj.telephoneNO = appBean.LifeAssured_personalInfo.getString("mobileNo");
					}
				}
			}

			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Owner Object's UW information
	 * RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasIndividualUWInfoIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasIndividualUWInfoIn> prepareHasIndividualUWInfoIn (ApplicationBean appBean) throws Exception {
		List<HasIndividualUWInfoIn> objList = new ArrayList<>();
		HasIndividualUWInfoIn hasIndividualUWInfoIn = new HasIndividualUWInfoIn();
		try {


			//Owner level Indicator:
			hasIndividualUWInfoIn.declineQuestionIND = "";
			hasIndividualUWInfoIn.avocationQuestionIND = "";
			hasIndividualUWInfoIn.healthQuestionIND = "";
			hasIndividualUWInfoIn.previousHealthExamQuestionIND = "";
			hasIndividualUWInfoIn.previousHealthExamResultIND = "";
			hasIndividualUWInfoIn.alcoholQuestionIND = "";
			hasIndividualUWInfoIn.alcoholUnitsPerWeekNO = "0";
			hasIndividualUWInfoIn.tobaccoQuestionIND = "";
			hasIndividualUWInfoIn.tobaccoUnitsPerDayNO = "0";
			hasIndividualUWInfoIn.smokingQuestionIND = "";
			hasIndividualUWInfoIn.previousHealthExamQuestionIND = "";
			hasIndividualUWInfoIn.previousHealthExamReasonIND = "";
			hasIndividualUWInfoIn.previousHealthExamResultIND = "";
			hasIndividualUWInfoIn.ePolicyDeliveryFLG = "P";

			//TRUST DECLARATION
			hasIndividualUWInfoIn.trustDeclarationIND = "N";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.la.issueAge < 16) {
					hasIndividualUWInfoIn.trustDeclarationIND = "Y";
				}
			}

			if (appBean.isThirdParty && appBean.hasPPEPS) {
				hasIndividualUWInfoIn.declineQuestionIND = "N";

				if ((!appBean.isOWPMED) || (!appBean.isPolicyPMED)) {
					if (appBean.Owner_insurability.has("INS01")) { //as payor, user owner
						hasIndividualUWInfoIn.declineQuestionIND = appBean.Owner_insurability.getString("INS01");

						if ("Y".equals(appBean.Owner_insurability.getString("INS01"))) {
							//				appBean.isOWPMED = true;
							appBean.isPolicyPMED = true;
						}
					}
				}

				//                    if ((!appBean.isOWPMED) || (!appBean.isPolicyPMED)) {
				//                        if (appBean.Owner_insurability.has("INS02")) {
				//                            if ("Y".equals(appBean.Owner_insurability.getString("INS02"))) {
				//                                appBean.isOWPMED = true;
				//                                appBean.isPolicyPMED = true;
				//                            }
				//                        }
				//                    }

				if ((!appBean.isOWPMED)) {
					if (appBean.Owner_insurability.has("LIFESTYLE06")) {
						if ("Y".equals(appBean.Owner_insurability.getString("LIFESTYLE06"))) {
							appBean.isOWPMED = true;
						}
					}
				}

				String OW_medicalQN = null;

				for (int QN_COUNT = 0; QN_COUNT < appBean.medicalQNString.length; QN_COUNT++) {
					OW_medicalQN = appBean.medicalQNString[QN_COUNT];

					if (appBean.Owner_insurability.has(OW_medicalQN))
						if ("Y".equals(appBean.Owner_insurability.getString(OW_medicalQN))) {
							appBean.isOWPMED = true;
							appBean.isPolicyPMED = true;
						}
					if (appBean.isOWPMED && appBean.isPolicyPMED) {
						break;
					}
				}

				//Details of Regular Doctor
				if (appBean.Owner_insurability.has("REG_DR01") && "Y".equals(appBean.Owner_insurability.getString("REG_DR01"))) {
					if (appBean.Owner_insurability.has("REG_DR01_DATA")) {
						JSONArray REG_DR01_DATA_array = appBean.Owner_insurability.getJSONArray("REG_DR01_DATA");

						for(int i = 0; i < REG_DR01_DATA_array.length(); i++) {
							JSONObject REG_DR01_DATA = REG_DR01_DATA_array.getJSONObject(i);

							if (REG_DR01_DATA.has("REG_DR01d")) {
								if (REG_DR01_DATA.getString("REG_DR01d").toUpperCase().equals("OTHER")) {
									appBean.isOWPMED = true;
									appBean.isPolicyPMED = true;
									break;
								}
							}
						}
					}	
				}




				//AVOCATION QUESTION FOR PAYOR
				hasIndividualUWInfoIn.avocationQuestionIND = "N";


				if (appBean.Owner_insurability.has("LIFESTYLE05")) {
					if ("Y".equals(appBean.Owner_insurability.getString("LIFESTYLE05"))) {
						hasIndividualUWInfoIn.avocationQuestionIND = "Y";
					} else {
						hasIndividualUWInfoIn.avocationQuestionIND = "N";
					}
				}


				Double OW_weight_change = 0.00;
				Double OW_weight_GainLost = 0.00;
				Double OW_weightchangePC = 0.0;

				if ((!appBean.isOWPMED) || (!appBean.isPolicyPMED)) {
					if (appBean.Owner_insurability.has("HW03") && ("Y".equals(appBean.Owner_insurability.getString("HW03")))) {
						if (appBean.Owner_insurability.has("HW03b")) {
							OW_weight_change = appBean.Owner_insurability.getDouble("HW03b");
						}

						if (OW_weight_change > 0) {
							if ("L".equals(appBean.Owner_insurability.getString("HW03a"))) {
								OW_weight_GainLost = OW_weight_change * (-1);
							} else {
								OW_weight_GainLost = OW_weight_change;
							}
						}

						OW_weightchangePC = OW_weight_change / (appBean.ow.weight + OW_weight_GainLost);

						if (OW_weightchangePC > 0.05) {
							appBean.isOWPMED = true;
							appBean.isPolicyPMED = true;
						} else if (appBean.Owner_insurability.has("HW03c") && ("Other".equals(appBean.Owner_insurability.getString("HW03c")))) {
							appBean.isOWPMED = true;
							appBean.isPolicyPMED = true;
						}

						//                            if (appBean.Owner_insurability.has("HW03c") && ("Other".equals(appBean.Owner_insurability.getString("HW03c")))) {
						//                                appBean.isOWPMED = true;
						//                                appBean.isPolicyPMED = true;
						//                            }
					}
				}

				//PREVIOUS EXAM FOR PAYOR
				hasIndividualUWInfoIn.previousHealthExamQuestionIND = "N";

				//PREVIOUS EXAM REASON FOR PA
				hasIndividualUWInfoIn.previousHealthExamReasonIND = "";

				//PREVIOUS EXAM RESULT FOR PA
				//Confirmed by Frankie, delete field
				hasIndividualUWInfoIn.previousHealthExamResultIND = "";

				//SMOKING QUESTION FOR PAYOR
				hasIndividualUWInfoIn.smokingQuestionIND = "N";

				//                    if (appBean.Owner_insurability.has("LIFESTYLE01")) {
				//                        hasIndividualUWInfoIn.smokingQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE01");
				//                        appBean.isPolicyPMED = true;
				//                    }

				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE01")) {
						hasIndividualUWInfoIn.smokingQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE01");

						if (appBean.Owner_insurability.getString("LIFESTYLE01").equals("Y"))
							appBean.isPolicyPMED = true;
					}
				}

				//NO. OF CIGARETTE PER DAY FO
				//sample: obj.cigarettesPerDayNO = "12";
				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE01b")) {
						hasIndividualUWInfoIn.cigarettesPerDayNO = Integer.toString(appBean.Owner_insurability.getInt("LIFESTYLE01b"));
					}
				}

				//ALCOHOL QUESTION FOR PAYOR
				hasIndividualUWInfoIn.alcoholQuestionIND = "N";
				boolean OW_is_drinker = false;

				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE02")) {
						hasIndividualUWInfoIn.alcoholQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE02");

						if (appBean.Owner_insurability.getString("LIFESTYLE02").equals("Y")) {
							OW_is_drinker = true;
						}
					}
				}

				//ALCOHOL UNITS/WEEK FOR PAYO
				//sample: obj.alcoholUnitsPerWeekNO = "27";
				Integer OW_alcohol_unit_weekly = 0;


				if (OW_is_drinker) {
					if (appBean.Owner_insurability.has("LIFESTYLE02a_1")) {
						if ("Y".equals(appBean.Owner_insurability.getString("LIFESTYLE02a_1"))) {
							OW_alcohol_unit_weekly = OW_alcohol_unit_weekly + appBean.Owner_insurability.getInt("LIFESTYLE02a_2");
						}		
					}

					if (appBean.Owner_insurability.has("LIFESTYLE02b_1")) {
						if ("Y".equals(appBean.Owner_insurability.getString("LIFESTYLE02b_1"))) {
							OW_alcohol_unit_weekly = OW_alcohol_unit_weekly + appBean.Owner_insurability.getInt("LIFESTYLE02b_2");
						}
					}

					if (appBean.Owner_insurability.has("LIFESTYLE02b_1")) {
						if ("Y".equals(appBean.Owner_insurability.getString("LIFESTYLE02c_1"))) {
							OW_alcohol_unit_weekly = OW_alcohol_unit_weekly + appBean.Owner_insurability.getInt("LIFESTYLE02c_2");
						}
					}					
				}	

				hasIndividualUWInfoIn.alcoholUnitsPerWeekNO = Integer.toString(OW_alcohol_unit_weekly);

				//TOBACCO QUESTION FOR PAYOR & SMOKING QUESTION
				//sample: obj.tobaccoQuestionIND = "N";
				// hasIndividualUWInfoIn.tobaccoQuestionIND = "N";
				hasIndividualUWInfoIn.smokingQuestionIND = "N";

				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE01")) {
						hasIndividualUWInfoIn.tobaccoQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE01");

						if (appBean.Owner_insurability.getString("LIFESTYLE01").equals("Y"))
							appBean.isPolicyPMED = true;
					}
				}

				//                    if (appBean.Owner_insurability.has("LIFESTYLE01")) {
				//                        hasIndividualUWInfoIn.tobaccoQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE01");
				//                        hasIndividualUWInfoIn.smokingQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE01");
				//                        appBean.isPolicyPMED = true;
				//                    }

				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE01")) {
						hasIndividualUWInfoIn.tobaccoQuestionIND = appBean.Owner_insurability.getString("LIFESTYLE01");

						if (appBean.Owner_insurability.getString("LIFESTYLE01").equals("Y"))
							appBean.isPolicyPMED = true;
					}
				}

				//TOBACCO UNITS/DAY FOR PAYOR
				//mandatory is controlled by UI
				hasIndividualUWInfoIn.tobaccoUnitsPerDayNO = "0";

				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE01b")) {
						hasIndividualUWInfoIn.tobaccoUnitsPerDayNO = Integer.toString(appBean.Owner_insurability.getInt("LIFESTYLE01b"));
					}
				}

				//

				hasIndividualUWInfoIn.healthQuestionIND = "N";


				//NO. OF CIGARETTE PER DAY
				//sample: obj.cigarettesPerDayNO = "12";
				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE01b")) {
						hasIndividualUWInfoIn.cigarettesPerDayNO = Integer.toString(appBean.Owner_insurability.getInt("LIFESTYLE01b"));
					}
				}

				//AVOCATION QUESTION
				hasIndividualUWInfoIn.avocationQuestionIND = "N";

				if (appBean.isFirstParty || appBean.isThirdParty) {
					if (appBean.Owner_insurability.has("LIFESTYLE05")) {
						if ("Y".equals(appBean.Owner_insurability.getString("LIFESTYLE05"))) {
							hasIndividualUWInfoIn.avocationQuestionIND = "Y";
						} else {
							hasIndividualUWInfoIn.avocationQuestionIND = "N";
						}
					}
				}

				if (appBean.isOWPMED) {
					hasIndividualUWInfoIn.healthQuestionIND = "Y";
				}
			}

			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasIndividualUWInfoIn - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			objList.add(hasIndividualUWInfoIn);

			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	private String getPaymentModeCD(String paymode) {
		if (paymode.equals("L"))
			return "A";
		else
			return paymode;
	}

	private String getBankCD(String BankName) {
		JSONObject bankCDlist = null;
		bankCDlist = Constant.CBUTIL.getDoc("branches");

		if (bankCDlist == null) {
			Log.error("Error - bank code file missing ");
		}

		String bank_NM = null;
		String bank_CD = null;
		JSONObject bank_CD_record = null;
		JSONArray bankCDlistoptions = bankCDlist.getJSONArray("options");

		if ((bankCDlist.length() > 0) && (!BankName.isEmpty())) {
			for (int h = 0; h < bankCDlistoptions.length(); h++) {
				bank_CD_record = bankCDlistoptions.getJSONObject(h);
				bank_NM = bank_CD_record.getString("value");
				//bank_CD = bank_CD_record.getString("rlscode");

				if (bank_NM.equals(BankName)) {
					bank_CD = bank_CD_record.getString("rlscode");
					break;
				}
			}
		}

		if (bank_CD == null) {
			return "";
		} else {
			return bank_CD;
		}
	}

	/**
	 * Prepare Object of
	 * RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private CanBeIndividual prepareCanBeIndividual(ApplicationBean appBean) throws Exception {
		CanBeIndividual canBeIndividual = new CanBeIndividual();
		try {


			//OWNER last name
			// sample:
			//if (appBean.quotation.has("pLastName")) {
			//	CanBeIndividual.lastNM = appBean.quotation.getString("pLastName");
			// } else {
			//	throw new IOException("Mandatory field missing - owner last name");
			//}

			//CanBeIndividual.lastNM = "";
			//if (is_1stparty || is_3rdparty)
			//{
			//	if (appBean.Owner_personalInfo.has("lastName"))
			//	{
			//		CanBeIndividual.lastNM = appBean.Owner_personalInfo.getString("lastName");
			//	}
			//}

			canBeIndividual.lastNM = "";
			canBeIndividual.firstNM = "";


			if (appBean.isThirdParty) {
				//				String appBean.ow.lastName = "";
				//				String appBean.ow.firstName = "";
				if (appBean.Owner_personalInfo.has("lastName")) {
					appBean.ow.lastName = spaceRemover(truncateSpecialChar(appBean.Owner_personalInfo.getString("lastName")));
				}
				if (appBean.Owner_personalInfo.has("firstName")) {
					appBean.ow.firstName = spaceRemover(truncateSpecialChar(appBean.Owner_personalInfo.getString("firstName")));
				}
				if (appBean.Owner_personalInfo.has("fullName")) {
					appBean.ow.fullName = spaceRemover(truncateSpecialChar(appBean.Owner_personalInfo.getString("fullName")));
				}
				if (appBean.Owner_personalInfo.has("hanyuPinyinName")) {
					appBean.ow.chineseName = spaceRemover(truncateSpecialChar(appBean.Owner_personalInfo.getString("hanyuPinyinName")));
				}
				if (appBean.Owner_personalInfo.has("othName")) {
					appBean.ow.otherName = spaceRemover(truncateSpecialChar(appBean.Owner_personalInfo.getString("othName")));
				}
				canBeIndividual.lastNM = appBean.ow.fullName;
				canBeIndividual.firstNM = "";
			}

			//mandatory gen owner for API testing only.

			//OWNER HKID
			//sample: obj.documentID = "S5678543";
			if (appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("idCardNo")) {
					canBeIndividual.documentID = truncateSpecialChar(appBean.Owner_personalInfo.getString("idCardNo"));
					appBean.ow.documentId = canBeIndividual.documentID;
				}
			}

			//OWNER GENDER *
			if (appBean.isThirdParty) {
				canBeIndividual.genderCD = appBean.ow.gender;
			}

			//OWNER BIRTH DATE
			//			if (appBean.quotation.has("pDob")) {
			//				Date pDob = new Date(appBean.quotation.getString("pDob"));
			//				obj.birthDT = pDob;
			//			}

			//Smaple: obj.birthDT = this.getDummyDate();//TODO
			if (appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("dob")) {
					canBeIndividual.birthDT = appBean.Owner_personalInfo.getString("dob");
					appBean.ow.birthdate = canBeIndividual.birthDT;
				}
			}

			//HEIGHT IN METRE FOR OWNER
			// sample : obj.height = "0";
			if (appBean.isThirdParty) {
				if (appBean.Owner_insurability.has("HW01")) {
					canBeIndividual.height = Double.toString(appBean.Owner_insurability.getDouble("HW01"));
				}
			}

			//WEIGHT IN KG FOR OWNER
			// sample :obj.weight = "0";
			if (appBean.isThirdParty && (appBean.ow.weight > 0)) {
				canBeIndividual.weight = Double.toString(appBean.ow.weight);
			}

			//OCC. CLASS FOR OWNER
			//obj.stdOccupationCD = "1";
			canBeIndividual.stdOccupationCD = "";
			if (appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("occupation")) {
					canBeIndividual.stdOccupationCD = getOccupationClass(appBean.Owner_personalInfo.getString("occupation"), appBean.ow.issueAge, appBean.ow.gender);
				}
			}

			//MONTHLY INCOME FOR PAYOR
			canBeIndividual.annualIncomeAMT = "0";

			if (appBean.isThirdParty) {
				if (appBean.proposerClientDoc.has("allowance")) {
					canBeIndividual.annualIncomeAMT = appBean.proposerClientDoc.get("allowance").toString();
				}
				if ("4".equals(canBeIndividual.stdOccupationCD) || "6".equals(canBeIndividual.stdOccupationCD)) {
					canBeIndividual.annualIncomeAMT = "0";
				}
			}

			//COUNTRY OF ORIGIN
			//sample: obj.countryOfOriginDESC = "Malaysia";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.Owner_personalInfo.has("nationality")) {
					if (NationalityConversion(appBean.Owner_personalInfo.getString("nationality"), false) != null) {
						canBeIndividual.countryOfOriginDESC = NationalityConversion(appBean.Owner_personalInfo.getString("nationality"), false);
					}
				}
			}

			if (appBean.la.countryOfResidenceCD.equals("LR")) {
				canBeIndividual.countryOfOriginDESC = "";
			}

			if (appBean.isThirdParty) {
				//countryOfResidenceCD
				if (appBean.la.countryOfResidenceCD.equals(appBean.ow.countryOfResidenceCD))
					canBeIndividual.countryOfResidenceCD  = appBean.la.countryOfResidenceCD;
				else
					canBeIndividual.countryOfResidenceCD  = "FR";
			}

			//NATIONALITY
			//sample :obj.nationality = "Indian";
			canBeIndividual.nationality = "";

			if (appBean.Owner_personalInfo.has("nationality")) {
				canBeIndividual.nationality = NationalityConversion(appBean.Owner_personalInfo.getString("nationality"), false);
				appBean.ow.nationality = canBeIndividual.nationality;
			}

			//CKA fields
			//Default Y to broker channel
			canBeIndividual.ckaEducationFLG = "Y";
			canBeIndividual.ckaInvestmentExperienceFLG = "Y";
			canBeIndividual.ckaWorkExperienceFLG = "Y";
			canBeIndividual.ckaFLG = "Y";

			boolean OW_withCKA = false;
			JSONObject bundleFNADoc_ckaSection = new JSONObject();
			JSONObject bundleFNADoc_ckaSection_owner = new JSONObject();
			if (appBean.bundleFNADoc != null && appBean.bundleFNADoc.has("ckaSection")) {
				bundleFNADoc_ckaSection = appBean.bundleFNADoc.getJSONObject("ckaSection");
				if (bundleFNADoc_ckaSection.has("isValid")) {
					if (bundleFNADoc_ckaSection.getBoolean("isValid")) {
						if (bundleFNADoc_ckaSection.has("owner")) {
							bundleFNADoc_ckaSection_owner = bundleFNADoc_ckaSection.getJSONObject("owner");
							OW_withCKA = true;
						}
					}
				}
			}

			if (OW_withCKA) {
				try {
					String bundleFNADoc_ckaSection_course = bundleFNADoc_ckaSection_owner.getString("course");
					if (bundleFNADoc_ckaSection_course != null) {
						if ("notApplicable".equals(bundleFNADoc_ckaSection_course)) {
							canBeIndividual.ckaEducationFLG = "N";
						}
					}
				} catch (Exception CAK_e0001) {

				}

				try {
					String bundleFNADoc_ckaSection_collectiveInvestment = bundleFNADoc_ckaSection_owner.getString("collectiveInvestment");
					if (bundleFNADoc_ckaSection_collectiveInvestment != null) {
						if ("N".equals(bundleFNADoc_ckaSection_collectiveInvestment)) {
							canBeIndividual.ckaInvestmentExperienceFLG = "N";
						}
					}
				} catch (Exception CAK_e0002) {

				}

				try {
					String bundleFNADoc_ckaSection_workingExperience = bundleFNADoc_ckaSection_owner.getString("profession");
					if (bundleFNADoc_ckaSection_workingExperience != null) {
						if ("notApplicable".equals(bundleFNADoc_ckaSection_workingExperience)) {
							canBeIndividual.ckaWorkExperienceFLG = "N";
						}
					}
				} catch (Exception CAK_e0003) {

				}

				if ("Y".equals(canBeIndividual.ckaEducationFLG) || "Y".equals(canBeIndividual.ckaInvestmentExperienceFLG) || "Y".equals(canBeIndividual.ckaWorkExperienceFLG)) {
					canBeIndividual.ckaFLG = "Y";
				} else {
					canBeIndividual.ckaFLG = "N";
				}
			}

			canBeIndividual.hasCRSInformationIn = prepareHasCRSInformationIn(appBean);
			canBeIndividual.hasAddressesIn = prepareHasAddressInForOwner(appBean);
			canBeIndividual.hasIndividualUWInfoIn = prepareHasIndividualUWInfoIn(appBean);

			//RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual - E
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			return canBeIndividual;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return canBeIndividual;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > forLifeUnitofExposure
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<ForLifeUnitofExposure> prepareForLifeUnitofExposure(ApplicationBean appBean) throws Exception {
		List<ForLifeUnitofExposure> objList = new ArrayList<>();
		ForLifeUnitofExposure obj = new ForLifeUnitofExposure();
		try {

			//For Basic Plan

			//LIFE SUM INSURED
			//sample: ForLifeUnitofExposureItemType.currAMT = "1000";
			if (appBean.quotation_basicPlan.has("sumInsured")) {
				obj.currAMT = Double.toString(appBean.quotation_basicPlan.getDouble("sumInsured"));
			}

			//BASIC PREMIUM RATE
			//Need to fill up until build complete
			if (appBean.quotation_basicPlan.has("premRate")) {
				obj.basicPremiumRt = Double.toString(appBean.quotation_basicPlan.getDouble("premRate"));
			}

			//ATTACH TO for rider only
			//obj.referenceTO = "11.55";
			//obj.referenceTO = IsAssociatedWithLifeInsuranceProductItemType.insuranceProductCd;

			//for basic plan, always blank;
			obj.referenceTO = "";

			obj.hasCoverageDetailsIn = prepareHasCoverageDetailsIn("P", appBean.basicPlanCode, "S");
			objList.add(obj);

			// for rider:

			if (appBean.quotation_plans.length() > 1) {
				appBean.quotation_RiderPlan = null;
				for (int rider_count = 1; rider_count < appBean.quotation_plans.length(); rider_count++) {
					obj = new ForLifeUnitofExposure();

					String riderProductCode = "";
					String riderPlanClass = "";
					appBean.quotation_RiderPlan = appBean.quotation_plans.getJSONObject(rider_count);

					boolean isWOP_rider = false;
					if (appBean.quotation_RiderPlan.has("planCode")) {
						riderProductCode = appBean.quotation_RiderPlan.getString("planCode");
						if (riderProductCode.contains("WOP")) {
							isWOP_rider = true;
						}
					} else {
						throw new IOException("Mandatory field missing - basic plan code");
					}

					//rider class code  /rider class code
					riderPlanClass = "1";


					if (appBean.quotation_RiderPlan.has("sumInsured")) {
						obj.currAMT = Double.toString(appBean.quotation_RiderPlan.getDouble("sumInsured"));
					}

					//obj.basicPremiumRt = "0";
					if (appBean.quotation_RiderPlan.has("premRate")) {
						obj.basicPremiumRt = Double.toString(appBean.quotation_RiderPlan.getDouble("premRate"));
					}

					//obj.referenceTO = basic_product_planCD;
					if (isWOP_rider) {
						obj.referenceTO = appBean.basicPlanCode;
					} else {
						obj.referenceTO = "";
					}


					// add as element:
					//isAssociatedWithLifeInsuranceProductList.add(rider_count,IsAssociatedWithLifeInsuranceProductItemType);

					obj.hasCoverageDetailsIn = prepareHasCoverageDetailsIn("R", riderProductCode, riderPlanClass);

					objList.add(obj);
				}
			}


			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare  Object of
	 * RecordApplicationRequest > policy > forLifeUnitofExposure > hasCoverageDetailsIn
	 *
	 * @param baseCoverageIndicatorCd
	 * @param insuranceProductCd
	 * @param planClass
	 * @return
	 * @throws Exception
	 */
	private HasCoverageDetailsIn prepareHasCoverageDetailsIn(String baseCoverageIndicatorCd, String insuranceProductCd, String planClass) throws Exception {
		HasCoverageDetailsIn obj = new HasCoverageDetailsIn();
		try {


			obj.baseCoverageIndicatorCd = baseCoverageIndicatorCd;
			obj.isAssociatedWithLifeInsuranceProduct = prepareIsAssociatedWithLifeInsuranceProduct(insuranceProductCd, planClass);

			return obj;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return obj;
		}
	}

	/**
	 * Deprecated
	 * Prepare  Object of
	 * RecordApplicationRequest > policy > forLifeUnitofExposure > hasCoverageDetailsIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private HasCoverageDetailsIn prepareHasCoverageDetailsIn(ApplicationBean appBean) throws Exception {
		HasCoverageDetailsIn obj = new HasCoverageDetailsIn();
		try {

			//basic plan code

			obj.baseCoverageIndicatorCd = "P";

			obj.isAssociatedWithLifeInsuranceProduct = prepareIsAssociatedWithLifeInsuranceProduct2(appBean);

			return obj;

		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return obj;
		}
	}

	/**
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > forLifeUnitofExposure > hasCoverageDetailsIn > isAssociatedWithLifeInsuranceProduct
	 * or RecordApplicationRequest > policy > isAssociatedWithLifeInsuranceProduct
	 *
	 * @param insuranceProductCd
	 * @param planClass
	 * @return
	 * @throws Exception
	 */
	private List<IsAssociatedWithLifeInsuranceProduct> prepareIsAssociatedWithLifeInsuranceProduct(String insuranceProductCd, String planClass) throws Exception {
		List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProductList = new ArrayList<>();
		IsAssociatedWithLifeInsuranceProduct isAssociatedWithLifeInsuranceProduct = new IsAssociatedWithLifeInsuranceProduct();
		try {

			isAssociatedWithLifeInsuranceProduct.insuranceProductCd = insuranceProductCd;
			isAssociatedWithLifeInsuranceProduct.planClassCD = planClass;
			isAssociatedWithLifeInsuranceProductList.add(isAssociatedWithLifeInsuranceProduct);

			return isAssociatedWithLifeInsuranceProductList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return isAssociatedWithLifeInsuranceProductList;
		}
	}

	/**
	 * Depreciated.
	 * Prepare Array Object of
	 * RecordApplicationRequest > policy > forLifeUnitofExposure > hasCoverageDetailsIn > isAssociatedWithLifeInsuranceProduct
	 * Note: this is different from the one RecordApplicationRequest > policy > isAssociatedWithLifeInsuranceProduct
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<IsAssociatedWithLifeInsuranceProduct> prepareIsAssociatedWithLifeInsuranceProduct2(ApplicationBean appBean) throws Exception {
		List<IsAssociatedWithLifeInsuranceProduct> objList = new ArrayList<>();
		IsAssociatedWithLifeInsuranceProduct obj = new IsAssociatedWithLifeInsuranceProduct();
		try {


			obj.insuranceProductCd = appBean.basicPlanCode;

			//rider class code

			obj.planClassCD = "S";
			objList.add(obj);

			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare Object of
	 * RecordApplicationRequest > policy > hasCustomerInformationIn > canBeIndividual > hasCRSInformationIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private HasCRSInformationIn prepareHasCRSInformationIn(ApplicationBean appBean) throws Exception {

		HasCRSInformationIn hasCRSInformationIn = new HasCRSInformationIn();
		List<String> taxIdentificationNOType = new ArrayList<>();
		List<String> countryOfTaxResidenceType = new ArrayList<>();

		try {
			//FATCA /CRS

			hasCRSInformationIn.CRSType = appBean.ownerIND;
			hasCRSInformationIn.CRSStatus = "N";
			hasCRSInformationIn.followUpIndicator = "N";

			//BBBB
			//JSONObject tmp_json = new JSONObject();

			if (appBean.Owner_declaration.has("FATCA01") && "Y".equals(appBean.Owner_declaration.getString("FATCA01"))) {
				String Str_SINGAPORE = "SINGAPORE";
				countryOfTaxResidenceType.add(Str_SINGAPORE);
				//hasCRSInformationIn.countryOfTaxResidence= countryOfTaxResidenceType;

				if (appBean.Owner_declaration.has("FATCA01b")) {
					taxIdentificationNOType.add(truncateSpecialChar(appBean.Owner_declaration.getString("FATCA01b")));
					//hasCRSInformationIn.taxIdentificationNO = taxIdentificationNOType;
				}
			} 

			if (appBean.Owner_declaration.has("FATCA02") && "Y".equals(appBean.Owner_declaration.getString("FATCA02"))) {
				String Str_UNITEDSTATES = "UNITED STATES";
				countryOfTaxResidenceType.add(Str_UNITEDSTATES);
				//hasCRSInformationIn.countryOfTaxResidence= countryOfTaxResidenceType;

				if (appBean.Owner_declaration.has("FATCA03")) {
					taxIdentificationNOType.add(truncateSpecialChar(appBean.Owner_declaration.getString("FATCA03")));
					//hasCRSInformationIn.taxIdentificationNO = taxIdentificationNOType;
				}

				hasCRSInformationIn.followUpIndicator = "Y";
			}

			if (appBean.Owner_declaration.has("CRS01") && "Y".equals(appBean.Owner_declaration.getString("CRS01"))) {
				hasCRSInformationIn.CRSStatus = "Y";
				String tax_country = null;

				if (appBean.Owner_declaration.has("CRS01_DATA")) {
					JSONArray Owner_declaration_Array = appBean.Owner_declaration.getJSONArray("CRS01_DATA");

					JSONObject Owner_declaration_Object = null;
					for (int i = 0; i < Owner_declaration_Array.length(); i++) {
						Owner_declaration_Object = Owner_declaration_Array.getJSONObject(i);

						if (Owner_declaration_Object.has("CRS01a")) {
							tax_country = Owner_declaration_Object.getString("CRS01a");
							countryOfTaxResidenceType.add(CountryConversion(tax_country));
							//hasCRSInformationIn.countryOfTaxResidence = taxIdentificationNOType;
						}

						if (Owner_declaration_Object.has("CRS01b")) {
							taxIdentificationNOType.add(Owner_declaration_Object.getString("CRS01b"));
							//hasCRSInformationIn.taxIdentificationNO = taxIdentificationNOType;
						}
					}
				}

				hasCRSInformationIn.followUpIndicator = "Y";
			} else {
				countryOfTaxResidenceType.add("");
				taxIdentificationNOType.add("");
				hasCRSInformationIn.countryOfTaxResidence = countryOfTaxResidenceType;
				hasCRSInformationIn.taxIdentificationNO = taxIdentificationNOType;
			}

			hasCRSInformationIn.countryOfTaxResidence = countryOfTaxResidenceType;
			hasCRSInformationIn.taxIdentificationNO = taxIdentificationNOType;

			if (appBean.Owner_personalInfo.has("residenceCountry") && (!"R2".equals((appBean.Owner_personalInfo.getString("residenceCountry")))))
				hasCRSInformationIn.followUpIndicator = "Y";

			hasCRSInformationIn.FATCAStatus = "N";

			//If US Citizen /Tax Resident = Yes, 
			if (appBean.Owner_declaration.has("FATCA02")) {
				if ("Y".equals(appBean.Owner_declaration.getString("FATCA02")))
					hasCRSInformationIn.FATCAStatus = "Y";
			}

			//Country of Residential Address = United States
			if (appBean.Owner_personalInfo.has("residenceCountry")) {
				if ("R252".equals(appBean.Owner_personalInfo.getString("residenceCountry")))
					hasCRSInformationIn.FATCAStatus = "Y";
			}

			// Country of Mailing Address = United States
			if(appBean.Owner_personalInfo.has("addrCountry")) {
				if ("R252".equals(appBean.Owner_personalInfo.getString("addrCountry")))
					hasCRSInformationIn.FATCAStatus = "Y";
			}

			//Country of Employer Address = United States
			if (appBean.Owner_personalInfo.has("organizationCountry")) {
				if ("R252".equals(appBean.Owner_personalInfo.getString("organizationCountry")))
					hasCRSInformationIn.FATCAStatus = "Y";
			}

			//Country Telephone Code = +1
			if (appBean.Owner_personalInfo.has("mobileCountryCode")) {
				if ("+1".equals(appBean.Owner_personalInfo.getString("mobileCountryCode")) || "+1".equals(appBean.Owner_personalInfo.getString("otherMobileCountryCode")))
					hasCRSInformationIn.FATCAStatus = "Y";
			}

			Policy policy = new Policy();

			//when "followUpIndicator": "Y", make medicalIND = Y
			if (hasCRSInformationIn.followUpIndicator.equals("Y"))
				policy.medicalIND = "Y";

			return hasCRSInformationIn;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return hasCRSInformationIn;
		}
	}

	private String getOccupationClass(String occupationCode, Integer parm_age, String parm_gender) {
		JSONObject occupationCDlist = null;
		occupationCDlist = Constant.CBUTIL.getDoc("occupation");

		if (occupationCDlist == null) {
			Log.error("Error - occupation mapping file missing ");
		}

		String occupation_CD = null;
		String occupation_Class = null;
		JSONObject occupation_list_record = null;
		JSONArray occupationListoptions = occupationCDlist.getJSONArray("options");

		if ((occupationListoptions.length() > 0) && (!occupationCode.isEmpty())) {
			for (int h = 0; h < occupationListoptions.length(); h++) {
				occupation_list_record = occupationListoptions.getJSONObject(h);
				occupation_CD = occupation_list_record.getString("value");

				if (occupation_CD.equals(occupationCode)) {
					occupation_Class = Integer.toString(occupation_list_record.getJSONObject("class").getInt("all"));
					break;
				}
			}
		}

		//cr per mail: Wed 10/25/2017 5:07 PM
		if ("O674".equals(occupationCode) && "F".equals(parm_gender)) {
			occupation_Class = "5";
		}
		if ("O675".equals(occupationCode) && "M".equals(parm_gender)) {
			occupation_Class = "5";
		}
		if ("O1321".equals(occupationCode) && (parm_age < 18)) {
			occupation_Class = "5";
		}
		if ("O1322".equals(occupationCode) && (parm_age >= 18)) {
			occupation_Class = "5";
		}

		if (occupation_Class == null) {
			return "";
		} else {
			return occupation_Class;
		}
	}

	private String CityConversion(String city_code) {
		//Get city json file - S
		JSONObject cityList = null;
		cityList = Constant.CBUTIL.getDoc("city");

		if (cityList == null) {
			Log.error("Error - city file missing ");
		}

		JSONArray city_list = cityList.getJSONArray("options");
		JSONObject city_record = null;
		JSONObject city_title = null;
		String city_option = null;
		String city_countryNM = null;

		if ((city_list.length() > 0) && (!city_code.isEmpty())) {
			for (int i = 0; i < city_list.length(); i++) {
				city_record = city_list.getJSONObject(i);
				city_option = city_record.getString("value");

				if (city_option.equals(city_code)) {
					city_title = city_record.getJSONObject("title");
					city_countryNM = city_title.getString("en");
					break;
				}
			}
		}

		//		if (city_countryNM == null) {
		//			if (city_code == "" || city_code == null) 
		//				return "";
		//			else
		//				return city_code;
		//		} else {
		//			return city_countryNM;
		//		}

		return "";//Requested by AXA to pass blank to city on release 1, refer mantis issue 0031811
	}

	private String CountryConversion(String Country_code) {
		//Get residency json file - S
		JSONObject residencyList = null;
		residencyList = Constant.CBUTIL.getDoc("residency");

		if (residencyList == null) {
			Log.error("Error - residency file missing ");
		}

		JSONArray residency_list = residencyList.getJSONArray("options");
		JSONObject residency_record = null;
		JSONObject residency_title = null;
		String residency_option = null;
		String residency_countryNM = null;

		if ((residency_list.length() > 0) && (!Country_code.isEmpty())) {
			for (int i = 0; i < residency_list.length(); i++) {
				residency_record = residency_list.getJSONObject(i);
				residency_option = residency_record.getString("value");

				if (residency_option.equals(Country_code)) {
					residency_title = residency_record.getJSONObject("title");
					residency_countryNM = residency_title.getString("en");
					break;
				}
			}
		}

		return residency_countryNM;
	}

	private String getCountryCD(String country_NM) {
		JSONObject countryCDlist = null;
		countryCDlist = Constant.CBUTIL.getDoc("standard_country_code");

		if (countryCDlist == null) {
			Log.error("Error - country code file missing ");
		}

		JSONArray countryCD_list = countryCDlist.getJSONArray("options");
		JSONObject countryCD_record = null;
		JSONObject country_name = null;
		String countryCD_NM = null;
		String countryCD = null;

		if ((countryCD_list.length() > 0) && (!country_NM.isEmpty())) {
			for (int k = 0; k < countryCD_list.length(); k++) {
				countryCD_record = countryCD_list.getJSONObject(k);
				country_name = countryCD_record.getJSONObject("title");
				countryCD_NM = country_name.getString("en");

				if (countryCD_NM.equals(country_NM)) {
					countryCD = countryCD_record.getString("value");
					break;
				}
			}
		}

		if (countryCD == null) {
			return "";
		} else {
			return countryCD;
		}
	}

	private String NationalityConversion(String Nationality_code, boolean isConnectedParty) {
		//Get nationality json file - S
		JSONObject nationalityList = null;
		nationalityList = Constant.CBUTIL.getDoc("nationality");

		if (nationalityList == null) {
			Log.error("Error - nationality file missing ");
		}

		JSONArray nationality_list = nationalityList.getJSONArray("options");
		JSONObject nationality_record = null;
		JSONObject nationality_title = null;
		String nationality_option = null;
		String nationality_countryNM = null;
		String nationality_countryNM_RLS = null;

		if ((nationality_list.length() > 0) && (!Nationality_code.isEmpty())) {
			for (int i = 0; i < nationality_list.length(); i++) {
				nationality_record = nationality_list.getJSONObject(i);
				nationality_option = nationality_record.getString("value");

				if (nationality_option.equals(Nationality_code)) {
					nationality_title = nationality_record.getJSONObject("title");
					nationality_countryNM = nationality_title.getString("en");
					nationality_countryNM_RLS = nationality_record.getString("rlsCode");
					break;
				}
			}
		}

		if (isConnectedParty) {
			if (nationality_countryNM_RLS == null) {
				return "";
			} else {
				return nationality_countryNM_RLS;
			}
		} else {
			if (nationality_countryNM == null) {
				return "";
			} else {
				return nationality_countryNM;
			}
		}
	}

	private String getCurrentDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	private Integer getDaysPassed(String dateFrom, String dateTo) {
		Calendar calDateFrom = null;  // From Date
		Calendar calDateTo = null;      // To date
		Calendar lastBirthDT = null;    //
		Calendar nextBirthDT = null; //

		String[] commDateFrom = null;
		String[] commDateTo = null;
		Integer daysPS = 0;

		try {
			if (dateFrom != null && dateTo != null) {
				commDateFrom = dateFrom.split("-");
				commDateTo = dateTo.split("-");

				// Assume Date String must be YYYY-MM-DD
				calDateFrom = new GregorianCalendar(Integer.parseInt(commDateFrom[0]), Integer.parseInt(commDateFrom[1]), Integer.parseInt(commDateFrom[2]));
				calDateTo = new GregorianCalendar(Integer.parseInt(commDateTo[0]), Integer.parseInt(commDateTo[1]), Integer.parseInt(commDateTo[2]));

				if (calDateFrom.compareTo(calDateTo) >= 0) {
					return daysPS;
				} else {
					for (int i = 0; ; i++) {
						if (calDateFrom.before(calDateTo)) {
							calDateFrom.add(Calendar.DAY_OF_MONTH, 1);
							daysPS++;
						} else {
							break;
						}
					}
					return daysPS;
				}
			}
		} catch (Exception e) {
			Log.error(e);
		}
		return null;
	}

	private String add_leading_zero(String missing_zero_string) {
		missing_zero_string = "0000000000000000".concat(missing_zero_string);
		return missing_zero_string;
	}

	/*private String[] policyRemarks(String policyRemark) {
        String[] remarklist = new String[99];

		for (int i = 0; i < remarklist.length; i++) {
			if (remarklist[i].equals(policyRemark)) {
				break;
			}
			if ("".equals(remarklist[i])) {
				remarklist[i] =  policyRemark;
				break;
			}
		}
		return remarklist;
	}*/

	private String changeDateFormat(Date _date) {
		SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
		String strDate = sdfDate.format(_date);

		return strDate;
	}

	private String age_cross_date(Date _date) {
		SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
		String strDate = sdfDate.format(_date);

		return strDate;
	}

	/**
	 * Prepare Address Detail of Insured.
	 * RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn > hasAddressesIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasAddressInRef> prepareHasAddressesInForInsured(ApplicationBean appBean) throws Exception {
		List<HasAddressInRef> objList = new ArrayList<>();
		HasAddressInRef obj = new HasAddressInRef();
		try {


			String ow_tmp2_block = "";
			String ow_tmp2_unit = "";
			String ow_addressline1 = "";
			String ow_addressline2 = "";
			String ow_addressline3 = "";
			String ow_addressline4 = "";
			String ow_addressline5 = "";

			if (appBean.Owner_personalInfo.has("addrBlock")) {
				ow_tmp2_block = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrBlock"));
			}

			if (appBean.Owner_personalInfo.has("unitNum")) {
				ow_tmp2_unit = truncateSpecialChar(appBean.Owner_personalInfo.getString("unitNum"));
			}

			if ("".equals(ow_tmp2_block) && "".equals(ow_tmp2_unit)) {
				obj.addressLine1 = "";
			} else if ("".equals(ow_tmp2_block)) {
				obj.addressLine1 = ow_tmp2_unit;
			} else if ("".equals(ow_tmp2_unit)) {
				obj.addressLine1 = ow_tmp2_block;
			} else {
				obj.addressLine1 = ow_tmp2_block + " " + ow_tmp2_unit;
			}
			ow_addressline1 = obj.addressLine1;

			//Address 2
			obj.addressLine2 = "";

			if (appBean.Owner_personalInfo.has("addrStreet")) {
				obj.addressLine2 = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrStreet"));
			}
			ow_addressline2 = obj.addressLine2;

			//Address 3
			String ow_tmp2_building = "";
			String ow_tmp2_city = "";

			if (appBean.Owner_personalInfo.has("addrEstate")) {
				ow_tmp2_building = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrEstate"));
			}

			if (appBean.Owner_personalInfo.has("addrCity")) {
				ow_tmp2_city = truncateSpecialChar(appBean.Owner_personalInfo.getString("addrCity"));
			}

			if ("".equals(ow_tmp2_building) && "".equals(ow_tmp2_city)) {
				obj.addressLine3 = "";
			} else if ("".equals(ow_tmp2_building)) {
				obj.addressLine3 = CityConversion(ow_tmp2_city);
			} else if ("".equals(ow_tmp2_city)) {
				obj.addressLine3 = ow_tmp2_building;
			} else {
				obj.addressLine3 = ow_tmp2_building + " " + CityConversion(ow_tmp2_city);
			}
			ow_addressline3 = obj.addressLine3;
			//Address 4
			String ow_tmp2_country = "";
			obj.addressLine4 = "";

			if (appBean.Owner_personalInfo.has("addrCountry")) {
				ow_tmp2_country = CountryConversion(truncateSpecialChar(appBean.Owner_personalInfo.getString("addrCountry")));

				if (ow_tmp2_country.toLowerCase().contains("China".toLowerCase())) {
					obj.addressLine4 = "China" + " " + truncateSpecialChar(appBean.Owner_personalInfo.getString("postalCode"));
				} else {
					obj.addressLine4 = ow_tmp2_country + " " + truncateSpecialChar(appBean.Owner_personalInfo.getString("postalCode"));
				}
			}
			ow_addressline4 = obj.addressLine4;

			//Address 5
			obj.cityNM = "";

			if (appBean.Owner_personalInfo.has("addrCountry")) {
				obj.cityNM = getCountryCD(ow_tmp2_country);
			}

			//MOBILE'NUMBER OF INSURED
			//obj.mobilePhoneNO = "9876512345";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_personalInfo.has("mobileNo")) {
					try {
						obj.mobilePhoneNO = Long.toString(appBean.LifeAssured_personalInfo.getLong("mobileNo"));
					} catch (Exception ex002) {
						obj.mobilePhoneNO = appBean.LifeAssured_personalInfo.getString("mobileNo");
					}
				}
			}

			//EMAIL ADDRESS OF INSURED
			/*
				obj.emailAddress = appBean.quotation.getString("iEmail");
			 */

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_personalInfo.has("email")) {
					obj.emailAddress = truncateSpecialChar(appBean.LifeAssured_personalInfo.getString("email"));
				}
			}


			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	/**
	 * Prepare the Underwriting Info for Insured
	 * RecordApplicationRequest > policy > hasDetailsOfRisksIn > hasPersonalDetailsIn > hasIndividualUWInfoIn
	 *
	 * @param appBean
	 * @return
	 * @throws Exception
	 */
	private List<HasIndividualUWInfoInRef> prepareHasIndividualUWInfoInForInsured(ApplicationBean appBean,  HasPersonalDetailsIn hasPersonalDetailsIn) throws Exception {
		List<HasIndividualUWInfoInRef> objList = new ArrayList<>();
		HasIndividualUWInfoInRef obj = new HasIndividualUWInfoInRef();
		Policy policy = new Policy();
		//CanBeIndividualReference obj_CanBeIndividual = new CanBeIndividualReference();
		try {
			obj.healthQuestionIND = "N";

			//CUSTOMER PROTECTION DECLARA
			obj.customerProtectionDeclarationIND = "N";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_policies.has("isProslReplace")) {
					obj.customerProtectionDeclarationIND = appBean.LifeAssured_policies.getString("isProslReplace");
				}
			}

			//AVOCATION QUESTION
			obj.avocationQuestionIND = "N";
			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE05")) {
					if ("Y".equals(appBean.LifeAssured_insurability.getString("LIFESTYLE05"))) {
						obj.avocationQuestionIND = "Y";
					} else {
						obj.avocationQuestionIND = "N";
					}
				}
			}

			//DECLINE QUESTION
			obj.declineQuestionIND = "N";
			if ((!appBean.isOWPMED) || (!appBean.isPolicyPMED)) {
				if (appBean.LifeAssured_insurability.has("INS01")) {
					obj.declineQuestionIND = appBean.LifeAssured_insurability.getString("INS01");

					if ("Y".equals(appBean.LifeAssured_insurability.getString("INS01"))) {
						//			appBean.isLAPMED = true;
						appBean.isPolicyPMED = true;
					}
				}
			}

			if ((!appBean.isLAPMED) || (!appBean.isPolicyPMED)) {
				if (appBean.LifeAssured_insurability.has("INS02")) {
					if ("Y".equals(appBean.LifeAssured_insurability.getString("INS02"))) {
						appBean.isLAPMED = true;
						appBean.isPolicyPMED = true;
					}
				}
			}

			if ((!appBean.isLAPMED)) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE06")) {
					if ("Y".equals(appBean.LifeAssured_insurability.getString("LIFESTYLE06"))) {
						appBean.isLAPMED = true;
					}
				}
			}

			String LA_medicalQN = null;

			for (int QN_COUNT = 0; QN_COUNT < appBean.medicalQNString.length; QN_COUNT++) {
				LA_medicalQN = appBean.medicalQNString[QN_COUNT];

				if (appBean.LifeAssured_insurability.has(LA_medicalQN))
					if ("Y".equals(appBean.LifeAssured_insurability.getString(LA_medicalQN))) {
						appBean.isLAPMED = true;
						appBean.isPolicyPMED = true;

						if (appBean.isLAPMED && appBean.isPolicyPMED) {
							break;
						}
					}
			}

			//Details of Regular Doctor
			if (appBean.Owner_insurability.has("REG_DR01_DATA")) {
				JSONArray REG_DR01_DATA_array = appBean.Owner_insurability.getJSONArray("REG_DR01_DATA");

				for(int i = 0; i < REG_DR01_DATA_array.length(); i++) {
					JSONObject REG_DR01_DATA = REG_DR01_DATA_array.getJSONObject(i);

					if (REG_DR01_DATA.has("REG_DR01d")) {
						if (REG_DR01_DATA.getString("REG_DR01d").toUpperCase().equals("OTHER")) {
							appBean.isLAPMED = true;
							appBean.isPolicyPMED = true;
							break;
						}
					}
				}
			}

			Double LA_weight_change = 0.00;
			Double LA_weight_GainLost = 0.00;
			Double LA_weightchangePC = 0.0;

			if ((!appBean.isLAPMED) || (!appBean.isPolicyPMED)) {
				if (appBean.LifeAssured_insurability.has("HW03") && ("Y".equals(appBean.LifeAssured_insurability.getString("HW03")))) {
					if (appBean.LifeAssured_insurability.has("HW03b")) {
						LA_weight_change = appBean.LifeAssured_insurability.getDouble("HW03b");
					}

					if (LA_weight_change > 0) {
						if ("L".equals(appBean.LifeAssured_insurability.getString("HW03a"))) {
							LA_weight_GainLost = LA_weight_change * (-1);
						} else {
							LA_weight_GainLost = LA_weight_change;
						}
					}
					LA_weightchangePC = LA_weight_change / (appBean.la.weight + LA_weight_GainLost);

					if (LA_weightchangePC > 0.05) {
						appBean.isLAPMED = true;
						appBean.isPolicyPMED = true;
					} else if (appBean.LifeAssured_insurability.has("HW03c") && ("Other".equals(appBean.LifeAssured_insurability.getString("HW03c")))) {
						appBean.isLAPMED = true;
						appBean.isPolicyPMED = true;
					}
				}
			}

			obj.smokingQuestionIND = "N";

			if (appBean.LifeAssured_insurability.has("LIFESTYLE01")) {
				obj.smokingQuestionIND = appBean.LifeAssured_insurability.getString("LIFESTYLE01");

				if (appBean.LifeAssured_insurability.getString("LIFESTYLE01").equals("Y"))
					appBean.isPolicyPMED = true;
			}


			obj.cigarettesPerDayNO = "0";

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE01b")) {
					obj.cigarettesPerDayNO = Integer.toString(appBean.LifeAssured_insurability.getInt("LIFESTYLE01b"));
				}
			}

			//            //RESIDENCE DECLARATION CODE
			//            obj.residenceDeclCD = "OIF";
			//
			//            //Below is SIF case, only for 1st party case, LA is proposer for 1st party.
			//            if (appBean.isFirstParty || appBean.isThirdParty) {
			//                //residing in Singapore = EAPP-P6-F1
			//                if (appBean.LifeAssured_residency.has("EAPP-P6-F1")) {
			//                    if ("resided".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F1")) || "outside".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F1"))) {
			//                        obj.residenceDeclCD = "SIF";
			//                    }
			//                }
			//
			//                //EAPP-P6-F2 = Have you resided in Singapore for at least 183 days in the last 12 months preceding the date of proposal?
			//                if (appBean.LifeAssured_residency.has("EAPP-P6-F2")) {
			//                    if ("Y".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F2"))) {
			//                        obj.residenceDeclCD = "SIF";
			//                    }
			//                }
			//
			//                //EAPP-P6-F3 = Do you intend to stay in Singapore on a permanent basis?
			//                if (appBean.LifeAssured_residency.has("EAPP-P6-F3")) {
			//
			//                }
			//
			//                //EAPP-P6-F4 = Have you resided in Singapore for at least 90 days in the last 12 months preceding the date of proposal?
			//                if (appBean.LifeAssured_residency.has("EAPP-P6-F4")) {
			//                    if ("Y".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F4"))) {
			//                        obj.residenceDeclCD = "SIF";
			//                    } else {
			//                    	obj.residenceDeclCD  = "OIF";
			//					}
			//                }
			//            }
			//            if (appBean.isThirdParty && appBean.isSameAnswer) {
			//                obj.residenceDeclCD = "SIF";
			//            } else if (appBean.isThirdParty && !appBean.isSameAnswer) {
			//                obj.residenceDeclCD = "OIF";
			//            }


			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			String LA_residenceDeclCD = "";
			String OW_residenceDeclCD = "";
			String LA_countryOfResidenceCD = "";
			String OW_countryOfResidenceCD = "";
			String LA_medicalIND = "";
			String OW_medicalIND = "";


			String LA_P6_Q1 = "";
			String LA_P6_Q2 = "";
			String LA_P6_Q3 = "";
			String LA_P6_Q4 = "";
			String OW_P6_Q1 = "";
			String OW_P6_Q2 = "";
			String OW_P6_Q3 = "";
			String OW_P6_Q4 = "";

			//Insured
			if (appBean.LifeAssured_residency.has("EAPP-P6-F1"))
				LA_P6_Q1 = appBean.LifeAssured_residency.getString("EAPP-P6-F1");

			if (appBean.LifeAssured_residency.has("EAPP-P6-F2"))
				LA_P6_Q2 = appBean.LifeAssured_residency.getString("EAPP-P6-F2");

			if (appBean.LifeAssured_residency.has("EAPP-P6-F3"))
				LA_P6_Q3 = appBean.LifeAssured_residency.getString("EAPP-P6-F3");

			if (appBean.LifeAssured_residency.has("EAPP-P6-F4"))
				LA_P6_Q4 = appBean.LifeAssured_residency.getString("EAPP-P6-F4");

			//Proposer
			if (appBean.Owner_residency.has("EAPP-P6-F1"))
				OW_P6_Q1 = appBean.Owner_residency.getString("EAPP-P6-F1");

			if (appBean.Owner_residency.has("EAPP-P6-F2"))
				OW_P6_Q2 = appBean.Owner_residency.getString("EAPP-P6-F2");

			if (appBean.Owner_residency.has("EAPP-P6-F3"))
				OW_P6_Q3 = appBean.Owner_residency.getString("EAPP-P6-F3");

			if (appBean.Owner_residency.has("EAPP-P6-F4"))
				OW_P6_Q4 = appBean.Owner_residency.getString("EAPP-P6-F4");

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Life Assured
			//residenceDeclCD
			//residing in Singapore or residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			if (LA_P6_Q1.equals("resided") || LA_P6_Q1.equals("outside"))
				LA_residenceDeclCD = "SIF";	
			else if (LA_P6_Q1.equals("outside5"))//residing outside Singapore. I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
				LA_residenceDeclCD = "OIF";	

			//Owner
			//residing in Singapore or residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			if (OW_P6_Q1.equals("resided") || OW_P6_Q1.equals("outside"))
				OW_residenceDeclCD = "SIF";	
			else if (OW_P6_Q1.equals("outside5"))//residing outside Singapore. I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
				OW_residenceDeclCD = "OIF";	

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//medicalIND
			//Life Assured
			//residing in Singapore 
			if (LA_P6_Q1.equals("resided"))
				LA_medicalIND = "N";	
			//I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
			//residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			else if (LA_P6_Q1.equals("outside5") || LA_P6_Q1.equals("outside"))//residing outside Singapore. 
				LA_medicalIND = "Y";	

			//Owner
			//residing in Singapore 
			if (OW_P6_Q1.equals("resided") )
				OW_medicalIND = "N";	
			//I have resided outside Singapore continously for more than 5 years preceding the date of proposal.
			//residing outside Singapore. I have resided outside Singapore continously for less than 5 years preceding the date of proposal.
			else if (OW_P6_Q1.equals("outside5") || OW_P6_Q1.equals("outside"))
				OW_medicalIND = "Y";	

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Life Assured
			//countryOfResidenceCD
			//LA_P6_Q2 = Have you resided in Singapore for at least 183 days in the 12 months preceding the date of proposal?
			//LA_P6_Q3 = Do you intend to stay in Singapore on a permanent basis?
			if (LA_P6_Q2.equals("Y") && LA_P6_Q3.equals("Y")) {
				LA_residenceDeclCD = "SIF";
				LA_countryOfResidenceCD = "LR";
				LA_medicalIND = "N";
			} else if (LA_P6_Q2.equals("N") && LA_P6_Q3.equals("Y")) {
				LA_residenceDeclCD = "OIF";
				LA_countryOfResidenceCD = "FR";
				LA_medicalIND = "Y";
			} else if (LA_P6_Q2.equals("Y") && LA_P6_Q3.equals("N")) {
				LA_residenceDeclCD = "SIF";
				LA_countryOfResidenceCD = "LR";
				LA_medicalIND = "Y";
			} else if (LA_P6_Q2.equals("N") && LA_P6_Q3.equals("N")) {
				LA_residenceDeclCD = "OIF";
				LA_countryOfResidenceCD = "FR";
				LA_medicalIND = "Y";
			}

			//Owner
			//OW_P6_Q2 = Have you resided in Singapore for at least 183 days in the 12 months preceding the date of proposal?
			//OW_P6_Q3 = Do you intend to stay in Singapore on a permanent basis?
			if (OW_P6_Q2.equals("Y") && OW_P6_Q3.equals("Y")) {
				OW_residenceDeclCD = "SIF";
				OW_countryOfResidenceCD = "LR";
				OW_medicalIND = "N";
			} else if (OW_P6_Q2.equals("N") && OW_P6_Q3.equals("Y")) {
				OW_residenceDeclCD = "OIF";
				OW_countryOfResidenceCD = "FR";
				OW_medicalIND = "Y";
			} else if (OW_P6_Q2.equals("Y") && OW_P6_Q3.equals("N")) {
				OW_residenceDeclCD = "SIF";
				OW_countryOfResidenceCD = "LR";
				OW_medicalIND = "Y";
			} else if (OW_P6_Q2.equals("N") && OW_P6_Q3.equals("N")) {
				OW_residenceDeclCD = "OIF";
				OW_countryOfResidenceCD = "FR";
				OW_medicalIND = "Y";
			}

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Have you resided in Singapore for at least 90 days in the last 12 months proceding the date of proposal?
			//Life Assured
			if (LA_P6_Q4.equals("Y")) {
				LA_residenceDeclCD = "SIF";
				LA_medicalIND = "Y";
			}
			else if (LA_P6_Q4.equals("N")) {
				LA_residenceDeclCD = "OIF";
				LA_medicalIND = "Y";
			}

			//Owner
			if (OW_P6_Q4.equals("Y")) {
				OW_residenceDeclCD = "SIF";
				OW_medicalIND = "Y";
			} else if (OW_P6_Q1.equals("N")) {
				OW_residenceDeclCD = "OIF";
				OW_medicalIND = "Y";
			}

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Life Assured
			if (LA_P6_Q1.equals("resided") || LA_P6_Q1.equals("outside") || LA_P6_Q1.equals("outside5"))
				LA_countryOfResidenceCD = "LR";	

			//Owner
			if (OW_P6_Q1.equals("resided") || OW_P6_Q1.equals("outside") || OW_P6_Q1.equals("outside5"))
				OW_countryOfResidenceCD = "LR";	

			//Life Assured
			if (LA_P6_Q4.equals("Y"))
				LA_countryOfResidenceCD = "FR";	
			else if (LA_P6_Q4.equals("N"))
				LA_countryOfResidenceCD = "FR";	

			//Owner
			if (OW_P6_Q4.equals("Y"))
				OW_countryOfResidenceCD = "FR";	
			else if (OW_P6_Q1.equals("N"))
				OW_countryOfResidenceCD = "FR";

			/////////////////////////////////////////////////////////////////////////////////////////////
			//Other Pass
			if (LA_residenceDeclCD.equals(""))
				LA_residenceDeclCD = "OIF";

			if (LA_countryOfResidenceCD.equals(""))
				LA_countryOfResidenceCD = "FR";

			if (LA_medicalIND.equals(""))
				LA_medicalIND = "Y";

			/////////////////////////////////////////////////////////////////////////////////////////////
			if (appBean.isThirdParty) {
				//residenceDeclCD
				if (LA_residenceDeclCD.equals(OW_residenceDeclCD))
					obj.residenceDeclCD = LA_residenceDeclCD;
				else
					obj.residenceDeclCD = "OIF";

				//            	//countryOfResidenceCD
				//            	if (LA_countryOfResidenceCD.equals(OW_countryOfResidenceCD))
				//            		obj_CanBeIndividual.countryOfResidenceCD  = LA_countryOfResidenceCD;
				//            	else
				//            		obj_CanBeIndividual.countryOfResidenceCD  = "FR";

				appBean.la.countryOfResidenceCD = LA_countryOfResidenceCD;
				appBean.ow.countryOfResidenceCD = OW_countryOfResidenceCD;

				//countryOfResidenceCD
				if (LA_medicalIND.equals(OW_medicalIND)) 
					policy.medicalIND = LA_medicalIND;	
				else 
					policy.medicalIND = "Y";

				//    			if (OW_countryOfResidenceCD.equals("LR"))
				//    				obj_CanBeIndividual.countryOfOriginDESC = ""; 
			} else {

				//HasPersonalDetailsIn hasPersonalDetailsIn = new HasPersonalDetailsIn();

				obj.residenceDeclCD =  LA_residenceDeclCD;
				hasPersonalDetailsIn.countryOfResidenceCD  = LA_countryOfResidenceCD;
				policy.medicalIND = LA_medicalIND;

				//            	if (LA_countryOfResidenceCD.equals("LR"))
				//            		obj_CanBeIndividual.countryOfOriginDESC = "";
			}

			//			if (hasPersonalDetailsIn.countryOfResidenceCD.equals("LR")) {
			//				obj_CanBeIndividual.countryOfOriginDESC = "";
			//			}

			appBean.la.countryOfResidenceCD = hasPersonalDetailsIn.countryOfResidenceCD;

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			//BD01-026
			if (!appBean.isPolicyPMED) {
				boolean isPassResidencyCHK = true;
				if (appBean.isFirstParty) {
					if (appBean.LifeAssured_residency.has("EAPP-P6-F1")) {
						if ("resided".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F1"))) {
							isPassResidencyCHK = true;
						} else {
							isPassResidencyCHK = false;
						}
					}
					if (appBean.LifeAssured_residency.has("EAPP-P6-F4")) {
						//if ("Y".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F4")) && "Y".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F5")))	{
						if ("Y".equals(appBean.LifeAssured_residency.getString("EAPP-P6-F4"))) {//TODO
							isPassResidencyCHK = true;
						} else {
							isPassResidencyCHK = false;
						}
					}
				}
				if (!isPassResidencyCHK) {
					appBean.isPolicyPMED = true;
				}
				if (appBean.isThirdParty && !appBean.isSameAnswer) {
					appBean.isPolicyPMED = true;
				}
			}

			//ALCOHOL QUESTION
			//sample: obj.alcoholQuestionIND = "Y";

			boolean LA_is_drinker = false;
			obj.alcoholQuestionIND = "N";

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE02")) {
					obj.alcoholQuestionIND = appBean.LifeAssured_insurability.getString("LIFESTYLE02");

					if ("Y".equals(appBean.LifeAssured_insurability.getString("LIFESTYLE02"))) {
						LA_is_drinker = true;
					}
				}
			}

			//ALCOHOL UNITS/WEEK
			//obj.alcoholUnitsPerWeekNO = "27";
			Integer LA_alcohol_unit_weekly = 0;

			if (LA_is_drinker) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE02a_1")) {
					if ("Y".equals(appBean.LifeAssured_insurability.getString("LIFESTYLE02a_1"))) {
						LA_alcohol_unit_weekly = LA_alcohol_unit_weekly + appBean.LifeAssured_insurability.getInt("LIFESTYLE02a_2");
					}

					if ("Y".equals(appBean.LifeAssured_insurability.getString("LIFESTYLE02b_1"))) {
						LA_alcohol_unit_weekly = LA_alcohol_unit_weekly + appBean.LifeAssured_insurability.getInt("LIFESTYLE02b_2");
					}

					if ("Y".equals(appBean.LifeAssured_insurability.getString("LIFESTYLE02c_1"))) {
						LA_alcohol_unit_weekly = LA_alcohol_unit_weekly + appBean.LifeAssured_insurability.getInt("LIFESTYLE02c_2");
					}
				}
			}
			obj.alcoholUnitsPerWeekNO = Integer.toString(LA_alcohol_unit_weekly);

			//TOBACCO QUESTION
			obj.tobaccoQuestionIND = "N";

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE01")) {
					obj.tobaccoQuestionIND = appBean.LifeAssured_insurability.getString("LIFESTYLE01");

					if (appBean.LifeAssured_insurability.getString("LIFESTYLE01").equals("Y"))
						appBean.isPolicyPMED = true;
				}
			}

			//TOBACCO UNITS/DAY
			//sample: obj.tobaccoUnitsPerDayNO = "23";
			obj.tobaccoUnitsPerDayNO = "0";

			if (appBean.isFirstParty || appBean.isThirdParty) {
				if (appBean.LifeAssured_insurability.has("LIFESTYLE01b")) {
					obj.tobaccoUnitsPerDayNO = Integer.toString(appBean.LifeAssured_insurability.getInt("LIFESTYLE01b"));
				}
			}

			//PREVIOUS EXAM FOR INSURED
			obj.previousHealthExamQuestionIND = "N";

			//PREVIOUS EXAM REASON FOR IN
			obj.previousHealthExamReasonIND = "";

			//PREVIOUS EXAM RESULT FOR IN
			obj.previousHealthExamResultIND = "";

			//AGENT REPORT
			obj.agentReportIND = "Y";

			if (appBean.isLAPMED) {
				obj.healthQuestionIND = "Y";
			}


			objList.add(obj);
			return objList;
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}
			return objList;
		}
	}

	private void determineMedicalIND(ApplicationBean appBean) throws Exception {

		AgeCrossDateCal ageCrossDateCal = new AgeCrossDateCal();
		try {

			//Policy Level:
			//policy remark summary& medical indicator
			//IF:
			//	"Supervisor Approval Date" already exceeded "Age Crossed Date"
			//OR
			//	"Age Crossed Date" - "Supervisory Approval Date" =< 3 Calendar days
			String ageCrossDate = "";
			int passedDays = 0;
			boolean isCrossAge = false;


			if (appBean.isAPPROVE) {
				//riskCommenceDate = "2017-11-21";
				//LA_birthDate = "1993-8-29";

				//Signature_date = "2017-11-21";
				//				
				ageCrossDate = ageCrossDateCal.getAgeXDate(appBean.riskCommenceDate, appBean.ow.birthdate, false); 

				//				String a= "";
				//				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
				//				long diff = format.parse(approveDate).getTime() - format.parse(LA_birthDate).getTime();
				//				long diffDays = (diff / (24 * 60 * 60 * 1000)) + 3;
				//				long diffYears = diffDays / 365;
				//				int eSub_age = ((Number)diffYears).intValue();

				//				approveDate = "2017-11-17";
				//				LA_birthDate = "1987-5-18";

				//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 

				//				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				//				Calendar cal  = Calendar.getInstance();
				//				cal.setTime(df.parse(approveDate));

				//For testing
				//				LA_birthDate = "1987-4-21";
				//				approveDate = "2017-11-5";
				//				LA_issueage = 31;

				String[] apprDateParts = appBean.approveDate.split("-");
				Calendar apprDate = null;

				apprDate = new GregorianCalendar(Integer.parseInt(apprDateParts[0]), Integer.parseInt(apprDateParts[1])-1, Integer.parseInt(apprDateParts[2]));								
				apprDate.add(Calendar.DATE, 3);
				//
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String _approveDate3 = sdf.format(apprDate.getTime());
				//				 				
				int eSub_age = ageCrossDateCal.calAgeNearestBirthday(_approveDate3, appBean.la.birthdate);
				//						

				if (appBean.isFirstParty || (appBean.hasPPEPS && appBean.isThirdParty)) {
					if (eSub_age > appBean.la.issueAge) {
						appBean.isPolicyPMED = true;
						appBean.Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon."); 
					}
				}

				//approveDate- LA_birthDate				

				//Age Cross Date < Date of Submission
				//				if (ageCrossDate.compareTo(Signature_date) <= 0) {
				//				
				//				} else {
				//ageCrossDate = "2017-10-20";
				//approveDate = "2017-11-21";

				//					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
				//					
				//					ageCrossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, true); 
				//					
				//					long diff = format.parse(riskCommenceDate).getTime() - format.parse(ageCrossDate).getTime();
				//				
				//					long diffDays = diff / (24 * 60 * 60 * 1000);
				//					passedDays  = ((Number)diffDays).intValue();
				//													
				//					if (is_1stparty || (hasPPEPS && is_3rdparty)) {
				//						if (passedDays > 183) {
				//							isPolicyPMED = true;
				//							Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon."); 
				//						}  else { 
				//							String crossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, false);				
				//							long diff1 = format.parse(crossDate).getTime() - format.parse(approveDate).getTime();
				//							long diffDays1 = diff1 / (24 * 60 * 60 * 1000);
				//							passedDays  = ((Number)diffDays1).intValue();
				//							
				//							if (passedDays != -1 && passedDays <= 3) {
				//								isPolicyPMED = true;
				//								Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
				//							}
				//						}
				//					}
				//	}




				//				if (hasPPEPS && is_3rdparty) {					
				//					if (passedDays > 183) {
				//						isPolicyPMED = true;
				//						Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon."); 
				//						passedDays = 0;
				//					} else { 
				//						String crossDate = ageCrossDateCal.getAgeXDate(riskCommenceDate, LA_birthDate, false);				
				//						long diff1 = format.parse(crossDate).getTime() - format.parse(approveDate).getTime();
				//						long diffDays1 = diff1 / (24 * 60 * 60 * 1000);
				//						passedDays  = ((Number)diffDays1).intValue();
				//						
				//						if (passedDays != -1 && passedDays <= 14) {
				//							isPolicyPMED = true;
				//							Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
				//						}
				//					}
				//				}

			}


			//            if (appBean.isAPPROVE) {
			////                ageCrossDate = ageCrossDateCal.getAgeXDate(appBean.riskCommenceDate, appBean.la.birthdate);
			////                if (appBean.approveDate.compareTo(ageCrossDate) > 0) {
			////                    passedDays = 0;
			////                } else {
			////                    passedDays = getDaysPassed(appBean.approveDate, ageCrossDate);
			////                }
			////                if (passedDays != null && passedDays <= 3) {
			////                    appBean.isPolicyPMED = true;
			////                    appBean.Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
			////                }
			//                
			//                
			//                ageCrossDate = ageCrossDateCal.getAgeXDate(appBean.riskCommenceDate, appBean.la.birthdate, false); 
			//				
			//				if (appBean.approveDate.compareTo(ageCrossDate) > 0) {
			//					appBean.isPolicyPMED = true;
			//					appBean.Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon."); 
			//					passedDays = 0;
			//					isCrossAge = true;
			//				} else if (getDaysPassed(appBean.approveDate,ageCrossDate) <= 3) {
			//					appBean.isPolicyPMED = true;
			//					appBean.Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
			//					isCrossAge = true;
			//				}
			//							
			//				if (appBean.hasPPEPS && appBean.isThirdParty && !isCrossAge) {
			//					ageCrossDate = ageCrossDateCal.getAgeXDate(appBean.riskCommenceDate, appBean.ow.birthdate, false); 
			//					
			//					if (appBean.approveDate.compareTo(ageCrossDate) > 0) {
			//						passedDays = 0;
			//					} else if (getDaysPassed(appBean.approveDate, ageCrossDate) <= 3) {
			//						appBean.isPolicyPMED = true;
			//						appBean.Pol_remarks.addpolicyRemarks("eApp: Age crossed or will cross soon.");
			//					}
			//				}
			//            }

			//IF:
			//	Payor = "Others" AND
			//	Relationship of Payor to the Proposer  = "Others".
			//  Remark: the payor always stored at proposer
			boolean hasFundsDetails = false;

			if (appBean.Owner_declaration.has("FUND_SRC01")) {
				if (appBean.Owner_declaration.getString("FUND_SRC01").toUpperCase().equals("FOREIGN")) {
					appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
					appBean.isPolicyPMED = true;
					hasFundsDetails = true;
				}
			}

			//            if (appBean.Owner_declaration.has("FUND_SRC02") && ("OTHER".equals(appBean.Owner_declaration.getString("FUND_SRC02").toUpperCase()))) {
			//                if (appBean.Owner_declaration.has("FUND_SRC09") && ("OTH".equals(appBean.Owner_declaration.getString("FUND_SRC09").toUpperCase()))) {
			//                    appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
			//                    appBean.isPolicyPMED = true;
			//                    hasFundsDetails = true;
			//                }
			//            } else if (appBean.Owner_declaration.has("FUND_SRC01") && (!"SG".equals(appBean.Owner_declaration.getString("FUND_SRC01").toUpperCase()))) {
			//            	appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
			//            	appBean.isPolicyPMED = true;
			//				hasFundsDetails = true;
			//			}

			//			if (appBean.Owner_declaration.has("FUND_SRC01") && (!"SG".equals(appBean.Owner_declaration.getString("FUND_SRC01").toUpperCase()))) {
			//            	appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
			//            	appBean.isPolicyPMED = true;
			//				hasFundsDetails = true;
			//			}

			if (appBean.Owner_declaration.has("FUND_SRC02") && ("OTHER".equals(appBean.Owner_declaration.getString("FUND_SRC02").toUpperCase()))) {
				if (appBean.Owner_declaration.has("FUND_SRC09") && ("OTH".equals(appBean.Owner_declaration.getString("FUND_SRC09").toUpperCase()))) {
					appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
					appBean.isPolicyPMED = true;
					hasFundsDetails = true;
				}
			}

			//Payor is LA with source of wealth = OTHERS
			if (!hasFundsDetails) {
				//String iCid = clientDoc.getString("cid");

				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(iCid)) {
				if (appBean.Owner_declaration.has("FUND_SRC30") && appBean.Owner_declaration.getString("FUND_SRC30").equals("N")) {
					if (appBean.Owner_declaration.has("FUND_SRC38") && appBean.Owner_declaration.getString("FUND_SRC38").equals("Y")) { 
						appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
						appBean.isPolicyPMED = true;	
						hasFundsDetails = true;
					}
				}	
				//} 
			}

			//Payor is PH with source of funds = OTHERS & source of wealth NOT = OTHERS
			if (!hasFundsDetails) {
				String pCid = appBean.proposerClientDoc.getString("cid");

				//Proposer
				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
				if (appBean.Owner_declaration.has("FUND_SRC30") && appBean.Owner_declaration.getString("FUND_SRC30").equals("Y")) {
					if (appBean.Owner_declaration.has("FUND_SRC38") && appBean.Owner_declaration.getString("FUND_SRC38").equals("N")) { 
						appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
						appBean.isPolicyPMED = true;	
						hasFundsDetails = true;
					}
				}
				//} 
			}

			//Payor is PH with source of wealth = OTHERS & source of funds NOT = OTHERS
			if (!hasFundsDetails) {
				String pCid = appBean.proposerClientDoc.getString("cid");							

				//Proposer
				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
				if (appBean.Owner_declaration.has("FUND_SRC38") && appBean.Owner_declaration.getString("FUND_SRC38").equals("Y")) { 
					if (appBean.Owner_declaration.has("FUND_SRC30") && appBean.Owner_declaration.getString("FUND_SRC30").equals("N")) {
						appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
						appBean.isPolicyPMED = true;	
						hasFundsDetails = true;
					}
				}
				//} 
			}

			//Payor is PH with source of wealth & source of funds = OTHERS
			if (!hasFundsDetails) {
				//String pCid = appBean.proposerClientDoc.getString("cid");							

				//Proposer
				//if (Owner_declaration.has("FUND_SRC02") && Owner_declaration.getString("FUND_SRC02").equals(pCid)) {
				if (appBean.Owner_declaration.has("FUND_SRC38") && appBean.Owner_declaration.getString("FUND_SRC38").equals("Y")) { 
					if (appBean.Owner_declaration.has("FUND_SRC30") && appBean.Owner_declaration.getString("FUND_SRC30").equals("Y")) {
						appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
						appBean.isPolicyPMED = true;	
						hasFundsDetails = true;
					}
				}
				//} 
			}


			//1st Party, payor = LA with Source of fund = OTHERS and source of wealth NOT = OTHERS
			//			if (!hasFundsDetails) {
			//				String iCid = appBean.clientDoc.getString("cid");							
			//				
			//				//Insured
			//				if (appBean.Owner_declaration.has("FUND_SRC02") && appBean.Owner_declaration.getString("FUND_SRC02").equals(iCid)) {
			//					if (appBean.Owner_declaration.has("FUND_SRC30") && appBean.Owner_declaration.getString("FUND_SRC30").equals("Y")) {
			//						if (appBean.Owner_declaration.has("FUND_SRC38") && appBean.Owner_declaration.getString("FUND_SRC38").equals("N")) { 
			//							appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
			//							appBean.isPolicyPMED = true;	
			//							hasFundsDetails = true;
			//						}
			//					}
			//				} 
			//			}


			//            if (hasFundsDetails == false) {
			//            	 if (appBean.Owner_declaration.has("FUND_SRC30")) {//Source of funds
			//     				if (appBean.Owner_declaration.get("FUND_SRC30").equals("Y")) {
			//     					appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
			//     					appBean.isPolicyPMED = true;
			//     				}
			//     			} else if (appBean.Owner_declaration.has("FUND_SRC38")) {//Source of Wealth
			//     				if (appBean.Owner_declaration.get("FUND_SRC38").equals("Y")) {
			//     					appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
			//     					appBean.isPolicyPMED = true;
			//     				}
			//     			}
			//            }

			//IF:
			//Source of Wealth = "Others" OR
			//Source of Funds = "Others"
			JSONObject Owner_declaration_PEP01_DATA = new JSONObject();
			try {
				if (appBean.Owner_declaration.has("PEP01_DATA")) {
					JSONArray Owner_declaration_PEP01_DATA_array = appBean.Owner_declaration.getJSONArray("PEP01_DATA");
					Owner_declaration_PEP01_DATA = Owner_declaration_PEP01_DATA_array.getJSONObject(0);
				}
			} catch (Exception PEP01_e0001) {

			}

			if (appBean.Owner_declaration.has("PEP01_DATA")) {
				if (Owner_declaration_PEP01_DATA.has("PEP01f6") && ("Y".equals(Owner_declaration_PEP01_DATA.getString("PEP01f6")))) {
					appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
					appBean.isPolicyPMED = true;
				}
				if (Owner_declaration_PEP01_DATA.has("PEP01e6") && ("Y".equals(Owner_declaration_PEP01_DATA.getString("PEP01e6")))) {
					appBean.Pol_remarks.addpolicyRemarks("eApp: Check source of funds details");
					appBean.isPolicyPMED = true;
				}
			}

			//Bankruptcy
			if (appBean.Owner_declaration.has("BANKRUPTCY01") && ("Y".equals(appBean.Owner_declaration.getString("BANKRUPTCY01")))) {
				appBean.Pol_remarks.addpolicyRemarks("eApp: Bankrupt declaration");
				appBean.isPolicyPMED = true;
			} else if (appBean.LifeAssured_declaration.has("BANKRUPTCY01") && ("Y".equals(appBean.LifeAssured_declaration.getString("BANKRUPTCY01")))) {
				appBean.Pol_remarks.addpolicyRemarks("eApp: Bankrupt declaration");
				appBean.isPolicyPMED = true;
			}

			//PEP
			if (appBean.Owner_declaration.has("PEP01") && ("Y".equals(appBean.Owner_declaration.getString("PEP01")))) {
				appBean.Pol_remarks.addpolicyRemarks("eApp: PEP declaration");
				appBean.isPolicyPMED = true;
			} else if (appBean.LifeAssured_declaration.has("PEP01") && ("Y".equals(appBean.LifeAssured_declaration.getString("PEP01")))) {
				appBean.Pol_remarks.addpolicyRemarks("eApp: PEP declaration");
				appBean.isPolicyPMED = true;
			}

			//Trusted Individual (if Proposer is a Selected Client )
			//            if (appBean.Owner_declaration.has("TRUSTED_IND01")) {
			//                if (!appBean.Owner_declaration.getString("TRUSTED_IND01").isEmpty()) {
			//                    appBean.Pol_remarks.addpolicyRemarks("VC=Y, TI=Y");
			//                    appBean.isPolicyPMED = true;
			//                }
			//            }

			//Do you have any pending or concurrent insurance applications? if this question is "Y", gen remark
			//eAPP: Concurrent submissions.
			if (appBean.Owner_policies.has("havPndinApp") && ("Y".equals(appBean.Owner_policies.getString("havPndinApp")))) {
				appBean.Pol_remarks.addpolicyRemarks("eAPP: Concurrent submissions.");
				appBean.isPolicyPMED = true;
			} else if (appBean.LifeAssured_policies.has("havPndinApp") && ("Y".equals(appBean.LifeAssured_policies.getString("havPndinApp")))) {
				appBean.Pol_remarks.addpolicyRemarks("eAPP: Concurrent submissions.");
				appBean.isPolicyPMED = true;
			}

			//Upload document:
			//supporting document
			if (appBean.applicationDoc.has("supportDocuments")) {
				JSONObject supportDocuments = appBean.applicationDoc.getJSONObject("supportDocuments");
				JSONObject supportDocuments_values = new JSONObject();

				if (supportDocuments.has("values")) {
					supportDocuments_values = supportDocuments.getJSONObject("values");
				}

				//LA's optional doc
				JSONObject supportDocuments_values_insured = supportDocuments_values.getJSONObject("insured");
				JSONObject supportDocuments_values_insured_optDoc = supportDocuments_values_insured.getJSONObject("optDoc");


				//proposer's optional doc
				JSONObject supportDocuments_values_proposer = supportDocuments_values.getJSONObject("proposer");
				JSONObject supportDocuments_values_proposer_optDoc = supportDocuments_values_proposer.getJSONObject("optDoc");

				//Other document:
				JSONObject supportDocuments_values_policyForm = supportDocuments_values.getJSONObject("policyForm");

				JSONObject supportDocuments_values_policyForm_otherDoc = new JSONObject();
				JSONObject supportDocuments_values_policyForm_otherDoc_values = new JSONObject();
				if (supportDocuments_values_policyForm.has("otherDoc")) {
					supportDocuments_values_policyForm.getJSONObject("otherDoc");
					if (supportDocuments_values_policyForm_otherDoc.has("values")) {
						supportDocuments_values_policyForm_otherDoc_values = supportDocuments_values_policyForm_otherDoc.getJSONObject("values");

					}
				}

				//LA's MedicalTest
				if (supportDocuments_values_insured_optDoc.has("iMedicalTest")) {
					JSONArray supportDocuments_values_insured_optDoc_iMedicalTest = supportDocuments_values_insured_optDoc.getJSONArray("iMedicalTest");
					if (supportDocuments_values_insured_optDoc_iMedicalTest.length() > 0) {
						for (int i = 0; i < supportDocuments_values_insured_optDoc_iMedicalTest.length(); i++) {
							JSONObject LA_medical_check_doc = supportDocuments_values_insured_optDoc_iMedicalTest.getJSONObject(i);
							if (LA_medical_check_doc.has("uploadDate")) {
								if (appBean.approveDate.equals(LA_medical_check_doc.getString("uploadDate").substring(0, 10))) {
									appBean.Pol_remarks.addpolicyRemarks("eAPP: Other docs uploaded");
									appBean.isPolicyPMED = true;
									break;
								}
							}
						}
					}
				}

				if (appBean.isThirdParty) {
					if (supportDocuments_values_proposer_optDoc.has("pMedicalTest")) {
						JSONArray supportDocuments_values_proposer_optDoc_pMedicalTest = supportDocuments_values_proposer_optDoc.getJSONArray("pMedicalTest");
						if (supportDocuments_values_proposer_optDoc_pMedicalTest.length() > 0) {
							for (int i = 0; i < supportDocuments_values_proposer_optDoc_pMedicalTest.length(); i++) {
								JSONObject OW_medical_check_doc = supportDocuments_values_proposer_optDoc_pMedicalTest.getJSONObject(i);
								if (OW_medical_check_doc.has("uploadDate")) {
									if (appBean.approveDate.equals(OW_medical_check_doc.getString("uploadDate").substring(0, 10))) {
										appBean.Pol_remarks.addpolicyRemarks("eAPP: Other docs uploaded");
										appBean.isPolicyPMED = true;
										break;
									}
								}
							}

						}
					}
				}

				if (supportDocuments_values_insured_optDoc.has("iJuvenileQuestions")) {
					JSONArray supportDocuments_values_insured_optDoc_iJuvenileQuestions = supportDocuments_values_insured_optDoc.getJSONArray("iJuvenileQuestions");
					if (supportDocuments_values_insured_optDoc_iJuvenileQuestions.length() > 0) {
						for (int i = 0; i < supportDocuments_values_insured_optDoc_iJuvenileQuestions.length(); i++) {
							JSONObject LA_medical_check_doc = supportDocuments_values_insured_optDoc_iJuvenileQuestions.getJSONObject(i);
							if (LA_medical_check_doc.has("uploadDate")) {
								if (appBean.approveDate.equals(LA_medical_check_doc.getString("uploadDate").substring(0, 10))) {
									appBean.Pol_remarks.addpolicyRemarks("eAPP: Other docs uploaded");
									appBean.isPolicyPMED = true;
									break;
								}
							}
						}
					}
				}

				boolean isdocfound = false;
				if (supportDocuments_values_policyForm_otherDoc.has("template")) {
					JSONArray supportDocuments_values_policyForm_otherDoc_template = supportDocuments_values_policyForm_otherDoc.getJSONArray("template");
					if (supportDocuments_values_policyForm_otherDoc_template.length() > 0) {
						for (int i = 0; i < supportDocuments_values_policyForm_otherDoc_template.length(); i++) {
							String otherDoc_id = (supportDocuments_values_policyForm_otherDoc_template.getJSONObject(i)).getString("id");
							JSONArray otherDoc_array = supportDocuments_values_policyForm_otherDoc.getJSONObject("values").getJSONArray(otherDoc_id);
							for (int j = 0; j < otherDoc_array.length(); j++) {
								JSONObject otherDoc_array_record = otherDoc_array.getJSONObject(j);
								if (otherDoc_array_record.has("uploadDate")) {
									if (appBean.approveDate.equals(otherDoc_array_record.getString("uploadDate").substring(0, 10))) {
										appBean.Pol_remarks.addpolicyRemarks("eAPP: Other docs uploaded");
										appBean.isPolicyPMED = true;
										isdocfound = true;
										break;
									}
								}
							}
							if (isdocfound) {
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (SubmissionUtil2.TESTING_FLAG) {
				e.printStackTrace();
			} else {
				throw new Exception(e);
			}            

		}
	}

	//Block messages that contain a single-quote ('), hash mark (#), or string (--) anywhere within the messagec
	private String truncateSpecialChar(String data) {
		return data.replace("&", "&amp;")
				.replace("#", "")
				.replace("--", "")
				.replace("'", "&apos;")
				.replace("\"", "&quot;")
				.replace("<", "&lt;")
				.replace(">", "&gt;");
	}

	//Remove space
	private String spaceRemover(String data) {
		String result = ""; 
		result = data.trim().replaceAll(" +", " ");

		return result;
	}

}
