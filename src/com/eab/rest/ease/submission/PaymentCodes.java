package com.eab.rest.ease.submission;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Log;

public class PaymentCodes {
	
	private String K_PaymentType;
	private String K_PlanCD;
	private String K_PaymentMethod;
	private String K_PayMode;
	private String K_GiroIND;
	private String R_ULChargeCD;	
	private String R_PaymentMethod;	
	private boolean isINDproduct = false;
	

	public void setPayKeys(String Key_PaymentType, String Key_PlanCD,String Key_PaymentMethod, String Key_PayMode,String Key_GiroIND) {
		this.K_PaymentType		= Key_PaymentType;
		this.K_PlanCD			= Key_PlanCD;
		this.K_PaymentMethod	= Key_PaymentMethod;
		this.K_PayMode			= Key_PayMode;
		this.K_GiroIND			= Key_GiroIND;
	}
	
	public void retrievePaymentRecord() {
			JSONObject payMapObject = null;
			payMapObject = Constant.CBUTIL.getDoc("RLSAPI_payment_relatedcode_Mapping");
				
			if (payMapObject == null) {
				Log.error("Error - RLSAPI_payment_relatedcode_Mapping file missing ");
			}
			
			JSONArray payMap_list = payMapObject.getJSONArray("options");
			JSONObject payMap_record	= null;
			String Cur_PaymentType 		= null;
			String Cur_PlanCD			= null;
			String Cur_PaymentMethod	= null;
			String Cur_PayMode			= null;
			String Cur_GiroIND			= null;


			if (payMap_list.length() > 0) 
			{

				for(int i = 0; i < payMap_list.length(); i++) {
					payMap_record 		= payMap_list.getJSONObject(i);
					Cur_PaymentType 	= payMap_record.getString("KPaymentType");
					Cur_PlanCD			= payMap_record.getString("KPlanCD");
					Cur_PaymentMethod	= payMap_record.getString("KPaymentMethod");
					Cur_PayMode			= payMap_record.getString("KPayMode");
					Cur_GiroIND			= payMap_record.getString("KGiroIND");
					
					if(Cur_PaymentType.equals(this.K_PaymentType)&&Cur_PlanCD.equals(this.K_PlanCD)&&Cur_PaymentMethod.equals(this.K_PaymentMethod)&&Cur_PayMode.equals(this.K_PayMode)&&Cur_GiroIND.equals(this.K_GiroIND))
					{
						this.isINDproduct = true;
						this.R_ULChargeCD = payMap_record.getString("ULChargeCD");
						this.R_PaymentMethod = payMap_record.getString("PaymentMethod");
						break;
					}
				}	
			}
			
	}
	
	public boolean isInspirePROD() {
		return isINDproduct;
	}
	
	public String getULChargeCD() {
		if(this.R_ULChargeCD != null){
			return this.R_ULChargeCD;
		}
		else {
			return "";
		}
		
	}
	
	public String getPaymentMethod() {
	if(this.R_ULChargeCD != null){
		return this.R_PaymentMethod;

	}
	else {
		return "";
	}
	
}

}
