package com.eab.rest.ease.submission;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.rest.ease.submission.applicationBean.ApplicationBean;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * To retrieve source data of Application including Application, Quotation, Client, Approval Objection.
 * And fill in the ApplicationBean Object
 */
class RetrieveApplicationUtil {


    void retreiveApplicationData(ApplicationBean appBean) throws Exception {

        //0. To derivate Json Object Nodes
        deriveJsonObjectNode(appBean);

        //1. First, get the Application JSON Object Document ID. And then get the application status
        appBean.appId = appBean.applicationDoc.getString("_id");
        appBean.applicationStatus = getApplicationStatus(appBean.bundleAppDoc, appBean.appId);

        //2. This is to retrieve the Sub Status of the application and it's event Date time
        getApplicationSubStatus(appBean);


        //3. The processing application must be signed
        //Sign date is getting from eApp applicationSignedDate

        if (appBean.applicationDoc.has("applicationSignedDate")) {
            try {
                appBean.signatureDate = appBean.applicationDoc.getString("applicationSignedDate").substring(0, 10);
                appBean.signatureTime = appBean.applicationDoc.getString("applicationSignedDate").substring(11, 19);

                if (appBean.signatureDate.isEmpty() || appBean.signatureTime.isEmpty()) {
                    throw new IOException("Signature date/time missing for processing application!");
                }
            } catch (Exception ex) {
            }
        }


        //4. set default value to APPROVE_REJECT_time to walk around
        getApprovalRejectionDetail(appBean);


        //5. retreive Payment Detail
        getPaymentDetail(appBean);

        //6. get personal information and application form data
        getPersonalDetail(appBean);

        
        //7. Get application information
        if (appBean.appForm_BasicPlan.has("planCode")) {
            appBean.basicPlanCode = appBean.appForm_BasicPlan.getString("planCode");
            if ("IN".equals((appBean.basicPlanCode).substring(0, 2))) {
                appBean.hasFundCharge = true;
            }
            if ("PAR".equals((appBean.basicPlanCode).substring(0, 3))) {
                appBean.isBandAid = true;
            }
        } else {
            throw new IOException("Mandatory field missing - basic plan code");
        }


    }

    /**
     * To derive Json Object Node from the initial Objects
     *
     * @param appBean
     * @throws Exception
     */
    private void deriveJsonObjectNode(ApplicationBean appBean) throws Exception {
        try {
        	//PolicyRemarks Pol_remarks = new PolicyRemarks();
        	
            //Get payment Json if any
        	JSONObject cpfisoaBlock = null;
    		JSONObject cpfissaBlock = null;
    		JSONObject srsBlock = null;
    		//String bankName = "";
    		
            if (appBean.applicationDoc.has("payment")) {
                appBean.payment = appBean.applicationDoc.getJSONObject("payment");
                
                if (appBean.payment.has("cpfisoaBlock"))
    				cpfisoaBlock = appBean.payment.getJSONObject("cpfisoaBlock");
    			
    			if (appBean.payment.has("cpfissaBlock"))
    				cpfissaBlock = appBean.payment.getJSONObject("cpfissaBlock");
    			
    			if (appBean.payment.has("srsBlock"))
    				srsBlock = appBean.payment.getJSONObject("srsBlock");
    			
    			//Process Fund Call on Specific Date
    			if (cpfisoaBlock != null) {
    				if(cpfisoaBlock.has("cpfisoaFundReqDate")) {
    					if (cpfisoaBlock.getString("cpfisoaFundReqDate").equals("Y")) {
    						appBean.hasFundReqDate = true;
    						appBean.Pol_remarks.addpolicyRemarks("EAPP: Process Fund Call on Specific Date");
    					} else {
    						appBean.Pol_remarks.addpolicyRemarks("EAPP: CPF a/c details received, Process Fund Call");
    					}	
    				} else if (cpfisoaBlock.has("cpfisoaAcctDeclare")) {
    					if (cpfisoaBlock.getString("cpfisoaAcctDeclare").equals("Y"))
    						appBean.Pol_remarks.addpolicyRemarks("CPF a/c opening form pending for submission at AXA Office");
    					else {
    						appBean.Pol_remarks.addpolicyRemarks("Client will submit CPF Account Opening form directly to the Bank.");
    						appBean.Pol_remarks.addpolicyRemarks("To require SuppForm to indicate CPF Account Number and Bank Name.");
    					}
    				}
    				
    				if(cpfisoaBlock.has("cpfisoaBankNamePicker"))
    					appBean.bankName = cpfisoaBlock.getString("cpfisoaBankNamePicker");
    			}
    			
    			if (cpfissaBlock != null) {
    				if(cpfissaBlock.has("cpfissaFundReqDate")) {
    					if (cpfissaBlock.getString("cpfissaFundReqDate").equals("Y")) {
    						appBean.hasFundReqDate = true;
    						appBean.Pol_remarks.addpolicyRemarks("EAPP: Process Fund Call on Specific Date");
    					} else {
    						appBean.Pol_remarks.addpolicyRemarks("EAPP: CPF a/c details received, Process Fund Call");
    					}
    				}
    				
    				if(cpfissaBlock.has("cpfissaBankNamePicker"))
    					appBean.bankName = cpfissaBlock.getString("cpfissaBankNamePicker");
    			}
    			
    			if (srsBlock != null) {
    				if(srsBlock.has("srsFundReqDate")) {
    					if (srsBlock.getString("srsFundReqDate").equals("Y")) {
    						appBean.hasFundReqDate = true;	
    						appBean.Pol_remarks.addpolicyRemarks("EAPP: Process Fund Call on Specific Date");
    					} else {
    						appBean.Pol_remarks.addpolicyRemarks("EAPP: CPF a/c details received, Process Fund Call");
    					}
    				} else if (srsBlock.has("srsAcctDeclare")) {
    					if (srsBlock.getString("srsAcctDeclare").equals("Y"))
    						appBean.Pol_remarks.addpolicyRemarks("CPF a/c opening form pending for submission at AXA Office");
    					else {
    						appBean.Pol_remarks.addpolicyRemarks("Client will submit CPF Account Opening form directly to the Bank.");
    						appBean.Pol_remarks.addpolicyRemarks("To require SuppForm to indicate CPF Account Number and Bank Name.");
    					}
    				}
    				
    				if(srsBlock.has("srsBankNamePicker"))
						appBean.bankName = srsBlock.getString("srsBankNamePicker");
    			}
            }

            //Get Quotation Objects from Application
            appBean.quotation = appBean.applicationDoc.getJSONObject("quotation");
            appBean.quotation_agent = appBean.quotation.getJSONObject("agent");
            appBean.quotation_policyOptions = appBean.quotation.getJSONObject("policyOptions");
            appBean.quotation_plans = appBean.quotation.getJSONArray("plans");
            appBean.quotation_basicPlan = null;

            if (appBean.quotation_plans.length() > 0) {
                appBean.quotation_basicPlan = appBean.quotation_plans.getJSONObject(0);
            }
            
    		if (appBean.app_plans.length() > 1) {
    			for (int i = 1; i < appBean.app_plans.length(); i++) {
    				JSONObject app = appBean.app_plans.getJSONObject(i);
    				
    				if(app.has("covCode")){
    					if ("PPEPS".equals(app.getString("covCode"))) {
    						appBean.hasPPEPS = true;	
    						break;
    					}
    				}
    			}
    		}


            appBean.appForm = appBean.applicationDoc.getJSONObject("applicationForm");
            appBean.appForm_basicValues = appBean.appForm.getJSONObject("values");
            appBean.appForm_basicValues_planDetails = appBean.appForm_basicValues.getJSONObject("planDetails");


            appBean.applicationForm_values_insured = appBean.appForm_basicValues.getJSONArray("insured");
            appBean.applicationForm_values_planDetails = appBean.appForm_basicValues.getJSONObject("planDetails");

            appBean.app_plans = appBean.applicationForm_values_planDetails.getJSONArray("planList");
            appBean.appForm_BasicPlan = null;

            if (appBean.app_plans.length() > 0) {
                appBean.appForm_BasicPlan = appBean.app_plans.getJSONObject(0);
            }


        } catch (Exception e) {
            if (SubmissionUtil2.TESTING_FLAG) {
                e.printStackTrace();
            } else {
                throw new Exception(e);
            }
        }
    }

    private void getPaymentDetail(ApplicationBean appBean) throws Exception {
        try {
            if (appBean.payment != null && appBean.payment.has("paymentMethod")) {
                appBean.paymentMethod = appBean.payment.getString("paymentMethod");
                if ("crCard".equals(appBean.paymentMethod) || "eNets".equals(appBean.paymentMethod) || "dbsCrCardIpp".equals(appBean.paymentMethod)) {
                    appBean.isCreditCardPayment = true;
                    appBean.isPaid = true;
                }

                if ("cpfisoa".equals(appBean.paymentMethod) || "cpfissa".equals(appBean.paymentMethod) || "srs".equals(appBean.paymentMethod)) {
                    appBean.isCPFPayment = true;
                }

                if ("cash".equals(appBean.paymentMethod)) {
                    appBean.isCashPayment = true;
                }

                if ("chequeCashierOrder".equals(appBean.paymentMethod)) {
                    appBean.isCheqPayment = true;
                }
            }
        } catch (Exception e) {
            if (SubmissionUtil2.TESTING_FLAG) {
                e.printStackTrace();
            } else {
                throw new Exception(e);
            }
        }
    }

    /**
     * This is to retrieve the Sub Status of the application and it's event Date time
     *
     * @param appBean
     * @throws Exception
     */
    private void getApplicationSubStatus(ApplicationBean appBean) throws Exception {
        try {
            if (StringUtils.isNotEmpty(appBean.applicationStatus)) {

                if ("INVALIDATED_SIGNED".equals(appBean.applicationStatus) && appBean.applicationDoc.getBoolean("isInitialPaymentCompleted")) {
                    if (appBean.invalidatedDate != null && appBean.invalidatedDate.isEmpty()) {
                        throw new IOException("Policy is INVALIDATED, while Date is missing");
                    } else {
                        if (appBean.bundleAppDoc.has("applications")) {

                            JSONArray applicationsArray = appBean.bundleAppDoc.getJSONArray("applications");
                            JSONObject applicationJson = new JSONObject();

                            for (int i = 0; i < applicationsArray.length(); i++) {
                                applicationJson = applicationsArray.getJSONObject(i);

                                if (applicationJson.has("applicationDocId")) {
                                    if (applicationJson.getString("applicationDocId").equals(appBean.appId)) {

                                        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                        //LocalDateTime date = LocalDateTime.parse((applicationJson.getLong("invalidateDate"), formatter);

                                        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(applicationJson.getLong("invalidateDate")), TimeZone.getDefault().toZoneId());

                                        LocalDateTime _date = date.plusHours(8);

                                        appBean.invalidatedDate = getFormattedDateType1(_date);
                                        //INVALIDATED_date = bundleAppDoc.getString("invalidateDate").substring(0, 10);

                                        appBean.invalidatedTime = getFormattedTimeType1(_date);
                                        //INVALIDATED_time = bundleAppDoc.getString("invalidateDate").substring(11,19);

                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if ("".equals(appBean.invalidatedDate) || "".equals(appBean.invalidatedTime)) {
                        throw new Exception("Policy is invalided, while Date/time is missing\"");
                    }
                    appBean.isINVALIDATED = true;
                }
            }
        } catch (Exception e) {
            if (SubmissionUtil2.TESTING_FLAG) {
                e.printStackTrace();
            } else {
                throw new Exception(e);
            }
        }
    }

    /**
     * Get Approval Rejection Detail such as Status and event data time
     *
     * @param appBean
     * @throws Exception
     */
    private void getApprovalRejectionDetail(ApplicationBean appBean) throws Exception {
        try {
            if (appBean.policyDoc != null && appBean.policyDoc.has("approvalStatus")) {
                appBean.approvalStatus = appBean.policyDoc.getString("approvalStatus"); //status: "A" - approved, "R" - Rejected, "E" - expired

                if ("A".equals(appBean.approvalStatus) || "R".equals(appBean.approvalStatus)) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    LocalDateTime date = LocalDateTime.parse(appBean.policyDoc.getString("approveRejectDate"), formatter);
                    LocalDateTime _date = date.plusHours(8);

                    appBean.approveRejectDate = getFormattedDateType1(_date);
                    appBean.approveRejectTime = getFormattedTimeType1(_date);

                    if (appBean.approveRejectDate.isEmpty() || appBean.approveRejectTime.isEmpty())
                        throw new IOException("Policy is approved/Rejected, while Date/time is missing");

                    appBean.isAPPROVE_REJECT = true;

                    if ("A".equals(appBean.approvalStatus)) {
                        appBean.isAPPROVE = true;
                        appBean.approveDate = appBean.approveRejectDate;
                    } else {
                        appBean.isREJECT = true;
                    }
                } else if ("E".equals(appBean.approvalStatus)) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    LocalDateTime date = LocalDateTime.parse(appBean.policyDoc.getString("expiredDate"), formatter);
                    LocalDateTime _date = date.plusHours(8);

                    appBean.expiredDate = getFormattedDateType1(_date);
                    appBean.expiredTime = getFormattedTimeType1(_date);

                    if (appBean.expiredDate.isEmpty() || appBean.expiredTime.isEmpty())
                        throw new IOException("Policy is expired, while Date/time is missing");

                    appBean.isEXPIRED = true;
                }
            }
        } catch (Exception e) {
            if (SubmissionUtil2.TESTING_FLAG) {
                e.printStackTrace();
            } else {
                throw new Exception(e);
            }
        }

    }

    /**
     * Lookup Application Status by appId
     *
     * @param bundleAppDoc
     * @param appId
     * @return
     */
    private String getApplicationStatus(JSONObject bundleAppDoc, String appId) throws Exception {
        String applicationStatus = null;
        try {

            if (bundleAppDoc.has("applications")) {
                JSONArray apps = bundleAppDoc.getJSONArray("applications");

                for (int i = 0; i < apps.length(); i++) {
                    JSONObject app = apps.getJSONObject(i);
                    if (app.has("applicationDocId")) {
                        if (appId.equals(app.getString("applicationDocId"))) {
                            applicationStatus = app.getString("appStatus");
                            break;
                        }
                    }
                }
            }
            return applicationStatus;
        } catch (Exception e) {
            if (SubmissionUtil2.TESTING_FLAG) {
                e.printStackTrace();
            } else {
                throw new Exception(e);
            }
            return applicationStatus;
        }
    }


    /**
     * Get Personal Information and determine first or third party case
     *
     * @param appBean
     */
    private void getPersonalDetail(ApplicationBean appBean) throws Exception {
        try {



            if (appBean.applicationForm_values_insured.length() > 0) {
                appBean.LifeAssured = appBean.applicationForm_values_insured.getJSONObject(0);

                appBean.isThirdParty = true;
            } else {
                appBean.LifeAssured = appBean.appForm_basicValues.getJSONObject("proposer");
                appBean.isFirstParty = true;
            }

            if (appBean.appForm_basicValues.has("proposer")) {
                appBean.Owner = appBean.appForm_basicValues.getJSONObject("proposer");
            } else {
                appBean.Owner = new JSONObject();
            }

            //personal information Owner section
            if (appBean.Owner.has("personalInfo")) {
                appBean.Owner_personalInfo = appBean.Owner.getJSONObject("personalInfo");
            } else {
                appBean.Owner_personalInfo = new JSONObject();
            }

            if (appBean.isThirdParty) {
                if (appBean.Owner_personalInfo.has("age")) {
                    appBean.ow.issueAge = appBean.Owner_personalInfo.getInt("age");
                }
                if (appBean.Owner_personalInfo.has("gender")) {
                    appBean.ow.gender = appBean.Owner_personalInfo.getString("gender");
                } else {
                    throw new Exception("owner gender missing");
                }
            }

            if (appBean.Owner.has("residency")) {
                appBean.Owner_residency = appBean.Owner.getJSONObject("residency");
            } else {
                appBean.Owner_residency = new JSONObject();
            }

            if (appBean.Owner.has("insurability")) {
                appBean.Owner_insurability = appBean.Owner.getJSONObject("insurability");
            } else {
                appBean.Owner_insurability = new JSONObject();
            }
            
            if (appBean.Owner_insurability.has("HW02")) {
                appBean.ow.weight = appBean.Owner_insurability.getDouble("HW02");
            }

            if (appBean.Owner.has("policies")) {
                appBean.Owner_policies = appBean.Owner.getJSONObject("policies");
            } else {
                appBean.Owner_policies = new JSONObject();
            }

            if (appBean.Owner.has("declaration")) {
                appBean.Owner_declaration = appBean.Owner.getJSONObject("declaration");
            } else {
                appBean.Owner_declaration = new JSONObject();
            }
            



            //personal info common field Section. S
            appBean.LifeAssured_personalInfo = appBean.LifeAssured.getJSONObject("personalInfo");

            //Get Life Assured Name  (By Bernard)
            if (appBean.LifeAssured_personalInfo.has("lastName")) {
                appBean.la.lastName = appBean.LifeAssured_personalInfo.getString("lastName");
            }
            if (appBean.LifeAssured_personalInfo.has("firstName")) {
                appBean.la.firstName = appBean.LifeAssured_personalInfo.getString("firstName");
            }
            if (appBean.LifeAssured_personalInfo.has("fullName")) {
                appBean.la.fullName = appBean.LifeAssured_personalInfo.getString("fullName");
            }

            if (appBean.LifeAssured_personalInfo.has("hanyuPinyinName")) {
                appBean.la.chineseName = appBean.LifeAssured_personalInfo.getString("hanyuPinyinName");
            }

            if (appBean.LifeAssured_personalInfo.has("othName")) {
                appBean.la.otherName = appBean.LifeAssured_personalInfo.getString("othName");
            }


            if (appBean.LifeAssured_personalInfo.has("dob")) {
                appBean.la.birthdate = appBean.LifeAssured_personalInfo.getString("dob");
            }
            

            if ("".equals(appBean.la.birthdate)) {
                throw new IOException("Life Assured birth date missing");
            }

            
            if (appBean.LifeAssured_personalInfo.has("nationality")) {
                appBean.la.nationality = appBean.LifeAssured_personalInfo.getString("nationality");
                appBean.la.nationalityMapped = nationalityConversion(appBean.LifeAssured_personalInfo.getString("nationality"), false);
            }
//            if (appBean.LifeAssured_personalInfo.has("age")) {
//                appBean.la.issueAge = appBean.LifeAssured_personalInfo.getInt("age");
//            }
            
//			if (LifeAssured_personalInfo.has("age")) {
//			LA_issueage = LifeAssured_personalInfo.getInt("age");
//		}
			if (appBean.quotation.has("iAge")) {
				 appBean.la.issueAge = appBean.quotation.getInt("iAge");
			}
	            
            
            if (appBean.LifeAssured_personalInfo.has("occupation")) {
                appBean.la.occupation = appBean.LifeAssured_personalInfo.getString("occupation");
            }
            if (appBean.LifeAssured_personalInfo.has("gender")) {
                appBean.la.gender = appBean.LifeAssured_personalInfo.getString("gender");
            }
            
            //personal info commom field LASection. E

            if (appBean.LifeAssured.has("insurability")) {
                appBean.LifeAssured_insurability = appBean.LifeAssured.getJSONObject("insurability");
            } else {
                appBean.LifeAssured_insurability = new JSONObject();
            }

            appBean.LifeAssured_residency = appBean.LifeAssured.getJSONObject("residency");
            appBean.LifeAssured_policies = appBean.LifeAssured.getJSONObject("policies");
            appBean.LifeAssured_declaration = appBean.LifeAssured.getJSONObject("declaration");


            if (appBean.Owner_personalInfo.has("mAddrCountry") && (!("".equals(appBean.Owner_personalInfo.getString("mAddrCountry"))))) {
                appBean.ismAddrdifferent = true;
            }

            appBean.LifeAssured_personalInfo_dependants = appBean.LifeAssured_personalInfo.getJSONArray("dependants");            

            if (appBean.LifeAssured_personalInfo_dependants.length() > 0) {
            	//appBean.proposer_dependant = appBean.LifeAssured_personalInfo_dependants.getJSONObject(0);
            	
            	for(int i = 0; i < appBean.LifeAssured_personalInfo_dependants.length(); i++) {
    				if (appBean.Owner_personalInfo.get("cid").equals(appBean.LifeAssured_personalInfo_dependants.getJSONObject(i).get("cid"))){
    					appBean.proposer_dependant = appBean.LifeAssured_personalInfo_dependants.getJSONObject(i);
    					break;
    				}
    			}	
            }
        } catch (Exception e) {
            if (SubmissionUtil2.TESTING_FLAG) {
                e.printStackTrace();
            } else {
                throw new Exception(e);
            }
        }
    }

    /**
     * To conver the nationality code from mapping
     *
     * @param Nationality_code
     * @return
     */
    private String nationalityConversion(String Nationality_code , boolean isConnectedParty) {
        //Get nationality json file - S
        JSONObject nationalityList = null;
        nationalityList = Constant.CBUTIL.getDoc("nationality");

        if (nationalityList == null) {
            Log.error("Error - nationality file missing ");
        }

        JSONArray nationality_list = nationalityList.getJSONArray("options");
		JSONObject nationality_record= null;
		JSONObject nationality_title = null;
		String nationality_option = null;
		String nationality_countryNM = null;
		String nationality_countryNM_RLS = null;

        if ((nationality_list.length() > 0) && (!Nationality_code.isEmpty()) ){
			for(int i = 0; i < nationality_list.length(); i++) {
				nationality_record = nationality_list.getJSONObject(i);
				nationality_option = nationality_record.getString("value");
			    
				if (nationality_option.equals(Nationality_code)) {
			    	nationality_title = nationality_record.getJSONObject("title");
			    	nationality_countryNM = nationality_title.getString("en");
			    	nationality_countryNM_RLS = nationality_record.getString("rlsCode");
			    	break;
			    }
			}	
		}

        if (isConnectedParty) {
			if (nationality_countryNM_RLS == null) {
				return "";
			} else {
				return nationality_countryNM_RLS;
			}
		} else {
			if (nationality_countryNM == null) {
				return "";
			} else {
				return nationality_countryNM;
			}
		}
    }
    
    /**
     * Get Date String in format of YYYY-MM-DD
     * @param date
     * @return
     */
    private String getFormattedDateType1(LocalDateTime date) {
    	String formattedDate = "";
    	formattedDate = date.getYear() + "-" + String.format("%02d", date.getMonthValue()) + "-" + String.format("%02d", date.getDayOfMonth());
    	return formattedDate;
    	
    }
    
    /**
     * Get Date String in format of hh:mi:ss
     * @param date
     * @return
     */
    private String getFormattedTimeType1(LocalDateTime date) {
    	String formattedTime = "";
    	formattedTime = String.format("%02d", date.getHour()) + ":" + String.format("%02d", date.getMinute()) + ":" +  String.format("%02d", date.getSecond());
    	return formattedTime;
    	
    }


}
