package com.eab.rest.ease.submission;

import javax.ws.rs.core.MediaType;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;

@Path("/submitInvalidApp")
public class SubmissionInvalidatedApp {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{polNum}")
	public Response submitInvalidApp(@Context HttpHeaders headers, @PathParam("polNum") String polNum) {
		Response output = null;
		String audID = headers.getRequestHeader("aud-id").get(0);

		try {
			Log.debug("Start insert to Batch_Submission_request table");
			
			BATCH_SUBMISSION_REQUEST bSubReq = new BATCH_SUBMISSION_REQUEST();
			boolean result = bSubReq.create(polNum, "Invalidated Paid", 0, "R");  // ready
			
			if (result) {
				output = RestUtil.toResponse(audID, Response.Status.ACCEPTED, "{\"success\":true}");
			} else {
				output = RestUtil.toResponse(audID, Response.Status.ACCEPTED, "{\"success\":false}");
			}
			
		} catch (Exception ex) {
			Log.error(ex);
			output = RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, RestUtil.genErrorJsonStr(false, ex.getMessage()));
		}
		return output;
	}
	
}
