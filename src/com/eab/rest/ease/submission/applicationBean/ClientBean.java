package com.eab.rest.ease.submission.applicationBean;

/**
 * The model (bean) object posses customer personal information
 */
public class ClientBean {

    public String id = "";
    public String fullName = "";
    public String lastName = "";
    public String firstName = "";
    public String otherName = "";
    public String chineseName = ""; // which is the Han Yu Pin Yin Name
    public String documentId = ""; // which is the Identity Number like NRIC and HKID
    public String birthdate = "";
    public String nationality = "";
    public String nationalityMapped = "";

    public String gender = "";
    public String occupation = "";

    public Integer issueAge = 0;
    public Double weight = 0.0;    
    public Double height = 0.0;
    public String countryOfResidenceCD = "";
    
}
