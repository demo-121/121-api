package com.eab.rest.ease.submission.model;

import java.util.List;

public class CanBeIndividualItemType {

    public String height;
    public String weight;
    public String annualIncomeAMT;
    public String firstNM;
    public String lastNM;
    public String chineseNM;
    public String genderCD;
    public String maritalStatusCD;
    public String birthDT;
    public String countryOfResidenceCD;
    public String passportNo;
    //public String ownershipFLG;
    public String documentID;
    public String checkDocumentID;
    public String stdOccupationCD;
    public String industryCD;
    public String occupationClassCd;
    public String citizenCD;
    public String religionCD;
    public String countryOfOriginDESC;
    public String nationality;
    public String occupation;
    public String placeOfBirth;
    public List<HasIndividualUWInfoIn> hasIndividualUWInfoIn;
    public List<HasAddressInfo> hasAddressesIn;
    //	public HasCRSInformationIn hasCRSInformationIn;

    public CanBeIndividualItemType() {
        //		this.hasCRSInformationIn = new HasCRSInformationIn();
    }
}