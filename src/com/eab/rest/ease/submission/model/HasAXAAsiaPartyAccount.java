package com.eab.rest.ease.submission.model;

public class HasAXAAsiaPartyAccount {

    public String payerID;
    public String mandateStatusCD;
    public String payorLastNM;
    public String accountNO;
    public String creditCardNO;
    public String cardExpirationDT;
    public String accountBankCD;
    public String payorFirstNM;
    public String branchCD;
    public String cpfBankEffectiveDT;
    public String cpfBankAbbr;
    public String cpfINVAccountNO;
    public String bankID;
    public String status;
    public String processDT;
    public String amalgametedAccountNO;
    public String accountTypeCD;

    public HasAXAAsiaPartyAccount() {
    }
}