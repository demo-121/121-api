package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasAXAAsiaPolicyPaymentAccount {

    public String paymentTypeCD;
    public String statusCD;
    public String bankNO;
    public String branchNO;
    public String validFromDTTM;
    public String factoringHouseCD;
    public String bankIssuerNM;
    public String authProcessDT;
    public String sectionCD;
    public String modDT;
    public String paymentMethodChangeFLG;
    public List<HasAXAAsiaPartyAccount> hasAXAAsiaPartyAccount;

    public HasAXAAsiaPolicyPaymentAccount() {
    }
}

