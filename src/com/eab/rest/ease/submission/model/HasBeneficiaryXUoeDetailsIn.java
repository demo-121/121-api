package com.eab.rest.ease.submission.model;

public class HasBeneficiaryXUoeDetailsIn {

    public String relationshipToInsuredCD;
    public String benefitDistributionPCT;
    public String beneficiaryTypeCD;

    public HasBeneficiaryXUoeDetailsIn() {
    }
}