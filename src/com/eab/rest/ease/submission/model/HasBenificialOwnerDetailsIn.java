package com.eab.rest.ease.submission.model;

import java.util.Date;
import java.util.List;

public class HasBenificialOwnerDetailsIn {

    public String ownershipFLG = "C"; //default
    public String sharesPCT;
    public String insurableInterestAMT;
    public String monthlyIncomeAMT;
    public String sourceFundDTL;
    public String additionalIncomeAMT;
    public String purposeDESC;
    public String totMonAddIncmAMT;
    public String positionDESC;
    public String registrationLetterTXT;
    public String assigneeLastNM;
    public String assigneeFirstNM;
    public String powerAttorneyDESC;
    public String domRegistrationNO;
    public String comRegistrationNO;
    public String deedEstdDT;
    public Date decreeLetterDT;
    public String comWebsiteTXT;
    public String businessEntityNM;
    public String totalComAssetAMT;
    public String relationshipToInsuredCD;
    public List<CanBeIndividualReference> canBeIndividual;

    public HasBenificialOwnerDetailsIn() {
    }
}
