package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasCoverageDetailsIn {

    public String baseCoverageIndicatorCd;
    public List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProduct;

    public HasCoverageDetailsIn() {
    }
}
