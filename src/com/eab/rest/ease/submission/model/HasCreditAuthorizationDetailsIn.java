package com.eab.rest.ease.submission.model;

import java.util.Date;

public class HasCreditAuthorizationDetailsIn {

    public Date caEffectiveDT;
    public String status;
    public String accountHolderFullName;
    public String bankID;
    public String branchCD;
    public String accountNO;
    public String limit;
    public String accountBankCD;
    public Date processDT;
    public String subTransactionNO;

    public HasCreditAuthorizationDetailsIn() {
    }
}