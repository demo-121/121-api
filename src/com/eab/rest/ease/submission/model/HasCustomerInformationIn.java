package com.eab.rest.ease.submission.model;

import java.util.List;

/**
 * Identifies the details of the owner
 */
public class HasCustomerInformationIn {

    public String ownerIND;
    public String smsTextableFlg;
    public String dependentNO;
    public String customerID;
    public String familyMembersNO;
    public List<String> prevPolicyNO;
    public String documentID;
    public String documentTypeCD;
    public CanBeIndividual canBeIndividual;
    public HasRiskRatingIn hasRiskRatingIn;

    public HasCustomerInformationIn() {
        this.canBeIndividual = new CanBeIndividual();
        this.hasRiskRatingIn = new HasRiskRatingIn();
    }
}
