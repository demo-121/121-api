package com.eab.rest.ease.submission.model;

public class HasDetailsOfPolicyIn {

    public String dvdoIND;
    public String uwRemark1;
    public String cpfAcctTypeCD;
    public String rtuPaymentMethodCD;
    public String rtuPaymentModeCD;
    public String dboIND;
    public String ownerAgeAtIssue;

    public HasDetailsOfPolicyIn() {
    }
}