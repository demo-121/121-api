package com.eab.rest.ease.submission.model;

import java.util.List;

/**
 * Identifies the details of the insured.
 */
public class HasDetailsOfRisksIn {

    public String issueAge;
    public String relationshipToOwnerCD;
    public List<String> insuredPrevPolicyNO;
    public List<HasPersonalDetailsIn> hasPersonalDetailsIn;
    public HasUnderwritingResultsIn hasUndewritingResultsIn;

    public HasDetailsOfRisksIn() {
        this.hasUndewritingResultsIn = new HasUnderwritingResultsIn();
    }
}