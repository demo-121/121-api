package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasIndividualUWInfoIn {

    public String declineQuestionIND;
    public String avocationQuestionIND;
    public String healthQuestionIND;
    public String previousHealthExamQuestionIND;
    public String previousHealthExamReasonIND;
    public String previousHealthExamResultIND;
    public String alcoholQuestionIND;
    public String alcoholUnitsPerWeekNO;
    public String tobaccoQuestionIND;
    public String tobaccoUnitsPerDayNO;
    public String smokingQuestionIND;
    public String cigarettesPerDayNO;
    public List<String> previousSubStdPNO;
    public String ePolicyDeliveryFLG;
    public String customerProtectionDeclarationIND;
    public String agentReportIND;
    public String residenceDeclCD;
    public String trustDeclarationIND;

    public HasIndividualUWInfoIn() {
    }

    public HasIndividualUWInfoIn(String declineQuestionIND, String avocationQuestionIND,
                                 String healthQuestionIND, String previousHealthExamQuestionIND, String previousHealthExamReasonIND,
                                 String previousHealthExamResultIND, String alcoholQuestionIND, String alcoholUnitsPerWeekNO,
                                 String tobaccoQuestionIND, String tobaccoUnitsPerDayNO, String smokingQuestionIND,
                                 String cigarettesPerDayNO, List<String> previousSubStdPNO, String ePolicyDeliveryFLG) {

        this.declineQuestionIND = declineQuestionIND;
        this.avocationQuestionIND = avocationQuestionIND;
        this.healthQuestionIND = healthQuestionIND;
        this.previousHealthExamQuestionIND = previousHealthExamQuestionIND;
        this.previousHealthExamReasonIND = previousHealthExamReasonIND;
        this.previousHealthExamResultIND = previousHealthExamResultIND;
        this.alcoholQuestionIND = alcoholQuestionIND;
        this.alcoholUnitsPerWeekNO = alcoholUnitsPerWeekNO;
        this.tobaccoQuestionIND = tobaccoQuestionIND;
        this.tobaccoUnitsPerDayNO = tobaccoUnitsPerDayNO;
        this.smokingQuestionIND = smokingQuestionIND;
        this.cigarettesPerDayNO = cigarettesPerDayNO;
        this.previousSubStdPNO = previousSubStdPNO;
        this.ePolicyDeliveryFLG = ePolicyDeliveryFLG;
    }

    public String getDeclineQuestionIND() {
        return declineQuestionIND;
    }

    public String getAvocationQuestionIND() {
        return avocationQuestionIND;
    }

    public String getHealthQuestionIND() {
        return healthQuestionIND;
    }

    public String getPreviousHealthExamQuestionIND() {
        return previousHealthExamQuestionIND;
    }

    public String getPreviousHealthExamReasonIND() {
        return previousHealthExamReasonIND;
    }

    public String getPreviousHealthExamResultIND() {
        return previousHealthExamResultIND;
    }

    public String getAlcoholQuestionIND() {
        return alcoholQuestionIND;
    }

    public String getAlcoholUnitsPerWeekNO() {
        return alcoholUnitsPerWeekNO;
    }

    public String getTobaccoQuestionIND() {
        return tobaccoQuestionIND;
    }

    public String getTobaccoUnitsPerDayNO() {
        return tobaccoUnitsPerDayNO;
    }

    public String getSmokingQuestionIND() {
        return smokingQuestionIND;
    }

    public String getCigarettesPerDayNO() {
        return cigarettesPerDayNO;
    }

    public List<String> getPreviousSubStdPNO() {
        return previousSubStdPNO;
    }

    public String getEPolicyDeliveryFLG() {
        return ePolicyDeliveryFLG;
    }

    public void setDeclineQuestionIND(String declineQuestionIND) {
        this.declineQuestionIND = declineQuestionIND;
    }

    public void setAvocationQuestionIND(String avocationQuestionIND) {
        this.avocationQuestionIND = avocationQuestionIND;
    }

    public void setHealthQuestionIND(String healthQuestionIND) {
        this.healthQuestionIND = healthQuestionIND;
    }

    public void setPreviousHealthExamQuestionIND(String previousHealthExamQuestionIND) {
        this.previousHealthExamQuestionIND = previousHealthExamQuestionIND;
    }

    public void setPreviousHealthExamReasonIND(String previousHealthExamReasonIND) {
        this.previousHealthExamReasonIND = previousHealthExamReasonIND;
    }

    public void setPreviousHealthExamResultIND(String previousHealthExamResultIND) {
        this.previousHealthExamResultIND = previousHealthExamResultIND;
    }

    public void setAlcoholQuestionIND(String alcoholQuestionIND) {
        this.alcoholQuestionIND = alcoholQuestionIND;
    }

    public void setAlcoholUnitsPerWeekNO(String alcoholUnitsPerWeekNO) {
        this.alcoholUnitsPerWeekNO = alcoholUnitsPerWeekNO;
    }

    public void setTobaccoQuestionIND(String tobaccoQuestionIND) {
        this.tobaccoQuestionIND = tobaccoQuestionIND;
    }

    public void setTobaccoUnitsPerDayNO(String tobaccoUnitsPerDayNO) {
        this.tobaccoUnitsPerDayNO = tobaccoUnitsPerDayNO;
    }

    public void setSmokingQuestionIND(String smokingQuestionIND) {
        this.smokingQuestionIND = smokingQuestionIND;
    }

    public void setCigarettesPerDayNO(String cigarettesPerDayNO) {
        this.cigarettesPerDayNO = cigarettesPerDayNO;
    }

    public void setPreviousSubStdPNO(List<String> previousSubStdPNO) {
        this.previousSubStdPNO = previousSubStdPNO;
    }

    public void setEPolicyDeliveryFLG(String ePolicyDeliveryFLG) {
        this.ePolicyDeliveryFLG = ePolicyDeliveryFLG;
    }
}