package com.eab.rest.ease.submission.model;

public class HasLifePolicyTransactionHeaderDetailsIn {

    public String creditCardNO;
    public String bankAccountNO;
    public String cardExpirationDT;
    public String authenticationCD;
    public String paymentRemarks;
    public String cbrDT;
    public String bankCD;
    public String branchCD;
    public String chequeNO;
    public String chequeDT;
    public String bankFeeAMT;
    public String ccLocalAMT;

    public HasLifePolicyTransactionHeaderDetailsIn() {
    }
}
