package com.eab.rest.ease.submission.model;

public class HasRiskRatingIn {

    public String productRiskRatingLVL;
    public String customerRiskRatingLVL;
    public String geographicalRiskRatingLVL;
    public String regulatoryRiskRatingLVL;

    public HasRiskRatingIn() {
    }
}
