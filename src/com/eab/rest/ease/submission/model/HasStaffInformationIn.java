package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasStaffInformationIn {

    public String channelID;
    public String staffCD;
    public String staffIND;
    public List<HasBankReferrerInfoIn> hasBankReferrerInfoIn;

    public HasStaffInformationIn() {
    }
}