package com.eab.rest.ease.submission.model;

import java.util.Date;

public class HasUnderwritingDecisionsIn {

    public String underwritingRemarks;
    public Date underwritingDate;

    public HasUnderwritingDecisionsIn() {
    }
}