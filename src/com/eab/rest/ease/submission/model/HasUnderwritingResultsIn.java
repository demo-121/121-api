package com.eab.rest.ease.submission.model;

import java.util.List;

public class HasUnderwritingResultsIn {

    public String insurableInterestRT;
    public String signatureConsistanatFLG;
    public String salesIllustrationFLG;
    public String capturedID;
    public String financialUWResultFLG;
    public String largeCaseIND;
    public String largeCaseAMT;
    public String amlSubjectPlanDESC;
    public String caseAcceptanceFLG;
    public List<String> previousSubStdPNO;
    public List<String> remarksDESC;
    public String sourceID;
    public String splitCommissionAMT;

    public HasUnderwritingResultsIn() {
    }
}