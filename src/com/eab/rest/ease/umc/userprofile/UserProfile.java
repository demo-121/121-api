package com.eab.rest.ease.umc.userprofile;

public class UserProfile {

	private String userId;
	private String title;
	private String fullName;
	private String mobilePhone;
	private String officePhone;
	private String email;
	private String userRole;
	private String userCat;
	private String agentCode;
	private String faAdvisorRole;
	private String directorOrFirm;
	private String distribCode;
	private String unitCode;
	private String authGroup;
	private String userStatus;
	private String authorizedHI;
	private String authorizedM8;
	private String authorizedM8A;
	private String authorizedIFAST;
	private String rank;
	private String position;
	private String upline1Code;
	private String upline2Code;
	private String proxy1UserId;
	private String proxy2UserId;
	private String proxyStartDate;
	private String proxyEndDate;
	private String suspStartDate;
	private String suspEndDate;
	private String userType;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserCat() {
		return userCat;
	}

	public void setUserCat(String userCat) {
		this.userCat = userCat;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getFaAdvisorRole() {
		return faAdvisorRole;
	}

	public void setFaAdvisorRole(String faAdvisorRole) {
		this.faAdvisorRole = faAdvisorRole;
	}

	public String getDirectorOrFirm() {
		return directorOrFirm;
	}

	public void setDirectorOrFirm(String directorOrFirm) {
		this.directorOrFirm = directorOrFirm;
	}

	public String getDistribCode() {
		return distribCode;
	}

	public void setDistribCode(String distribCode) {
		this.distribCode = distribCode;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getAuthGroup() {
		return authGroup;
	}

	public void setAuthGroup(String authGroup) {
		this.authGroup = authGroup;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getAuthorizedHI() {
		return authorizedHI;
	}

	public void setAuthorizedHI(String authorizedHI) {
		this.authorizedHI = authorizedHI;
	}

	public String getAuthorizedM8() {
		return authorizedM8;
	}

	public void setAuthorizedM8(String authorizedM8) {
		this.authorizedM8 = authorizedM8;
	}

	public String getAuthorizedM8A() {
		return authorizedM8A;
	}

	public void setAuthorizedM8A(String authorizedM8A) {
		this.authorizedM8A = authorizedM8A;
	}

	public String getAuthorizedIFAST() {
		return authorizedIFAST;
	}

	public void setAuthorizedIFAST(String authorizedIFAST) {
		this.authorizedIFAST = authorizedIFAST;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getUpline1Code() {
		return upline1Code;
	}

	public void setUpline1Code(String upline1Code) {
		this.upline1Code = upline1Code;
	}

	public String getUpline2Code() {
		return upline2Code;
	}

	public void setUpline2Code(String upline2Code) {
		this.upline2Code = upline2Code;
	}

	public String getProxy1UserId() {
		return proxy1UserId;
	}

	public void setProxy1UserId(String proxy1UserId) {
		this.proxy1UserId = proxy1UserId;
	}

	public String getProxy2UserId() {
		return proxy2UserId;
	}

	public void setProxy2UserId(String proxy2UserId) {
		this.proxy2UserId = proxy2UserId;
	}

	public String getProxyStartDate() {
		return proxyStartDate;
	}

	public void setProxyStartDate(String proxyStartDate) {
		this.proxyStartDate = proxyStartDate;
	}

	public String getProxyEndDate() {
		return proxyEndDate;
	}

	public void setProxyEndDate(String proxyEndDate) {
		this.proxyEndDate = proxyEndDate;
	}

	public String getSuspStartDate() {
		return suspStartDate;
	}

	public void setSuspStartDate(String suspStartDate) {
		this.suspStartDate = suspStartDate;
	}

	public String getSuspEndDate() {
		return suspEndDate;
	}

	public void setSuspEndDate(String suspEndDate) {
		this.suspEndDate = suspEndDate;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
}
