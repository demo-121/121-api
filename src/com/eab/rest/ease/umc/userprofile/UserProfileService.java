package com.eab.rest.ease.umc.userprofile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_AGT_PROFILE_TRX;
import com.eab.dao.API_AGT_PROXY_HIST;
import com.eab.dao.LOOKUP_AGENT_RANK;
import com.eab.dao.USER_PROFILE;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class UserProfileService {

	private UserProfileValidator userProfileValidator = new UserProfileValidator();

	public Response validateContent(String audID, JsonObject bodyJson) throws Exception {
		Log.info("Goto UserProfileService.validateContent");

		Response output = null;

		JsonArray userResultJsonArray = new JsonArray(); // user result container

		if (bodyJson.has("people")) {

			JsonArray peopleJsonArray = bodyJson.getAsJsonArray("people"); // get user profile array

			boolean hasError = false; // any one error will cause bad request response 400

			for (JsonElement jsonElement : peopleJsonArray) {
				JsonObject agentJson = jsonElement.getAsJsonObject();
				if (agentJson != null) {

					String errorString = "";
					String userId = "";

					try {
						if (agentJson.has("userId"))
							userId = agentJson.get("userId").getAsString(); // Get userId

						errorString += userProfileValidator.validate(agentJson);
					} catch (Exception ex) {
						Log.error(ex);
						errorString += ex.getMessage();
					}

					boolean isFailed = errorString.length() > 0 ? true : false;

					String status = "SUCCESS";
					if (isFailed) {
						status = "FAILURE";
						hasError = true;
					} else {

						boolean updateCouchBaseResult = updateAgentRecordOnCouchBase(userId, agentJson); // shoot user
																											// profile
																											// data to
																											// couch
																											// base

						if (!updateCouchBaseResult) {
							errorString += "cannot be saved to CouchBase";
							status = "FAILURE";
							Log.error("ERROR - updateAgentRecordOnCouchBase FAIL");
						}

						boolean updateProxyRecordResult = updateProxyRecord(audID, userId, agentJson);

						if (!updateProxyRecordResult) {
							errorString += "cannot be saved to Proxy History";
							status = "FAILURE";
						}

						updateUserProfileRecord(agentJson); // Save user profile into DB

					}

					// Result of each User
					JsonObject userResultJson = new JsonObject();
					userResultJson = createResultNode(userId, status, errorString);

					// Add user result JSON to an array
					userResultJsonArray.add(userResultJson);
				}
			}

			if (hasError) {
				output = createFailureResponse(audID, userResultJsonArray);
			} else {
				output = createSuccessResponse(audID, userResultJsonArray);
			}

		} else {
			JsonObject userResultJson = new JsonObject();
			userResultJson = createResultNode("", "FAILURE", "missing people;");
			userResultJsonArray.add(userResultJson);
			output = createFailureResponse(audID, userResultJsonArray);
		}

		return output;
	}

	private UserProfile convertToUserProfile(JsonObject agentJson) {
		// TODO save to DB
		Gson gson = new Gson(); // convert JSON to be Java object
		UserProfile userProfile = gson.fromJson(agentJson, UserProfile.class);

		return userProfile;
	}

	private JsonObject createResultNode(String userId, String status, String message) {
		Log.info("Goto UserProfileService.createResultNode");

		// Result Json object in fixed structure
		JsonObject resultJson = new JsonObject();
		resultJson.addProperty("userId", userId);
		resultJson.addProperty("status", status);
		resultJson.addProperty("message", message);

		return resultJson;
	}

	private Response createSuccessResponse(String audID, JsonArray userResultJsonArray) {
		Log.info("Goto UserProfileService.createSuccessResponse");

		JsonObject responseJson = new JsonObject();
		responseJson.add("result", userResultJsonArray);

		return RestUtil.toResponse(audID, Response.Status.OK, responseJson.toString());
	}

	private Response createFailureResponse(String audID, JsonArray userResultJsonArray) {
		Log.info("Goto UserProfileService.createFailureResponse");

		JsonObject responseJson = new JsonObject();
		responseJson.add("result", userResultJsonArray);

		return RestUtil.toResponse(audID, Response.Status.BAD_REQUEST, responseJson.toString());
	}

	public Response createErrorResponse(String audID, String message) {
		Log.info("Goto UserProfileService.createErrorResponse");

		JsonObject resultJson = this.createResultNode("", "EXCEPTION", message);

		JsonArray resultJsonArray = new JsonArray();
		resultJsonArray.add(resultJson);

		JsonObject responseJson = new JsonObject();
		responseJson.add("result", resultJsonArray);

		return RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, responseJson.toString());
	}

	private boolean updateAgentRecordOnCouchBase(String userId, JsonObject agentJson) {
		Log.info("Goto UserProfileService.updateAgentRecordOnCouchBase");

		JSONObject existingAgentProfile = Constant.CBUTIL.getDoc("U_" + userId);

		boolean result = false;

		try {
			String revisedAgentJsonString = "";
			String resultDoc = "";

			if (existingAgentProfile != null) {

				// Update record
				revisedAgentJsonString = parseAgentJsonForCouchbase(agentJson, existingAgentProfile.toString());
				resultDoc = Constant.CBUTIL.UpdateDoc("U_" + userId, revisedAgentJsonString);
			} else {

				// Create record
				revisedAgentJsonString = parseAgentJsonForCouchbase(agentJson, null);
				resultDoc = Constant.CBUTIL.CreateDoc("U_" + userId, revisedAgentJsonString);
			}

			if (resultDoc.length() > 0) {
				result = true;
				Log.debug("updateAgentRecordOnCouchBase Succeed");
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		return result;
	}

	private String parseAgentJsonForCouchbase(JsonObject rawData, String existingAgentProfileString) throws Exception {
		Log.info("Goto UserProfileService.parserawData");
		
		JsonObject agentProfileForCouchBase = new JsonObject();

		// date
		String nowAsISO8601 = Function.getDateUTCNowStr();

		String createDateString = null;
		if (existingAgentProfileString != null) { // keep old data
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(existingAgentProfileString, JsonElement.class);
			JsonObject existingAgentProfile = jsonElement.getAsJsonObject();

			if (existingAgentProfile.has("createDate")) {
				createDateString = existingAgentProfile.get("createDate").getAsString();
			}
			
			if (existingAgentProfile.has("acceptDate")) {
				agentProfileForCouchBase.add("acceptDate", existingAgentProfile.get("acceptDate"));
			}
			
			if (existingAgentProfile.has("rejectDate")) {
				agentProfileForCouchBase.add("rejectDate", existingAgentProfile.get("rejectDate"));
			}
			
			if (existingAgentProfile.has("acceptTermVersion")) {
				agentProfileForCouchBase.add("acceptTermVersion", existingAgentProfile.get("acceptTermVersion"));
			}
		}
		
		if (createDateString != null) {
			agentProfileForCouchBase.addProperty("createDate", createDateString);
		} else {
			agentProfileForCouchBase.addProperty("createDate", nowAsISO8601);
		}
		
		agentProfileForCouchBase.addProperty("lastUpdateDate", nowAsISO8601);

		// authorised mapping
		JsonArray authorisedArray = new JsonArray();

		// default values for authorised field
		authorisedArray.add("M9A");
		authorisedArray.add("M9");
		authorisedArray.add("M5");

		String authorisedDesString = "Investment Linked Plan, Life Insurance";

		if (rawData.has("authorizedHI")) {
			if ("Y".equals(rawData.get("authorizedHI").getAsString())) {
				authorisedArray.add("HI");

				authorisedDesString += ", Health Insurance";
			}
		}

		// boolean isUnitTrusts = false;
		boolean isM8A = false;
		boolean isM8 = false;
		boolean isIFAST = false;

		if (rawData.has("authorizedM8A")) {
			if ("Y".equals(rawData.get("authorizedM8A").getAsString())) {
				authorisedArray.add("M8A");

				isM8A = true;
			}
		}

		if (rawData.has("authorizedM8")) {
			if ("Y".equals(rawData.get("authorizedM8").getAsString())) {
				authorisedArray.add("M8");

				isM8 = true;
			}
		}

		if (rawData.has("authorizedIFAST")) {
			if ("Y".equals(rawData.get("authorizedIFAST").getAsString())) {
				authorisedArray.add("IFAST");

				isIFAST = true;
			}
		}
		agentProfileForCouchBase.add("authorised", authorisedArray);

		if (isM8A && isM8 && isIFAST) {
			authorisedDesString += ", Unit Trusts";
		}

		agentProfileForCouchBase.addProperty("authorisedDes", authorisedDesString);

		String authGroup = "";
		if (rawData.has("authGroup")) { // channel mapping
			String channelValue = "";

			authGroup = rawData.get("authGroup").getAsString();
			switch (authGroup) {
			case "AG001":
				channelValue = "AGENCY";
				break;
			case "PO006":
				channelValue = "SINGPOST";
				break;

			case "WS005":
				channelValue = "DIRECT";
				break;

			case "FU004":
				channelValue = "FUSION";
				break;

			case "BF003":
				channelValue = "BROKER";
				break;

			case "SY002":
				channelValue = "SYNERGY";
				break;

			case "GI001":
				channelValue = "GIAGENCY";
				break;

			case "BA001":
				channelValue = "BANCA";
				break;
			default:
				break;
			}

			if (channelValue.length() > 0) {
				agentProfileForCouchBase.addProperty("channel", channelValue);
			}
		}

		// agentCode for 121
		if (rawData.has("agentCode")) {
			String agentCode = rawData.get("agentCode").getAsString();
			agentProfileForCouchBase.addProperty("agentCode", agentCode);
		}

		if (rawData.has("distribCode")) {
			String distribCode = rawData.get("distribCode").getAsString();
			agentProfileForCouchBase.addProperty("agentCodeDisp", distribCode);
		}

		// company
		if (rawData.has("directorOrFirm")) {
			String company = rawData.get("directorOrFirm").getAsString();
			agentProfileForCouchBase.addProperty("company", company);
		}

		// fixed value 01 for compCode
		agentProfileForCouchBase.addProperty("compCode", "01");

		// email
		if (rawData.has("email")) {
			String email = rawData.get("email").getAsString();
			agentProfileForCouchBase.addProperty("email", email);
		}

		// managerCode
		if (rawData.has("upline1Code")) {
			String managerCode = rawData.get("upline1Code").getAsString();

			// manager name;
			USER_PROFILE userProfileDao = new USER_PROFILE();
			JSONObject userProfileJson = userProfileDao.selectRecordByAgentNo(managerCode);

			if (userProfileJson != null) {
				if (userProfileJson.has("NAME")) {
					String managerName = userProfileJson.getString("NAME");
					agentProfileForCouchBase.addProperty("manager", managerName);
				}

				if (userProfileJson.has("DISTRIB_NO")) {
					String managerDisplayCode = userProfileJson.getString("DISTRIB_NO");
					agentProfileForCouchBase.addProperty("managerDisp", managerDisplayCode);
				}
			}

			agentProfileForCouchBase.addProperty("managerCode", managerCode);
		}

		// mobile
		if (rawData.has("mobilePhone")) {
			String mobile = rawData.get("mobilePhone").getAsString();
			agentProfileForCouchBase.addProperty("mobile", mobile);
		}

		if (rawData.has("fullName")) {
			String name = rawData.get("fullName").getAsString();
			agentProfileForCouchBase.addProperty("name", name);
		}

		if (rawData.has("userId")) {
			String profileId = rawData.get("userId").getAsString();
			agentProfileForCouchBase.addProperty("profileId", profileId);
		}

		if (rawData.has("userRole")) {
			String role = rawData.get("userRole").getAsString();
			agentProfileForCouchBase.addProperty("role", role);
		}

		if (rawData.has("officePhone")) {
			String tel = rawData.get("officePhone").getAsString();
			agentProfileForCouchBase.addProperty("tel", tel);
		}

		agentProfileForCouchBase.addProperty("type", "agent"); // fixed value
		
		String rank = "";

		if (rawData.has("rank")) { // AXA's rank Code
			rank = rawData.get("rank").getAsString();
		}

		agentProfileForCouchBase.add("rawData", rawData); // raw Data of AXA

		LOOKUP_AGENT_RANK agentRankDao = new LOOKUP_AGENT_RANK(); // position name lookup
		JSONObject rankLookupJson = agentRankDao.select(authGroup, rank);

		if (rankLookupJson != null) {
			if (rankLookupJson.has("NAME")) {
				String positionName = rankLookupJson.getString("NAME");
				agentProfileForCouchBase.addProperty("position", positionName);
			}
		}

		return agentProfileForCouchBase.toString();
	}

	private boolean updateProxyRecord(String audID, String userId, JsonObject agentJson) {
		boolean result = true;

		try {
			String proxy1 = agentJson.has("proxy1UserId") ? agentJson.get("proxy1UserId").getAsString() : null;
			String proxy2 = agentJson.has("proxy2UserId") ? agentJson.get("proxy2UserId").getAsString() : null;
			String startDateString = agentJson.has("proxyStartDate") ? agentJson.get("proxyStartDate").getAsString()
					: null;
			String endDateString = agentJson.has("proxyEndDate") ? agentJson.get("proxyEndDate").getAsString() : null;

			boolean hasChange = compareChangeOfProxyRecord(proxy1, proxy2, startDateString, endDateString, userId);

			Log.debug("***ProxyRecord hasChange=" + hasChange);

			if (hasChange) {
				boolean proxyHistoryResult = insertChangeOfProxy(audID, proxy1, proxy2, startDateString, endDateString,
						userId);
				Log.debug("***proxyHistory result=" + proxyHistoryResult);
			}
		} catch (Exception ex) {
			Log.error(ex);

			result = false;
		}

		return result;
	}

	private boolean compareChangeOfProxyRecord(String proxy1, String proxy2, String startDateString,
			String endDateString, String userId) throws Exception {
		Log.info("Goto UserProfileService.compareChangesWithOldRecord");
		
		SimpleDateFormat dateSQLFormatter = new SimpleDateFormat(Constant.DATETIME_PATTERN_AS_SQL_DATE);
		dateSQLFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		SimpleDateFormat dateDateOnlyFormatter = new SimpleDateFormat(Constant.DATETIME_PATTERN_AS_DATE_ONLY);
		dateDateOnlyFormatter.setTimeZone(TimeZone.getTimeZone(Constant.ZONE_ID));

		boolean isEmpty = false;

		if (proxy1 == null && proxy2 == null && startDateString == null && endDateString == null) {
			isEmpty = true;
		}

		API_AGT_PROXY_HIST historyProfile = new API_AGT_PROXY_HIST();

		JSONObject resultJson = historyProfile.selectLastRecord(userId);

		boolean hasChanged = true;

		if (resultJson != null) {
			hasChanged = false;

			boolean hasChangedPROXY1 = false;
			boolean hasChangedPROXY2 = false;
			boolean hasChangedSTART_DT = false;
			boolean hasChangedEND_DT = false;

			// Compare with incoming data, only save record if there is a change
			if (resultJson.has("PROXY1") && proxy1 != null) {
				hasChangedPROXY1 = !resultJson.getString("PROXY1").equals(proxy1);
			} else if ((resultJson.has("PROXY1") && proxy1 == null) || (!resultJson.has("PROXY1") && proxy1 != null)) {
				hasChangedPROXY1 = true;
			}

			if (resultJson.has("PROXY2") && proxy2 != null) {
				hasChangedPROXY2 = !resultJson.getString("PROXY2").equals(proxy2);
			} else if ((resultJson.has("PROXY2") && proxy2 == null) || (!resultJson.has("PROXY2") && proxy2 != null)) {
				hasChangedPROXY2 = true;
			}

			if (resultJson.has("START_DT") && startDateString != null) {

				String startDateStringFromDB = "";

				try {
					startDateStringFromDB = resultJson.get("START_DT").toString();
					
					Log.debug("*** orignal startDateStringFromDB="+startDateStringFromDB);
					
					Date parsedStartDate = dateSQLFormatter.parse(startDateStringFromDB);
					startDateStringFromDB = dateDateOnlyFormatter.format(parsedStartDate);

				} catch (Exception ex) {
					Log.error(ex);
				}
				
				Log.debug("*** startDateStringFromDB="+startDateStringFromDB+", startDateString="+startDateString);
				hasChangedSTART_DT = !startDateStringFromDB.equals(startDateString);

			} else if ((resultJson.has("START_DT") && startDateString == null)
					|| (!resultJson.has("START_DT") && startDateString != null)) {
				hasChangedSTART_DT = true;
			}

			if (resultJson.has("END_DT") && endDateString != null) {

				String endDateStringFromDB = "";

				try {
					endDateStringFromDB = resultJson.get("END_DT").toString();
					
					Log.debug("*** orignal endDateStringFromDB="+endDateStringFromDB);
					
					Date parsedEndDate = dateSQLFormatter.parse(endDateStringFromDB);
					endDateStringFromDB = dateDateOnlyFormatter.format(parsedEndDate);

				} catch (Exception ex) {
					Log.error(ex);
				}
				
				Log.debug("*** endDateStringFromDB="+endDateStringFromDB+", endDateString="+endDateString);
				hasChangedEND_DT = !endDateStringFromDB.equals(endDateString);
			} else if ((resultJson.has("END_DT") && endDateString == null)
					|| (!resultJson.has("END_DT") && endDateString != null)) {
				hasChangedEND_DT = true;
			}

			if (hasChangedPROXY1 || hasChangedPROXY2 || hasChangedSTART_DT || hasChangedEND_DT) {
				hasChanged = true;
			}

		} else if (isEmpty && resultJson == null) {
			hasChanged = false; // Do not save to DB if incoming data is empty and DB has no record
		}

		return hasChanged;
	}

	private boolean insertChangeOfProxy(String audID, String proxy1, String proxy2, String startDateString,
			String endDateString, String userId) throws Exception {
		Log.info("Goto UserProfileService.insertChangeOfProxy");

		// Save changed data for record
		API_AGT_PROXY_HIST proxyHistory = new API_AGT_PROXY_HIST();

		boolean result = proxyHistory.create(audID, proxy1, proxy2, startDateString, endDateString, userId);

		return result;
	}

	private void updateDirectReportsOnCouchBase(String agentCode, String managerName, String managerDisplayCode) {

		try {
			USER_PROFILE userProfileDao = new USER_PROFILE();
			JSONArray directReports = userProfileDao.selectDirectReports(agentCode);

			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement directReportsJsonElement = gson.fromJson(directReports.toString(), JsonElement.class);

			if (directReportsJsonElement != null) {

				JsonArray directReportsJsonArray = directReportsJsonElement.getAsJsonArray();

				for (JsonElement jsonElement : directReportsJsonArray) {
					JsonObject jsonObject = jsonElement.getAsJsonObject();

					String userId = jsonObject.get("USER_ID").getAsString();

					JSONObject existingAgentProfile = Constant.CBUTIL.getDoc("U_" + userId);

					try {

						if (existingAgentProfile != null) {
							existingAgentProfile.put("manager", managerName);
							existingAgentProfile.put("managerDisp", managerDisplayCode);

							// Update record
							String resultDoc = Constant.CBUTIL.UpdateDoc("U_" + userId,
									existingAgentProfile.toString());

							if (resultDoc.length() > 0) {
								Log.debug("updateDirectReportsOnCouchBase Succeed");
							} else {
								Log.debug("updateDirectReportsOnCouchBase Failed");
							}
						}
					} catch (Exception ex) {
						Log.error(ex);
					}

					Log.debug("*********jsonElement=" + jsonElement.toString());
				}
			}

			Log.debug("*********directReports=" + directReports.toString());
		} catch (Exception ex) {
			Log.error(ex);
		}

	}

	private boolean updateUserProfileRecord(JsonObject agentJson) {
		boolean result = false;

		try {
			UserProfile userProfile = convertToUserProfile(agentJson);

			if (userProfile != null) {

				JSONObject userProfileJson = getUserProfileRecord(userProfile.getAgentCode());

				if (userProfileJson != null) {
					boolean removeResult = removeUserProfileRecord(userProfile.getAgentCode());
					Log.debug("***removeUserProfileRecord=" + removeResult);
				}

				result = insertUserProfileRecord(userProfile);
				Log.debug("***insertUserProfileRecord=" + result);

				if (userProfile.getFullName() != null) {
					updateDirectReportsOnCouchBase(userProfile.getAgentCode(), userProfile.getFullName(),
							userProfile.getDistribCode()); // update all
					// direct
					// reports'
					// managaer's
					// name
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		return result;
	}

	private JSONObject getUserProfileRecord(String agentCode) throws Exception {
		Log.info("Goto UserProfileService.getUserProfile");
		USER_PROFILE userProfileDao = new USER_PROFILE();

		JSONObject userProfileJson = userProfileDao.selectRecordByAgentNo(agentCode);
		return userProfileJson;
	}

	private boolean removeUserProfileRecord(String agentCode) throws Exception {
		Log.info("Goto UserProfileService.removeUserProfileRecord");
		USER_PROFILE userProfileDao = new USER_PROFILE();
		boolean result = userProfileDao.delete(agentCode);

		return result;
	}

	private boolean insertUserProfileRecord(UserProfile userProfile) throws Exception {
		Log.info("Goto UserProfileService.insertUserProfileRecord");
		USER_PROFILE userProfileDao = new USER_PROFILE();
		boolean result = userProfileDao.create(userProfile.getUserId(), userProfile.getTitle(),
				userProfile.getFullName(), userProfile.getMobilePhone(), userProfile.getOfficePhone(),
				userProfile.getEmail(), userProfile.getUserRole(), userProfile.getUserCat(), userProfile.getAgentCode(),
				userProfile.getFaAdvisorRole(), userProfile.getDirectorOrFirm(), userProfile.getDistribCode(),
				userProfile.getUnitCode(), userProfile.getAuthGroup(), userProfile.getUserStatus(),
				userProfile.getAuthorizedHI(), userProfile.getAuthorizedM8(), userProfile.getAuthorizedM8A(),
				userProfile.getAuthorizedIFAST(), userProfile.getRank(), userProfile.getPosition(),
				userProfile.getUpline1Code(), userProfile.getUpline2Code(), userProfile.getProxy1UserId(),
				userProfile.getProxy2UserId(), userProfile.getProxyStartDate(), userProfile.getProxyEndDate(),
				userProfile.getSuspStartDate(), userProfile.getSuspEndDate(), userProfile.getUserType());
		return result;
	}

	public boolean createTransactionRecord(String audID, String header, String body) throws Exception {
		Log.info("Goto UserProfileService.createTransactionRecord");
		API_AGT_PROFILE_TRX trxProfileDao = new API_AGT_PROFILE_TRX();
		return trxProfileDao.create(audID, header, body);
	}

	public boolean updateTransactionRecord(String audID, String responseHeaderAsString, String responseAsString,
			boolean isCompleted) throws Exception {
		Log.info("Goto UserProfileService.updateTransactionRecord");
		API_AGT_PROFILE_TRX trxProfileDao = new API_AGT_PROFILE_TRX();
		return trxProfileDao.update(audID, responseHeaderAsString, responseAsString, isCompleted);
	}

}
