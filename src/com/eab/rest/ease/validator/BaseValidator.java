package com.eab.rest.ease.validator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public abstract class BaseValidator {	
	public String validate(JsonObject bodyJson) {
		String errorString = "";
		
		errorString += validateJsonNodeRecursively(bodyJson, "","","");

		return errorString;
	}
	
	protected abstract boolean getLevelDisplay();
	protected abstract String validateMandatoryFields(JsonObject json, String parentKey, String level);
	protected abstract String validateField(JsonObject json, String currentKey, String parentKey);  
	
	private String validateJsonNodeRecursively(JsonObject json, String levelArrayIndex, String parentKey, String level) {
		
		boolean isLevelDisplay = getLevelDisplay();
		
		String localErrorString = "";

		String errorOfMandatoryFields = validateMandatoryFields(json, parentKey, level); //check missing fields

		if (errorOfMandatoryFields.length() > 0) { //has missing field error
			localErrorString += isLevelDisplay? levelArrayIndex + ": " + errorOfMandatoryFields + " | " : errorOfMandatoryFields;
		}

		for (String key : json.keySet()) {
			String errorOfField = validateField(json,key,parentKey); //check JSON type
			
			if(errorOfField.length() > 0) {  //has field error
				localErrorString += isLevelDisplay? levelArrayIndex + ": " + errorOfField + " | " : errorOfField;
			}

			if(json.get(key).isJsonArray()) {
				String tempLevelKey = levelArrayIndex + "." + key; // level with array index
				JsonArray jsonArray = json.get(key).getAsJsonArray();
				
				String tempLevel = level+ "." + key; // level without array index
				
				int objIndex = 0;
				
				for (JsonElement jsonElement : jsonArray) {
					if (jsonElement.isJsonObject()) {
						String tempArrayIndexKey = tempLevelKey + "[" + String.valueOf(objIndex) + "]";
						localErrorString += validateJsonNodeRecursively(jsonElement.getAsJsonObject(), tempArrayIndexKey, key, tempLevel);
					}
					
					objIndex++;
				}
			}else if(json.get(key).isJsonObject()) {
				String tempLevelKey = level + "." + key;
				JsonObject subJson = json.get(key).getAsJsonObject();
				localErrorString += validateJsonNodeRecursively(subJson, tempLevelKey, key, tempLevelKey); //go down into next level
			}

		}

		return localErrorString;
	}

}
