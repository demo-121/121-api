package com.eab.rest.ease.wfi.document;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.eab.auth.ease.Maam;
import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.API_WFI_REC_DOC_TRX;
import com.google.gson.JsonObject;

public class RecordDocumentService {
	
	
	public HttpPost createDocumentPostRequest(String msgid, JsonObject jsonBody, boolean isHealth)
			throws Exception {
		Log.info("Goto RecordDocumentUtil.createPostDocumentRequest");

		String recordDocumentHost = EnvVariable.get("WEB_API_WFI_DOC");
		String environmentValueForWFI= EnvVariable.get("WEB_API_WFI_DOC_ENV");

		HttpPost request = new HttpPost(recordDocumentHost);
		request.addHeader("Content-Type", "application/json");

		// Headers
		request.addHeader("Authorization", "Bearer " + Maam.getToken());
		request.addHeader("x-axa-entity", "SG");
		request.addHeader("x-axa-requesting-channel", "EASE");
		request.addHeader("x-axa-contextheader-customdata-sourcesystem", "EASE");
		request.addHeader("x-axa-contextheader-customdata-targetsystem", "FileNet");
		request.addHeader("x-axa-msgid", msgid);
		request.addHeader("x-axa-initialmsgid", msgid);
		request.addHeader("x-axa-lob", "Life");
		request.addHeader("x-axa-contextheader-contextversion", "1.0");
		request.addHeader("x-axa-env", environmentValueForWFI);//environment value for WFI
		request.addHeader("Accept", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

	public JsonObject convertJsonBodyForRecordDocument(JsonObject fromJsonBody) throws Exception{
		Log.info("Goto RecordDocumentUtil.convertJsonBodyForRecordDocument");
		
		// documentElements
		JsonObject documentElement = new JsonObject();
		documentElement.add("MIMEType", fromJsonBody.get("MIMEType"));
		documentElement.add("documentTitle", fromJsonBody.get("documentTitle"));
		documentElement.add("documentType", fromJsonBody.get("documentType"));
		documentElement.add("insuredName", fromJsonBody.get("insuredName"));
		documentElement.add("policyNumber", fromJsonBody.get("policyNumber"));
		documentElement.add("primaryDocumentFlag", fromJsonBody.get("primaryDocumentFlag"));
		documentElement.add("productType", fromJsonBody.get("productType"));
		documentElement.add("channelCode", fromJsonBody.get("channelCode"));
		
		if (fromJsonBody.has("otherPolicyNumber"))
			documentElement.add("otherPolicyNumber", fromJsonBody.get("otherPolicyNumber"));
		
		if (fromJsonBody.has("basePlanCode"))
			documentElement.add("basePlanCode", fromJsonBody.get("basePlanCode"));

		// fixed values
		documentElement.addProperty("processType", "NBU");
		documentElement.addProperty("entityCode", "SG");
		documentElement.addProperty("backFillFlag", "NO");
		documentElement.addProperty("workType", "NEWAPPLICATION");

		// Convert creationDate to be time zone +8
		String creationDateString = fromJsonBody.get("creationDate").getAsString();
		String convertedCreationDateString = Function.convertDateTimeZoneString(creationDateString, Constant.ZONE_ID, Function.DATE_FORMAT_AXA);
		documentElement.addProperty("creationDate", convertedCreationDateString);
		
		// RecordDocumentRequest
		JsonObject recordDocumentRequest = new JsonObject();
		recordDocumentRequest.add("contentData", fromJsonBody.get("contentData"));
		recordDocumentRequest.add("documentElement", documentElement);

		JsonObject toJsonBody = new JsonObject();
		toJsonBody.add("RecordDocumentRequest", recordDocumentRequest);

		return toJsonBody;
	}
	
	public boolean createTransactionRecord(String audID, String headerAsString, String body, String policyNumberAsString) throws Exception {
		Log.info("Goto RecordDocumentService.createTransactionRecord");
		API_WFI_REC_DOC_TRX  trxRecordDocDao = new API_WFI_REC_DOC_TRX();
		return trxRecordDocDao.create(audID, headerAsString, body, policyNumberAsString);
	}

	public boolean updateTransactionRecord(String audID, String requestHeader, String requestParam, String responseHeaderAsString, String responseAsString,
			boolean isCompleted) throws Exception {
		Log.info("Goto RecordDocumentService.updateTransactionRecord");
		API_WFI_REC_DOC_TRX  trxRecordDocDao = new API_WFI_REC_DOC_TRX();
		return trxRecordDocDao.update(audID, requestHeader, requestParam, responseHeaderAsString, responseAsString,isCompleted);
	}
}
