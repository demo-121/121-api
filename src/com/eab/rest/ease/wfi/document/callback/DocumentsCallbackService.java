package com.eab.rest.ease.wfi.document.callback;

import javax.ws.rs.core.Response;

import com.eab.common.Log;
import com.eab.common.RestUtil;
import com.eab.dao.API_WFI_DOC_NOTI_TRX;
import com.google.gson.JsonObject;

public class DocumentsCallbackService {


	public Response createSuccessResponse(String audID) {
		Log.info("Goto DocumentsCallbackService.createSuccessResponse");

		JsonObject notifyDocumentResponse = new JsonObject();
		notifyDocumentResponse.addProperty("transactionResponseFlag", "Success");

		JsonObject successResponseJson = new JsonObject();
		successResponseJson.add("NotifyDocumentResponse", notifyDocumentResponse);

		return RestUtil.toResponse(audID, Response.Status.OK, successResponseJson.toString());
	}

	public Response createErrorResponse(String audID, String message, String reasonCode, String reason) {
		Log.info("Goto DocumentsCallbackService.createErrorResponse");

		JsonObject exception = new JsonObject();

		exception.addProperty("message", message);
		exception.addProperty("reasonCode", reasonCode);
		exception.addProperty("reason", reason);

		JsonObject errorResponseJson = new JsonObject();
		errorResponseJson.add("exception", exception);

		return RestUtil.toResponse(audID, Response.Status.INTERNAL_SERVER_ERROR, errorResponseJson.toString());
	}

	public String getPolicyNumber(JsonObject jsonBody) {
		Log.info("Goto DocumentsCallbackService.getPolicyNumber");

		String policyNumberAsString = "";
		try {
			if (jsonBody.has("CustomerInteractionRequest")) {
				JsonObject customerInteractionRequest = jsonBody.get("CustomerInteractionRequest").getAsJsonObject();
				if (customerInteractionRequest.has("policyNumber")) {
					policyNumberAsString = customerInteractionRequest.get("policyNumber").getAsString();
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		return policyNumberAsString;
	}

	public boolean createTransactionRecord(String audID, String header, String body) throws Exception {
		Log.info("Goto DocumentsCallbackService.createTransactionRecord");
		API_WFI_DOC_NOTI_TRX trxDocNotiDao = new API_WFI_DOC_NOTI_TRX();
		return trxDocNotiDao.create(audID, header, body);
	}

	public boolean updateTransactionRecord(String audID, String policyNumberAsString, String responseHeaderAsString, String responseAsString,
			boolean isCompleted) throws Exception {
		Log.info("Goto DocumentsCallbackService.updateTransactionRecord");
		API_WFI_DOC_NOTI_TRX trxDocNotiDao = new API_WFI_DOC_NOTI_TRX();
		return trxDocNotiDao.update(audID, policyNumberAsString, responseHeaderAsString, responseAsString, isCompleted);
	}

}
