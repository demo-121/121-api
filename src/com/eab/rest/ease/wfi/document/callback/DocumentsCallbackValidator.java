package com.eab.rest.ease.wfi.document.callback;

import com.eab.rest.ease.validator.BaseValidator;
import com.google.gson.JsonObject;

public class DocumentsCallbackValidator extends BaseValidator {

	@Override
	protected boolean getLevelDisplay() {
		return false;
	}

	@Override
	protected String validateMandatoryFields(JsonObject json, String parentKey, String level) {
		//Do nothing for coming data from AXA. Prepared this for future use.
		return "";
	}

	@Override
	protected String validateField(JsonObject json, String currentKey, String parentKey) {
		//Do nothing for coming data from AXA. Prepared this for future use.
		return "";
	}

}
